## Version 2.0
## language: en

Feature: Cryptanalysis server-Root Me
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    blaxter
  Goal:
    Find the validation password in the ZIP file.

  Background:
  Hacker's software:
    | <Software name> |    <Version>       |
    | Linux           | 4.19.0-kali1-amd64 |
    | fcrackzip       | version 1.0        |

  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/en/Challenges/Cryptanalysis/File-PKZIP
    File - PKZIP
    """
    And read the challenge statement
    """
    Enter password :
    """
    Then Click on the download button
    """
    http://challenge01.root-me.org/cryptanalyse/ch5/ch5.zip
    """
  Scenario: Success - bruteforce ZIP file password.
    Given the related ressource
    """
    open https://bit.ly/2C99suG
    """
    Then Use fcrackzip to bruteforce.
    """
    $ fcrackzip ch5.zip
    possible pw found: 14535 ()
    """
    Then use the possible password finded.
    """
    $ unzip ch5.zip
      Archive:  ch5.zip
      [ch5.zip] readme.txt password:
      inflating: readme.txt
    """
    Then i proceed to read the txt file.
    """
    $ cat readme.txt
    Use ZIP password to validate this challenge.
    Utiliser le mot de passe de l'archive pour valider le challenge.
    """
    Then I use the ZIP password code (14535) to validate the challenge.
