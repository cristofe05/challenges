;; $ clj-kondo --lint nickkar.cljs
;; linting took 70ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as s]))

(defn factors [r factor num count]

  (cond
    (and (s/blank? r) (>= factor num)) (str num)
    (or (>= factor num) (= count 1)) (s/join "" (drop-last r))
    (= (mod count factor) 0) (factors
    (str r (str factor "*"))
    2
    num
    (/ count factor))
    :else (factors r (+ factor 1) num count)
    )
  )

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (let [number (core/read)]
        (print (factors "" 2 number number))
        (print " "))
      (recur (+ x 1)))
    )
  )


(defn main []
  (let [input-size (core/read)]
    (itera input-size)
    (println "")))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 89*193*223*353*367 137*193*241*313*587 53*167*271*433*523
;; 191*223*251*349*547 149*283*389*433*587 113*479*509*571*577
;; 107*211*359*587*599 67*113*197*563*571 89*107*349*401*541
;; 89*109*193*223*449 199*229*263*293*379 59*157*251*409*457
;; 53*241*269*379*409 107*199*283*317*547 79*347*379*431*439
;; 389*439*467*509 59*89*193*233*263 107*181*379*463*467
;; 317*443*461*541 223*233*293*439*571 83*107*317*443*463
;; 79*97*101*397*433 307*349*359*457*509 61*79*389*461*503
;; 53*239*277*523*577 61*97*157*193*599 71*227*251*283*503
