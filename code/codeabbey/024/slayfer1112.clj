;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (data 1)]
    [head
     body]))

(defn check-digits [value]
  (if (< (count value) 8)
    (check-digits (str "0" value))
    value))

(defn neumann [seed arr counter]
  (let [seed-sqrt (* seed seed)
        value (check-digits (str seed-sqrt))
        trunc (Integer. (subs value 2 6))]
    (if (< (.indexOf arr trunc) 0)
      (neumann trunc (vec (concat arr [trunc])) (inc counter))
      counter)))

(defn solution [data]
  (let [seed data
        val (neumann seed [seed] 1)]
    (print (str val " "))))

(defn main []
  (let [[_ body] (get-data)
        data (str/split body #" ")]
    (doseq [x data]
      (solution (read-string x)))
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 106 111 97 101 98 98 105 98 110 100 101
