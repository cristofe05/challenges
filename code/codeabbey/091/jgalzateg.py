#!/usr/bin/env python3
# $ mypy jgalzateg.py
# Success: no issues found in 1 source file

from typing import List


def drop_zeros(vec: List[int]) -> List[int]:
    i = len(vec) - 1
    if i > 0:
        if vec[i] == 0:
            vec.pop()
            vec = drop_zeros(vec)
    return vec


def count_occu(vector: List[int]) -> List[int]:
    vals = list(range(1, 12))
    ans = list(map(lambda x: vector.count(2**x), vals))
    return drop_zeros(ans)


def only_row(board: List[List[int]], i: int) -> List[int]:
    if i > 0:
        vec = board[i] + only_row(board, i - 1)
    else:
        vec = board[i]
    return vec


def to_str(vec: List[int], i: int) -> str:
    acum = ""
    if i > 0:
        acum = acum + to_str(vec, i - 1) + " " + str(vec[i])
    else:
        acum = acum + str(vec[i])
    return acum


def move(arr_m: List[int], i: int) -> List[int]:
    if i > 0:
        arr_m[i] = arr_m[i - 1]
        arr_m = move(arr_m, i - 1)
    else:
        arr_m[i] = 0
    return arr_m


def act_vect(vector: List[int], i: int) -> List[int]:
    if i > 0:
        if max(vector[:i]) != 0:
            if vector[i] == 0:
                act_vect(move(vector, i), i)
            elif vector[i - 1] == 0:
                act_vect(move(vector, i - 1), i)
            elif vector[i] == vector[i - 1]:
                vector[i] = vector[i] + vector[i - 1]
                act_vect(move(vector, i - 1), i - 1)
            else:
                act_vect(vector, i - 1)
    return vector


def nat_disp(matriz: List[List[int]], i: int = 0) -> List[List[int]]:
    dim = len(matriz) - 1
    if i < dim:
        matriz[i] = act_vect(matriz[i], dim)
        matriz = nat_disp(matriz, i + 1)
    else:
        matriz[i] = act_vect(matriz[i], dim)
    return matriz


def inv_disp(matriz: List[List[int]], i: int = 0) -> List[List[int]]:
    dim = len(matriz) - 1
    if i < dim:
        matriz[i] = act_vect(matriz[i][::-1], dim)[::-1]
        matriz = inv_disp(matriz, i + 1)
    else:
        matriz[i] = act_vect(matriz[i][::-1], dim)[::-1]
    return matriz


def read_moves(board: List[List[int]],
               mov: List[str], i: int = 0) -> List[List[int]]:
    if mov[i] == 'U':
        board_inv = inv_disp(list(map(list, list(zip(*board)))))
        board = list(map(list, list(zip(*board_inv))))
    elif mov[i] == 'D':
        board_inv = nat_disp(list(map(list, list(zip(*board)))))
        board = list(map(list, list(zip(*board_inv))))
    elif mov[i] == 'L':
        board = inv_disp(board)
    elif mov[i] == 'R':
        board = nat_disp(board)
    if i < len(mov) - 1:
        board = read_moves(board, mov, i + 1)
    return board


if __name__ == "__main__":
    LSA = input("")
    LSB = input("")
    LSC = input("")
    LSD = input("")
    MOVES = input("")
    LSA_INT = list(map(int, LSA.split(" ")))
    LSB_INT = list(map(int, LSB.split(" ")))
    LSC_INT = list(map(int, LSC.split(" ")))
    LSD_INT = list(map(int, LSD.split(" ")))
    MOVES_L = MOVES.split(" ")
    BOARD_INT = [LSA_INT, LSB_INT, LSC_INT, LSD_INT]
    BOARD_RES = read_moves(BOARD_INT, MOVES_L)
    print(BOARD_RES)
    VAL_RES = count_occu((only_row(BOARD_RES, len(BOARD_RES) - 1)))
    print(to_str(VAL_RES, len(VAL_RES) - 1))

# cat DATA.lst | python jgalzateg.py
# 2 1 2 1
