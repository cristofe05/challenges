/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::i64;
use std::io::{self, BufRead};
use std::str;

const PRINTABLE: [u8; 53] = [
  97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112,
  113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 65, 66, 67, 68, 69, 70, 71,
  72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
  32,
];

fn is_printable(chars: Vec<u8>) -> bool {
  let mut printable = false;
  for chr in chars {
    if !PRINTABLE.contains(&chr) {
      printable = false;
      break;
    } else {
      printable = true;
    }
  }
  printable
}

fn xor(chars: Vec<u8>) -> (u32, u32) {
  let mut p = 0;
  let mut l_key = 0u32;

  for key in 0..255 {
    let mut xored = Vec::new();
    for chr in chars.clone() {
      xored.push(key ^ chr);
    }

    if is_printable(xored.clone()) {
      p += 1;
      l_key = key as u32;
    }
  }

  if p == 1 {
    (p, l_key)
  } else {
    (p, 0)
  }
}

fn find_key(messages: Vec<Vec<u8>>, minimun: usize) {
  let mut decoded_string = "".to_string();
  let mut decoded_string2 = "".to_string();

  for index in 0..minimun {
    let mut chars = Vec::new();
    for message in messages.clone() {
      chars.push(message[index]);
    }
    let (p, key) = xor(chars.clone());
    if p == 1 {
      let ascii = &[(key ^ messages.last().unwrap()[index] as u32) as u8];
      let decoded = str::from_utf8(ascii).unwrap();

      let ascii2 = &[(key ^ messages[0][index] as u32) as u8];
      let decoded2 = str::from_utf8(ascii2).unwrap();

      decoded_string += &decoded;
      decoded_string2 += &decoded2;
    } else {
      decoded_string += "_";
      decoded_string2 += "_";
      let chr1 = messages[0][index];
      let chr2 = messages.last().unwrap()[index];
      println!("string1: {} -> string2: {}", chr1, chr2);
    }
  }
  println!("{}", decoded_string2);
  println!("{}", decoded_string);
}

fn line2vec(line: String) -> (Vec<u8>, u32) {
  let mut vect = Vec::new();
  let mut len: u32 = 0;
  for hex in line.split('.') {
    len += 1;
    vect.push(i64::from_str_radix(hex, 16).unwrap() as u8);
  }
  (vect, len)
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  let mut messages = Vec::new();
  let mut lenght = Vec::new();

  for (index, line) in lines.enumerate() {
    if index > 0 {
      let (data, len) = line2vec(line.unwrap());
      messages.push(data);
      lenght.push(len);
    }
  }

  find_key(messages, *lenght.iter().min().unwrap() as usize);
}

/*
$ cat DATA.lst | ./dfuribez
string1: 113 -> string2: 106
string1: 217 -> string2: 196
string1: 169 -> string2: 164
string1: 68 -> string2: 72
string1: 20 -> string2: 51
string1: 207 -> string2: 207
__r towns and destro_ed the l_ves of our people He i_ at _his time
__ese are Life Liber_y and th_ pursuit of Happiness _hat _o secure
*/
