## Version 2.0
## language: en

Feature: ithappenssometimes-realist-rootme
  Site:
    Root-me.org
  Category:
    Realist
  User:
    ununicornio
  Goal:
    Find the flag in the admin sectionof challenge site

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Google Chrome   | 69.0.3497.100 |
    | Postman         | 6.5.2         |
  Machine information:
    Given I am accessing the challenge site from my browser
    And I press the Start the Challenge button, going to another page
    And see this statement
    """
    Welcome on WebGallery 1.0
    On 11/11/1960 we decided to start coding a revolutionary program, a
    program that would meet everyone's needs. WebGallery 1.0 is a program that
    classifies your photos by date. So cool, isn't it? It is on sale for 50
    easy payments of $500. The purchase page will soon be open.

    Last November, we suffered a hack, so we had to close all our systems, the
    hacker had deleted all our files. So we always work on our site, there is
    nothing special about it.
    """

  Scenario: Fail:Reading-source-code
    Given I access the challenge page
    And see this statement
    """
    Welcome on WebGallery 1.0
    On 11/11/1960 we decided to start coding a revolutionary program, a
    program that would meet everyone's needs. WebGallery 1.0 is a program that
    classifies your photos by date. So cool, isn't it? It is on sale for 50
    easy payments of $500. The purchase page will soon be open.

    Last November, we suffered a hack, so we had to close all our systems, the
    hacker had deleted all our files. So we always work on our site, there is
    nothing special about it.
    """
    And I check the page source code to see if there's any clue
    Then I find nothing interesting

  Scenario: Fail:Bruteforce
    Given I access the challenge page
    And see this statement
    """
    Welcome on WebGallery 1.0
    On 11/11/1960 we decided to start coding a revolutionary program, a
    program that would meet everyone's needs. WebGallery 1.0 is a program that
    classifies your photos by date. So cool, isn't it? It is on sale for 50
    easy payments of $500. The purchase page will soon be open.

    Last November, we suffered a hack, so we had to close all our systems, the
    hacker had deleted all our files. So we always work on our site, there is
    nothing special about it.
    """
    And I append to the url
    """/admin"""
    Then I get a prompt for inputting a user/password so I try
    """admin/admin"""
    Then it fails to log me in

  Scenario: Success:HTTP-verb-tampering
    Given I send a GET request to the challenge admin url
    """
    http://challenge01.root-me.org/realiste/ch3/admin
    """
    Then I get the expected response back [evidence](getresponse.png)
    Then I try with a POST request, but get the same result
    Then I try a LINK request
    And finally get access, greeted with [evidence](linkresponse.png)
    Then I input the given password in the challenge site
    Then I solved the challenge
