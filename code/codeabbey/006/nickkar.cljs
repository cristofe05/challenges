;; $ clj-kondo --lint nickkar.cljs
;; linting took 31ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn div-n-round [a b]
  (println (Math/round (double (/ a b))))
)

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (div-n-round (core/read) (core/read))
      (recur (+ x 1))))
  )


(defn main []
  (let [input-size (core/read)]
    (itera input-size)))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 3 6 3 16040 -2 14 17320 13 31766 18 18 3 5 41470 19
;; 37201 8 17622 9 17 10 -8 2 18 18 9 8 21 18 5 2
