#!/usr/bin/ruby
# frozen_string_literal: false

# $ rubocop juancrucruz.rb #linting
# Inspecting 1 file
# .
#
# 1 file inspected, no offenses detected
# $

def hex_decode(chain: [])
  chain = chain.split('.')
  chain.each_with_index { |_valor, i| chain[i] = chain[i].to_i(16) }
end

def string_todec(word: '')
  chain = []
  (0..word.length - 1).each_with_index { |_valor, i| chain << word[i].ord }
  chain
end

def xorer(chain1: [], chain2: [])
  xored = []
  if chain1.length >= chain2.length
    chain2.each_with_index { |valor, i| xored << (valor ^ chain1[i]) }
  else
    chain1.each_with_index { |valor, i| xored << (valor ^ chain2[i]) }
  end
  xored
end

def dec_tostring(chain: [])
  stinger = []
  chain.each_with_index { |_valor, i| stinger << chain[i].chr }
  stinger
end

def decryptor(chain: [], key: [])
  scope = chain[0].to_i
  chain_dec = hex_decode(chain: chain[scope])
  decipher = xorer(chain1: chain_dec, chain2: key)
  dec_str = dec_tostring(chain: decipher).join
  puts dec_str
end

# This is the key i got from my StreamBreaker code, i need to iterate
# a lot of times to get it. But this script is not meant to be interactive

key = [136, 15, 90, 155, 52, 106, 81, 123, 78, 238, 216, 152, 132, 144, 245, \
       147, 228, 187, 229, 188, 145, 62, 137, 4, 247, 218, 138, 198, 169, 233,\
       2, 50, 249, 92, 205, 45, 199, 30, 169, 21, 13, 130, 174, 145, 18, 163,\
       36, 247, 94, 10, 179, 240, 72, 60, 244, 64, 22, 127, 6, 192, 104, 9,\
       242, 97, 102, 192, 143, 46, 223, 57, 68, 236, 187, 242, 126, 206, 150,\
       163, 197, 245, 173, 120, 229]

input = []
STDIN.each_line do |line|
  input << line.chomp
end

decryptor(chain: input, key: key)
__END__

$ cat DATA.lst | ./juancrucruz.rb

out their substance He has kept among us in times of peace Standing
