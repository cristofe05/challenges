## Version 1.4.1
## language: en

Feature:
  TOE:
    WackoPicko
  Category:
    Local File Inclusion
  Location:
   admin/index.php?page=login
  CWE:
    CWE-0098: Improper Control of Filename for Include/Require Statement in PHP
    Program ('PHP Remote File Inclusion') -base-
      https://cwe.mitre.org/data/definitions/98.html
    CWE-0829: Inclusion of Functionality from Untrusted Control Sphere -class-
      https://cwe.mitre.org/data/definitions/829.html
    CWE-1016: Limit Exposure -category-
      https://cwe.mitre.org/data/definitions/1016.html
  CAPEC:
    CAPEC-0252: PHP Local File Inclusion -detailed-
      https://capec.mitre.org/data/definitions/252.html
    CAPEC-0251: Local Code Inclusion -standard-
      https://capec.mitre.org/data/definitions/251.html
    CAPEC-0175: Code Inclusion -meta-
      https://capec.mitre.org/data/definitions/175.html
  Rule:
    REQ.041: Depurar archivos de código malicioso
      https://fluidattacks.com/web/es/rules/041/
  Goal:
    Get secrect page whit LFI
  Recommendation:
    Edit file .php whit whitelist

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Firefox             | 64.0        |
    | WackoPicko          | 1.4.0       |

  TOE information:
    Given I enter the site http://localhost:8080/
    And I can buy, upload and share images on the site
    And the server is runing MySQL version >=5
    And PHP version 5.5.9
    And apache version 2.4.7
    And is running on Docker image (adamdoupe/wackopicko) on parrot OS 4.2.2x64

  Scenario: Normal use case
  Buying and uploading images
    Given I access the site "http://localhost:8080/users/login.php"
    Then I enter or register with my name
    And I can upload images
    And I see images on recent images
    And I can comment images and send comment

  Scenario: Static detection
  The file index.php in the directory / administrator does not have pages of the
  whitelist. it can consular any page and file with the null byte (%00) in the
  main server.
    Given I open the file index.ini on "/var/www/html/admin/index.ini"
    And I see:

    """
    1 <?php
    2 $page = $_GET['page'] . '.php';
    3 require_once($page);
    4 ?>
    """
    Then I detect that this file is a vulnerability to LFI, because the var
    #"page" no has one whitelist and can open any page in the main server

  Scenario: Dynamic detection
  seeing if the special characters is interpreted by the server
    Given I access the site "http://localhost:8080/admin/index.php?page=login"
    And I see that site can be vulnerable because:

    """
    i add "../" and "example: calendar" the page return to page of calendar
    http://localhost:8080/admin/index.php?page=../calendar
    """
    Then I see that is posible find files in the server
    And I conclude that LFI is posible because the URL can do open files .php
    #in the main server and any files whit the null byte (%00)

  Scenario Outline: Extraction
  getting the secrect page .php
    Given I access the site "http://localhost:8080/admin/index.php?page=login"
    Then I do random consultations
    And I access the page"http://localhost:8080/admin/index.php?page=../secrect"

      | <file>      | <output> | <evidence>     |
      | /secrect    |   OUTPUT | imagen.png     |

    Then I conclude that the access to sensibles pages
    #is possible in this server. See evidences at the end of the file

  Scenario: Remediation
  The server WackoPicko has LFI vulnerability on
  #http://localhost:8080/admin/index.php?page=login
    Given this I advise to repair the vulnerability modifying the file index.php
    #on /admin
    """
    1 <?php
    2
    3 $whitelist = array(
    4 'home.php',
    5 'login.php',
    6 'admin.php',
    7 );
    8
    9 ?>
    10
    11 <?php
    12
    13 if(in_array($_GET['page'] . '.php', $whitelist) &&
    14 file_exists($_GET['page'] . '.php')) {include($_GET['page'] . '.php');}
    15
    16 ?>
    """

    Then I can confirm that the vulnerability can be successfully patched.
    #See evidences at the end of the file (imagen1.png)
    And I also conclude that it is possible to patch the vulnerability by
    #modifying the file php.ini in dir of PHP
    #"include_path = .:/usr/share/php"

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.8/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.6/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2019-01-05
