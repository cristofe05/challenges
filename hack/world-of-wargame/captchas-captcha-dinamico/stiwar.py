import requests
import pyduktape
import re

url = 'https://wow.sinfocol.org/wargame/captchas/captcha_siete.php'
headers = {'User-Agent': 'M'}
cookies={'PHPSESSID': 'your_cookie'}
response = requests.get(url, headers=headers, cookies=cookies)
ms = response.content
param = re.findall('type="text" name="(.*?)" value=""', ms, re.DOTALL)
mp = param[0]
ns=ms[9:-356]
context = pyduktape.DuktapeContext()
d=context.eval_js(ns)
response = requests.post(url, headers=headers, cookies=cookies, data = {param[0]:d,'Validar':'Validar'})
print(response.content)
