#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun get-current-player(current-player)
  (if (string= current-player "x")
    (return-from get-current-player "o")
    (return-from get-current-player "x")
  )
)

(defun winner(board)
  (if (or (and (string= (aref board 0 0) "x")
    (string= (aref board 1 1) "x") (string= (aref board 2 2) "x"))
    (and (string= (aref board 2 0) "x") (string= (aref board 1 1) "x")
    (string= (aref board 0 2) "x")))
    (return-from winner "x")
  )
  (if (or (and (string= (aref board 0 0) "o")
    (string= (aref board 1 1) "o") (string= (aref board 2 2) "o"))
    (and (string= (aref board 2 0) "o") (string= (aref board 1 1) "o")
    (string= (aref board 0 2) "o")))
    (return-from winner "o")
  )
  (setq cont 0)
  (loop
    (if (or (and (string= (aref board cont 0) "x")
      (string= (aref board cont 1) "x")(string= (aref board cont 2) "x"))
      (and (string= (aref board 0 cont) "x") (string= (aref board 1 cont) "x")
      (string= (aref board 2 cont) "x")))
      (return-from winner "x")
    )
    (if (or (and (string= (aref board cont 0) "o")
      (string= (aref board cont 1) "o") (string= (aref board cont 2) "o"))
      (and (string= (aref board 0 cont) "o") (string= (aref board 1 cont) "o")
      (string= (aref board 2 cont) "o")))
      (return-from winner "o")
    )
    (setq cont (+ cont 1))
    (when (> cont 2)(return NIL))
  )
)

(defun solve-single-problem()
  (let ((board (make-array '(3 3) :initial-element "-")) (i 1)
  (current-player "x") (pos-winner NIL) (move NIL) (result 0))
    (loop
      (setq move (read))
      (setf (aref board (floor (- move 1) 3) (mod (- move 1) 3))
      current-player)
      (setq pos-winner (winner board))
      (if (and pos-winner (= result 0))
        (setq result i)
      )
      (setq current-player (get-current-player current-player))
      (setq i (+ i 1))
      (when (> i 9)(return-from solve-single-problem result))
    )
  )
)

(defun solve-all(size-input)
  (loop for i from 0 to (- size-input 1)
    do(format t "~a " (solve-single-problem))
  )
)

(defvar size-input (read))
(solve-all size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  5 6 8 8 8 0 8 6 6 8 7 9 6 8 7
|#
