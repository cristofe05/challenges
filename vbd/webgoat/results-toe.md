# OVERVIEW

## Number of Bugged Files: 32

## Number of Vulnerabilities: 49

In the following table you will find
each vulnerability exposed by each scanner
or manually, linked with it's respective
[Common Weakness Enumeration (CWE)](https://cwe.mitre.org/)
classification.

* **LGTM:** Instances of a particular vulnerability detected by LGTM
* **SkipFish:** Instances of a particular vulnerability detected by Skipfish
* **LGTM Escapes:** Operation in the following form: webgoat(#) - LGTM(#)
* **LGTM Escapes(%):** Operation in the following form: LGTMescapes/webgoat
* **SkipFish Escapes:** Operation in the following form: webgoat(#) - SkipFish(#)
* **SkipFish Escapes(%):** Operation in the following form: SFescapes/webgoat

| # | Vuln. Detected | LGTM | LGTM Escapes | LGTM Escapes (%) | SkipFish | SkipFish Escapes | SkipFish Escapes (%) |
| :---: | --- | :---: | :---: | :---: | :---: | :---: | :---: |
|1| [Information Exposure](https://cwe.mitre.org/data/definitions/200.html) | 1 | 0 | 0 | 0 | 1 | 100 |
|2| [SQL Injection](https://cwe.mitre.org/data/definitions/89.html) | 4 | 3 | 42.9 | 0 | 7 | 100 |
|3| [Insecure Cookie](https://cwe.mitre.org/data/definitions/1004.html) | 2 | 0 | 0 | 1 | 1 | 50 |
|4| [Insecure Deserialization](https://cwe.mitre.org/data/definitions/502.html) | 1 | 0 | 0 | 0 | 1 | 100 |
|5| [Assignment to Variable without Use](https://cwe.mitre.org/data/definitions/563.html) | 2 | 0 | 0 | 0 | 2 | 100 |
|6| [Dead Code](https://cwe.mitre.org/data/definitions/561.html) | 2 | 0 | 0 | 0 | 2 | 100 |
|7| [Improper Resource Shutdown or Release](https://cwe.mitre.org/data/definitions/404.html) | 7 | 0 | 0 | 0 | 7 | 100 |
|8| [Client-Side Enforcement of Server-Side Security](https://cwe.mitre.org/data/definitions/602.html) | 0 | 3 | 100 | 0 | 3 | 100 |
|9| [Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')](https://cwe.mitre.org/data/definitions/79.html) | 0 | 3 | 100 | 0 | 3 | 100 |
|10| [Permitir actualización de contraseña](https://fluidattacks.com/web/es/rules/125/) | 0 | 1 | 100 | 0 | 1 | 100 |
|11| [Unprotected Storage of Credentials](https://cwe.mitre.org/data/definitions/256.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|12| [Improper Access Control](https://cwe.mitre.org/data/definitions/284.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|13| [Improper Authentication](https://cwe.mitre.org/data/definitions/287.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|14| [Gestionar Cuentas de Usuario](https://fluidattacks.com/web/es/rules/246/) | 0 | 1 | 100 | 0 | 1 | 100 |
|15| [Establecer acciones de usuario seguras](https://fluidattacks.com/web/es/rules/237/) | 0 | 1 | 100 | 0 | 1 | 100 |
|16| [Cleartext Transmission of Sensitive Information](https://cwe.mitre.org/data/definitions/319.html) | 0 | 2 | 100 | 0 | 2 | 100 |
|17| [Cross-Site Request Forgery (CSRF)](https://cwe.mitre.org/data/definitions/352.html) | 0 | 3 | 100 | 0 | 3 | 100 |
|18| [Session Fixation](https://cwe.mitre.org/data/definitions/384.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|19| [HTML Tampering](https://cwe.mitre.org/data/definitions/472.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|20| [Insufficiently Protected Credentials](https://cwe.mitre.org/data/definitions/522.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|21| [Improper Restriction of XML External Entity Reference (XXE)](https://cwe.mitre.org/data/definitions/611.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|22| [Insufficient Session Expiration](https://cwe.mitre.org/data/definitions/613.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|23| [Authorization Bypass Through User-Controlled Key](https://cwe.mitre.org/data/definitions/639.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|24| [Weak Password Recovery Mechanism for Forgotten Password](https://cwe.mitre.org/data/definitions/640.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|25| [Insufficient Logging](https://cwe.mitre.org/data/definitions/778.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|26| [Define secure default options](https://fluidattacks.com/web/en/rules/161/) | 0 | 1 | 100 | 0 | 1 | 100 |
| - | Total | 19 | 30 | 62,5| 1 | 48 | 98 |

## Graphical Analysis through Venn's Diagram

![Diagram](venn-diagram.png "Venn's Diagram")

This diagram's purpose is to illustrate
in an elegant way the overall results
from the LGTM scan vs the manual tests
conducted by our analysts.

# Vulnerability List
* **Type**: Type of input,
it can be one of the following:
  * **Get|Post Parameter**
  * **Get|Post Header**
  * **TCP|UDP Port**
  * **Cookie**
  * **Code** When there is a code smell, not necessarily an input
  * **URL:** When the vulnerability
  does not has a particular input
  migth be in URL without parameters.
  * **Other**
* **Input:** It refers to the particular name of
  the vulnerable Type.
* **Vulnerability:** Name of the vulnerability found.
* **Detected by:** By which fuzzer was detected.
* **Status:** Can take up to three values:
  * **False positive**
  * **Pending confirmation**
  * **Writeup:** If it is this one, it should
  point to a gherkin file in this repo
  https://gitlab.com/fluidattacks/writeups

If a vulnerabilty is found manually, but on table doesn't exist, or no scanner
find it, that field should be filled with the **manual** keyword instead of
dash(-), only if both, detected by auth/no-auth fields are empty
or with the dash(-).

| # | URL | Type | Input | Vulnerability | Status | LGTM Link |
| :---: | --- | :---: | :---: | :---: | :---: | :---: |
|1| AjaxAuthenticationEntryPoint.java | Other | Other | Information Exposure | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0209-information-exposure/simongomez95.feature) | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-container/src/main/java/org/owasp/webgoat/AjaxAuthenticationEntryPoint.java#x69b1bde9108b11cf:1) |
|2| JWTFinalEndpoint.java | Header | Authentication | SQL Injection | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/jwt/src/main/java/org/owasp/webgoat/plugin/JWTFinalEndpoint.java#x197b8453ba41cf48:1) |
|3| JWTVotesEndpoint.java | Cookie | access_token | Insecure Cookie | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/1004-cookie-sec-attr/simongomez95.feature) | [Link](https://54.157.208.107/projects/g/WebGoat/WebGoat/snapshot/0a37af27f0b627ddd53a72b07df790f6fa44d5de/files/webgoat-lessons/jwt/src/main/java/org/owasp/webgoat/plugin/JWTVotesEndpoint.java#x84a4c92c523d81a1:1) |
|4| JWTVotesEndpoint.java | Cookie | access_token | Insecure Cookie | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/1004-cookie-sec-attr/simongomez95.feature) | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/jwt/src/main/java/org/owasp/webgoat/plugin/JWTVotesEndpoint.java#x84a4c92c523d81a1:1) |
|5| SqlInjectionLesson6a.java | Parameter | account | SQL Injection | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0089-sql-inyection/kergrau.feature) | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/advanced/SqlInjectionLesson6a.java#x2b95e3c48ba92cd0:1) |
|6| SqlInjectionLesson5a.java | Parameter | account | SQL Injection | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/introduction/SqlInjectionLesson5a.java#x2b95e3c48ba92cd0:1) |
|7| SqlInjectionLesson5b.java | Parameter | userid | SQL Injection | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/introduction/SqlInjectionLesson5b.java#xc54383daa06390f8:1) |
|8| VulnerableComponentsLesson.java | Parameter | payload | Insecure Deserialization | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0502-insecure-deserialization/simongomez95.feature) | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/vulnerable-components/src/main/java/org/owasp/webgoat/plugin/VulnerableComponentsLesson.java#x2c8af258d960906:1) |
|9| LessonContentView.js | Code Smell | Line 19 | Assignment to Variable without Use | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/02a39babb2b00ea1f75048e9327bd9f51d198399/files/webgoat-container/src/main/resources/static/js/goatApp/view/LessonContentView.js#x266d61f3c445dfb9:1) |
|10| MenuStageView.js | Code Smell | Line 7 | Assignment to Variable without Use | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/02a39babb2b00ea1f75048e9327bd9f51d198399/files/webgoat-container/src/main/resources/static/js/goatApp/view/MenuStageView.js#xe0f23d4929f08ffc:1) |
|11| PaginationControlView.js | Code Smell | Line 142 | Dead Code | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/02a39babb2b00ea1f75048e9327bd9f51d198399/files/webgoat-container/src/main/resources/static/js/goatApp/view/PaginationControlView.js#x731db6f536374e34:1) |
|12| CreateDB.java | Improper Resource Shutdown or Release | Lines 47, 41, 125, 153, 183, 223, 262, 317, 340, 380, 423, 465, 503, 560, 652, 698, 800, 939 | Dead Code | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-container/src/main/java/org/owasp/webgoat/session/CreateDB.java#xe2a21060530835:1) |
|13| JWTFinalEndpoint.java | Code Smell | Line 78 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/jwt/src/main/java/org/owasp/webgoat/plugin/JWTFinalEndpoint.java#x197b8453ba41cf48:1) |
|14| Users.java | Code Smell | Lines 38, 39 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/missing-function-ac/src/main/java/org/owasp/webgoat/plugin/Users.java#xd97cff360376633e:1) |
|15| SqlInjectionLesson6a.java | Code Smell | Lines 68, 69 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](hhttps://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/advanced/SqlInjectionLesson6b.java#xd97cff360376633e:1) |
|16| SqlInjectionLesson6b.java | Code Smell | Lines 72, 73 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/advanced/SqlInjectionLesson6b.java#xd97cff360376633e:1) |
|17| SqlInjectionLesson5a.java | Code Smell | Lines 66, 67 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/introduction/SqlInjectionLesson5b.java#xc54383daa06390f8:1) |
|18| SqlInjectionLesson5b.java | Code Smell | Lines 68, 69 | Improper Resource Shutdown or Release | Pending Confirmation | [Link](https://lgtm.com/projects/g/WebGoat/WebGoat/snapshot/69ed9cff38b342eac2e212428bfa5d88c477db9b/files/webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin/introduction/SqlInjectionLesson5b.java#xc54383daa06390f8:1) |
|19| ClientSideFiltering.html | Parameter | answer | Client-Side Enforcement of Server-Side Security | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0020-client-side-filtering/dyepesatfluid.feature) | Not Found |
|20| BypassRestrictions.html | Parameter | shortInput | Client-Side Enforcement of Server-Side Security | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0041-bypass-field-restrictions/dyepesatfluid.feature) | Not Found |
|21| CrossSiteScriptingLesson5a.java | Parameter | field1 | XSS (Reflected) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0080-reflected-xss/badwolf10.feature) | Not Found |
|22| LessonContentView.js | Parameter | parameter | XSS (DOM) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0079-dom-xss/simongomez95.feature) | Not Found |
|23| StoredXssComments.java | Parameter | commentInput | XSS (Stored) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0079-stored-xss/simongomez95.feature) | Not Found |
|24| Assignment6.java | Parameter | commentInput | SQL Injection (Blind) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0089-creating-account/simongomez95.feature) | Not Found |
|25| SqlInjectionChallenge.java | Parameter | username_reg | SQL Injection (Blind) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0089-sql-injection-advanced/badwolf10.feature) | Not Found |
|26| Assignment5.java | Parameter | password | SQL Injection | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0089-without-password/simongomez95.feature) | Not Found |
|27| None | Server Configuration | HTTPS | Cleartext Transmission of Sensitive Information | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0319-plaintext-reset/simongomez95.feature) | Not Found |
|28| None | Feature | Credentials Management | Permitir actualización de contraseña | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0255-no-password-change/simongomez95.feature) | Not Found |
|29| WebGoatUser.java | Feature | Insecure Communication | Unprotected Storage of Credentials | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0256-unprotected-creds/simongomez95.feature) | Not Found |
|30| MissingFunctionACUsers.java | Endpoint | users | Improper Access Control | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0284-improper-access/simongomez95.feature) | Not Found |
|31| VerifyAccount.java | Parameter | secQuestion0 | Improper Authentication | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0285-bypass-authentication/badwolf10.feature) | Dummy Vuln |
|32| None | Feature | Account Management | Gestionar cuentas de usuarios | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0286-account-management/simongomez95.feature) | Not Found |
|33| None | Feature | Account Management | Establecer acciones de usuario seguras | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0287-secure-actions/simongomez95.feature) | Not Found |
|34| None | Server Configuration | HTTPS | Cleartext Transmission of Sensitive Information | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0319-cleartext-transmission/simongomez95.feature) | Not Found |
|35| ForgedReviews.java | Parameter | csrf, submit | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0352-csrf/simongomez95.feature) | Not Found |
|36| CSRFFeedback.java | Parameter | form | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0352-csrf-json/simongomez95.feature) | Not Found |
|37| None | Form | login | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0352-csrf-login/simongomez95.feature) | Not Found |
|38| WebSecurityConfig.java | Cookie | JSESSIONID | Session Fixation | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0384-session-fixation/simongomez95.feature) | Not Found |
|39| HtmlTamperingTask.java | Parameter | total | HTML Tampering | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0472-html-tampering/dyepesatfluid.feature) | Not Found |
|40| JWTSecretKeyEndpoint.java | Parameter | access_token | Insufficiently Protected Credentials | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0522-jwt-signing/badwolf10.feature) | Not Found |
|41| Comments.java | Parameter | XML | Improper Restriction of XML External Entity Reference (XXE) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0611-xxe/simongomez95.feature) | Not Found |
|42| WebSecurityConfig.java | Feature | Session Expiration | Insufficient Session Expiration | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0613-session-expiration/simongomez95.feature) | Not Found |
|43| IDOREditOtherProfile.java | Parameter | userId | Authorization Bypass Through User-Controlled Key | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0639-idor-edit/simongomez95.feature) | Not Found |
|44| IDORViewOtherProfile.java | Parameter | userId | Authorization Bypass Through User-Controlled Key | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0639-idor-view/simongomez95.feature) | Not Found |
|45| ResetLinkAssignment.java | Header | Host | Weak Password Recovery Mechanism for Forgotten Password | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0640-reset-poisoning/simongomez95.feature) | Not Found |
|46| PasswordReset.html | Parameter | securityQuestion | Weak Password Recovery Mechanism for Forgotten Password | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0640-weak-recovery/simongomez95.feature) | Not Found |
|47| Assignment5.java | Code | none | Insufficient Logging | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0778-insufficient-logging/simongomez95.feature) | Not Found |
|48| BypassRestrictions.html | Parameter | fieldn | Client-Side Enforcement of Server-Side Security | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/0788-bypass-front-end-restrictions/dyepesatfluid.feature) | Not Found |
|49| RegistrationController.java | Code | Line 41 | Define secure default options | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/webgoat/1006-if-no-else/simongomez95.feature) | Not Found |