## Version 2.0
## language: en

Feature: elf-x86-keygenme
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Find the serial numbers corresponding to the name : Root-Me

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.4   |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then it is possible to download the challenge
    And provides us with initial information
    """
    System : Debian squeeze.
    ...
    Packages to install : libgtk2.0-0
    ...
    """

  Scenario: Fail:Execute the program
    Given a zip file is delivered
    Then I unpacked it
    And only contained one file "KeygenMe"
    Then I tried to execute it
    And I got the following error
    """
    error while loading shared libraries: libgtk-x11-2.0.so.0:
    cannot open shared object file: No such file or directory
    """
    Then I installed the libgtk2 library
    And I kept getting the same error

  Scenario: Sucess:Execute the program
    Given I cannot run the program
    Then I installed a Debian squeeze VM on VMware to test
    And nothing was still with the same error
    Then I spent a long time trying to fix this
    Then I decided to mount an Ubuntu 18.04.4 desktop VM
    And I found a solution to the error
    """
    https://ubuntuforums.org/showthread.php?t=2378659
    """
    Then after configuring x86 support, I managed to run the binary

  Scenario: Fail:Debug the binary
    Given now I can run the binary [evidences](test.png)
    And I see that it is a program to validate a user and a serial
    Then I decide to debug it
    And once again i failed [evidences](sigsegv.png)
    """
    8049624: got SIGSEGV signal (Segmentation violation) (exc.code b, tid 11181
    """

  Scenario: Sucess:Debug the binary
    Given I manage to run the binary out of a debugger
    Then The first thing I thought is that it uses some anti-debug technique
    And It is indeed so
    """
    rdtsc
    """
    When "rdtsc" is executed, the time of the instructions can be determined
    And with this you can verify if the processor is being stepped
    """
    The Read-Time-Stamp-Counter (RDTSC) instruction can be used by malware
    to determine how quickly the processor executes the program's instructions.
    """
    Then the idea for bypassing is hook all rdtsc calls and alter the jumps
    Then after doing that, I found an interesting sub routine
    """
    mov     ecx, ebp-18h
    add     ebx, 1
    mov     esi, ebp-14h
    imul    ecx, ebp-24h
    imul    esi, ebp-28h
    mov     eax, ebp-18h
    mul     dword ptr ebp-28h
    add     ecx, esi
    add     edx, ecx
    cmp     edi, ebx
    mov     ebp-28h, eax
    mov     ebp-24h, edx
    ja      short loc_8049070
    """
    And what this subroutine does is iterate the user [evidences](byte.png)
    Then multiply a byte by itself as many times as indicated by an accumulator
    And this is basically an exponential function [evidences](gg.png)
    Then I made a python script to generate serials with different users
    And it is this: [evidences](vmelendez.py)
