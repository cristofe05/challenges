#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (node (:conc-name dr-))
  value
  seen
  neighbors
)

(defun add-last(value)
  (setq stack (append stack value))
)

(defun get-last()
  (setq value (first (last stack)))
  (setq stack (subseq stack 0 (- (length stack) 1)))
  (return-from get-last value)
)

(defun load-edge(nodes)
  (setq fth (read))
  (setq sth (read))
  (if (not (gethash fth nodes))
    (progn
      (setf (gethash fth nodes)
        (make-node
          :value fth
          :seen 0
          :neighbors (list)
        )
      )
      (setq nodes-list (append nodes-list (list fth)))
    )
  )
  (if (not (gethash sth nodes))
    (progn
      (setf (gethash sth nodes)
        (make-node
          :value sth
          :seen 0
          :neighbors (list)
        )
      )
      (setq nodes-list (append nodes-list (list sth)))
    )
  )
  (setf (dr-neighbors (gethash fth nodes))
    (append (dr-neighbors (gethash fth nodes)) (list sth)))
)

(defun topological-sort (v)
  (setf (dr-seen (gethash v nodes)) 1)
  (loop for j from 0 to (- (length (dr-neighbors (gethash v nodes))) 1)
    do(topological-sort-aux (nth j (dr-neighbors (gethash v nodes))))
  )
  (add-last (list (string-capitalize v)))
)

(defun topological-sort-aux(v)
  (if (= (dr-seen (gethash v nodes)) 0)
    (topological-sort v)
  )
)

(defun find-order()
  (loop for i from 0 to (- (length nodes-list) 1)
    do(topological-sort-aux (nth i nodes-list))
  )
)

(defun load-input-data (amount-edges nodes)
  (loop for i from 0 to (- amount-edges 1)
    do(load-edge nodes)
  )
)

(defun print-solution()
  (loop
    (format t "~a " (get-last))
    (when (not stack) (return NIL))
  )
)

(defvar amount-edges (read))
(defparameter nodes (make-hash-table :size 10))
(defvar stack '())
(defvar nodes-list '())
(load-input-data amount-edges nodes)
(find-order)
(print-solution)

#|
  cat DATA.lst | clisp bridamo98.lsp
  Hx Er Pp Vq Dg Wz Ez Nm Ne Yp Mm Xh Ue Ik Vl Uf Et Tl Ok Fr Pe Ep
  Xt Fj Pq Fk Ds Yb Ix Qj Oy Wm Uj It Da Ci Gy Ht Dn Fp Ev Zh Sg Aw
  Pg Nd Tn Pz Wp Rs Hb Ca Rm Jo Ww Fw Wj Bn Nl Tm Ft Hi Uu Js Fb Re
  Bc Pl Os Sc Ri Pt Fg Lg Si Zm Yu Tg Tq Aq Mj Ns Ma Kf Ql Zi Ve Gh
  Wu Al Vw Vu Jz Mf Lr Cq Uq Ua Xv Bo Ug Rk Fx Ys Qz Ki Fl Um Rt Lv
  Du Yc Iu Yw Ka Lz Lk Vz Wh Rr Lu Cy Ia Ol Il Pf Py Rb In Rd Am Fs
  Eo Bu Bg Ky Po Va Zt La Vn Go Ag Ea Fa Rp Mk Tz Mb Tt Px Dq Ow Uz
  So Tk
|#
