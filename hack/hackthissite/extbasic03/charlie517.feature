## Version 2.0
## language: en

Feature: hackthissite
  Code:
    BEGIN notr.eal
    CREATE int AS 2
    DESTROY int AS 0
    ANS var AS Create + TO
    out TO
  Site:
    hackthissite
  User:
    charlie517 (wechall)
  Goal:
    Understand what the code does.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Linux Mint      | 4.15.0-45     |
    | Browser         | 65.0          |
  Machine information:
    Given I am accessing hackthissite.
    And get to see the challenge.

  Scenario: Success:
    Given they ask me what does this code return
    And I input 2
    Then I solved the challenge.

