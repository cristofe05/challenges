/*
$ gofmt -w neds.go
$ go vet neds.go
vet.exe: errors: 0; warnings: 0
$ go build -work neds.go
WORK=/tmp/go-build635398477
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "strconv"
)

func convString(el string) float64 {
  number, e := strconv.ParseFloat(el, 64)
  if e != nil {
    fmt.Println(e)
  }
  return number
}

func readData() []float64 {
  var data []float64
  file, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  lines := bufio.NewScanner(file)
  for lines.Scan() {
    data = append(data, convString(lines.Text()))
  }

  if err := lines.Err(); err != nil {
    log.Fatal(err)
  }
  return data
}

func convertNumber(el float64) string {
  a := el * 6
  b := int(a)
  c := b + 1
  return strconv.Itoa(c)
}

func main() {
  data := readData()
  var out string
  for i := 1; i < len(data); i++ {
    out += convertNumber(data[i]) + " "
  }
  fmt.Println(out)
}

/*
./neds
6 3 1 6 6 5 4 1 5 4 4 6 3 4 1 6 6 2 5 4 3 4 5 2
*/
