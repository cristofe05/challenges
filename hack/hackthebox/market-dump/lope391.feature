## Version 2.0
## language: en

Feature: marketDump-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Forensics
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | WireShark       | 2.6.10      |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given A .pcapnpg file
    And A flag format of
    """
    HTB{$FLAG}
    """
    And The instructions:
    """
    We have got informed that a hacker managed to get into our internal network
    after pivoiting through the web platform that runs in public internet.
    He managed to bypass our small product stocks logging platform and then he
    got our costumer database file. We believe that only one of our costumers
    was targeted. Can you find out who the customer was?
    """
    And A packet analyzer in WireShark
    And Its running on Mint 19.3 with kernel 18.04.3

  Scenario: Fail: Did not find the flag
    Given The .pcapnpg
    When I open the file with WireShark using the GUI
    Then I get a dump of all the packages captured during the incident
    When I read through the packages
    Then I see there are mostly TCP packages with a few TELNET packages
    When I filter in only the TELNET packages
    Then I start dumping the data inside the TELNET packages to a text file
    When I read through the data
    Then I find the attacker got into the inventory platform
    And He used default admin credentials [evidence](1.png)
    When I keep reading through the data
    Then I find the attacker opened up a network connection [evidence](2.png)
    But There are no more TELNET packages
    And I conclude the flag is not within the TELNET packages

  Scenario: Success: Found the flag
    Given The .pcapnpg file
    And The information obtained from the Fail Scenario
    When I open the file with WireShark using the GUI
    Then I get a dump of all the packages captured during the incident
    When I filter in only TCP packages after the last TELNET package
    Then I dump the packages data to a text file
    When I read through the text file
    Then I find the attacker set up a python web server
    And Loaded the customers file for download [evidence](3.png)
    When I keep reading the text file
    Then I find the attacker downloaded the customer list [evidence](4.png)
    And I the payload was sent through an HTTP request
    When I read through the payload data
    Then I find a list of 10000 credit card numbers
    But One of them looks more like an encoded string
    When I try to decode the string as base64
    Then I get an invalid decoding
    When I try different type of encodings
    Then I find the string was encoded to base58
    When I input the base58 decoded string as the flag
    Then I solved the challenge
