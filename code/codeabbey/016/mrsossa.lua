--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local A
local i=-1
local sum = {}
for line in data do
    if i==-1 then
        A = tonumber(line)
    else
        local a = 0
        local s = 0
        for token in string.gmatch(line, "[^%s]+") do
            a = a+1
            s = s+tonumber(token)
        end
        s = s/(a-1)
        sum[i] = s
    end
    i = i + 1
end
for c=0, A-1 do
    print(math.floor(sum[c]+0.5))
end

--[[
$ lua mrsossa.lua
6375
231
421
3489
145
407
209
8453
145
974
451
3330
]]
