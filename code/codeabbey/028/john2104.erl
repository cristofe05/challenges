% erlc -W john2104.erl
% erl -compile john2104.erl
% erlint:lint("john2104.erl").
% {ok,[]}

-module(john2104).
-export([start/0]).
-export([readfile/1]).
-export([domath/2]).
-export([decide/1]).
-export([tofloat/1]).
-export([loop/1]).
-export([digest/1]).
-import(lists,[nth/2]).
-export([returnnumbers/1]).


loop(A) ->
  lists:foreach(fun(X) -> digest(X) end, A).


digest(A) ->
  Z = returnnumbers(A),
  L = length(Z),
  domath(Z, L).


domath(_, L) when L == 1 ->
  ok;


domath(A, L) when L > 1 ->
  K = float(nth(1, A)),
  B = float(nth(2, A)),
  Sq = B*B,
  Bm = K/Sq,
  decide(Bm).


decide(Bm) ->
  R = if
        Bm < 18.5 -> under;
        Bm < 25.0 -> normal;
        Bm < 30 -> over;
      true -> obese
      end,
  io:fwrite("~w ", [R]).


tofloat(S) ->
  Z = string:to_float(S),
  Y = element(1, Z),
  if Y == error ->
    string:to_integer(S);
  true -> Z
  end.


returnnumbers(T) ->
  [ element(1, tofloat(Substr)) ||
  Substr <- string:tokens(T, " ")].


readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").


start() ->
  Arr = readfile("DATA.lst"),
  loop(Arr),
  io:fwrite("~n").


% erl -noshell -s john2104 start -s init stop
% over normal under
