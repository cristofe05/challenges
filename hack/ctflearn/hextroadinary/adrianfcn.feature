## Version 2.0
## language: en

Feature: Hextroadinary - Cryptography - ctflearn
  Site:
    https://ctflearn.com/problems/158
  User:
    adrianfcn (ctflearn)
  Goal:
    Generate a secret code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
    | calculator      | 3.30.1-2    |
  Machine information:
    Given I have "0xc4115 0x4cf8" to generate a code

  Scenario: Success: XOR operation
    Given I need to generate a code using "0xc4115 0x4cf8"
    And The pseudonym of the creator of the secret code is
    """
    ROXy, a coder obsessed with being exclusively the worlds best hacker.
    """
    When I reverse "ROXy"
    Then I obtained "XORy"
    And I know that "XOR" is a logical operator
    When I use the XOR operation "c4115 ⊻ 4cf8"
    Then I generate a secret code <flag>
    And I solve the challenge
