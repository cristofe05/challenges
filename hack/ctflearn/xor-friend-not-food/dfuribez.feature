## Version 2.0
## language: en

Feature:
  Site:
    ctflearn
  Category:
    Cryptography
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
  Machine information:
    Given the challenge description
    """
    Here: \t\x1b\x11\x00\x16\x0b\x1d\x19\x17\x0b\x05\x1d(\x05\x005\x1b\x1f\t,
    \r\x00\x18\x1c\x0e

    I think the flag started with: ctflearn{
    """
    And the challenge's name
    """
    XOR Is Friend Not Food
    """

  Scenario: Success:Find the first bytes of the key
    Given I assume the flag was encrypted by XORing a key with the plain text
    And I try to find the first bytes of the key by applying that
    """
    m ^ k = c
    m ^ k ^ m = m ^ m ^ k = 0 ^ k = k
    """
    And "m" is the message, "k" the key and "c" the encrypted text
    When I write a script to automatize this process
    """
    #!/usr/bin/env python

    ENC = b"\t\x1b\x11\x00\x16\x0b\x1d\x19\x17\x0b\x05\x1d(\x05\x005\x1b\x1f"\
      b"\t,\r\x00\x18\x1c\x0e"


    def find_key(enc: int, clear: int) -> int:
      return enc ^ clear


    def main() -> None:
      clear_text = "ctflearn{"
      key = []

      for index, byte in enumerate(clear_text):
        key.append(find_key(ENC[index], ord(byte)))
      print(key)


    main()
    """
    Then I find the first bytes of the key are
    """
    106 111 119 108 115 106 111 119 108
    """

  Scenario: Fail:Concat the key to meet the message length
    Given that this kind of encryption is weak if the key is repeated
    When I try to concat the key to meet the message's length
    """
    #!/usr/bin/env python

    import itertools

    ENC = b"\t\x1b\x11\x00\x16\x0b\x1d\x19\x17\x0b\x05\x1d(\x05\x005\x1b\x1f"\
      b"\t,\r\x00\x18\x1c\x0e"


    def find_key(enc: int, clear: int) -> int:
        return enc ^ clear


    def main() -> None:
        clear_text = "ctflearn{"
        key = []

        for index, byte in enumerate(clear_text):
          key.append(find_key(ENC[index], ord(byte)))

        for c, k in zip(ENC, itertools.cycle(key)):
          print(chr(c ^ k), end="")
        print()


    main()
    """
    Then I get the flag
    """
    ctflearn{ajjDvjZlscCzlkva
    """
    When I try at the page
    """
    ctflearn{ajjDvjZlscCzlkva}
    """
    Then the flag does not work

  Scenario: Success:Find the right flag
    Given that concatenate the flag did not work
    When I try to match the key by shifting the message one position at a time
    """
    #!/usr/bin/env python

    import itertools

    ENC = b"\t\x1b\x11\x00\x16\x0b\x1d\x19\x17\x0b\x05\x1d(\x05\x005\x1b\x1f"\
      b"\t,\r\x00\x18\x1c\x0e"

    def find_key(enc: int, clear: int) -> int:
        return enc ^ clear


    def main() -> None:
        clear_text = "ctflearn{x"
        key = []

        enc_slice = ENC[len(clear_text):]

        for index, byte in enumerate(clear_text):
            key.append(find_key(ENC[index], ord(byte)))

        while len(enc_slice) > 0:
            for c, k in zip(enc_slice, itertools.cycle(key)):
                print(chr(c ^ k), end="")
            print()
            enc_slice = enc_slice[1:]


    main()
    """
    Then I get the output
    """
    ajjDvjZlscCzlkva
    or_is_theFbwtod
    wGrlFqp~@goop}
    BjwYhuf[ajwkb
    ooBwlcCzlrsy
    jZlszFbwtva
    _the_goopd
    ...
    """
    And the message "or_is_theFbwtod" catch my eye
    And I think it should start with a "x" to make any sence
    """
    xor_is_theFbwtod
    """
    When I update the plain text and run again the script
    """
    clear_text = "ctflearn{x"
    """
    Then I get the output
    """
    or_is_the_goop}
    wGrlFqp~@~jwkb
    BjwYhuf[asrsy
    ooBwlcCzlkva
    ...
    """
    When I try the flag
    """
    ctflearn{xor_is_the_goop}
    """
    Then I solve the challenge
