## Version 2.0
## language: en

Feature: ip-tracer
  Site:
    https://ctflearn.com/challenge/686
  User:
    MrSossa (ctflearn)
  Goal:
    Find where the IP Address is located

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox Browser | 71.0(64.bit)|
  Machine information:
    Given a IP Address
    And Search where it was located

  Scenario: Fail: Geolocator
    Given a IP Address
    Then I look for a geolocator in google
    And I found one
    Then I used it to find the location
    And I didn't find the flag

  Scenario: Success: Geolocator
    Given a IP Address
    Then I search for another geolocator
    Then I found it
    And I place the flag CTFlearn{<flag>}
    And I solve the challenge
