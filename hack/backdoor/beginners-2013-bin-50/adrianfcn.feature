## Version 2.0
## language: en

Feature: 2013-BIN-50 - reversing - backdoor
  Site:
    https://backdoor.sdslabs.co/challenges/2013-BIN-50
  User:
    adrianfcn (backdoor)
  Goal:
    Get the flag to challenges

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | strings         | 2.31.1      |
    | sha256sum       | 8.30        |
  Machine information:
    Given I have the binary file "binaary50" on my computer

  Scenario: Success: Looking useful plain text
    Given I can execute the binary "binaary50"
    And I use "strings -d binaary50" for scan the data section in the file
    When I search in the output
    Then I can see several times "Password is" followed by different words
    And I clean a little output
    When I use "strings -d binaary50 |awk '{print $3}' > pP.txt"
    Then I have a file with possible passwords called "pP.txt"
    And I try each one of them
    When I use "for i in $(cat pP.txt); do ./binaary50 $i; done"
    Then I get a MD5 hash
    And I know that the flag is SHA-256 of the MD5 hash obtained
    When I use a "echo -n <MD5 hash>|sha256sum -t"
    Then I get the flag
