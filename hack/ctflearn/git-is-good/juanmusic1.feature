## Version 2.0
## language: en

Feature: Git is good - Forensics - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Forensics
  User:
    JuanMusic1
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu Desktop  | 18.04.3 LTS |
    | Firefox         | 71.0        |
  Machine information:
    Given A link in the challenge page
    """
    https://ctflearn.com/challenge/104
    """
    And the challenge has an archive
    And the challenge's title is contains the word Git

  Scenario: Fail: Get strings inside the file
    When I check folder
    Then I can see a file named "flag.txt"
    And I think this should have strings inside with the flag
    When I use the "cat" command in terminal
    """
    $ cat flag.txt
    """
    Then I get this text
    """
    flag{REDACTED}
    """
    And I try this as flag
    Then I can't solve the challenge

  Scenario: Success: Analysing contains of the folder
    When I check the challenge's title
    Then I think is a folder of a git repository
    And I try a git command for confirm
    """
    $ git log
    """
    Then I get a git log
    """
    commit d10f77c4e766705ab36c7f31dc47b0c5056666bb (HEAD -> master)
    Author: LaScalaLuke <lascala.luke@gmail.com>
    Date:   Sun Oct 30 14:33:18 2016 -0400

        Edited files

    commit 195dd65b9f5130d5f8a435c5995159d4d760741b
    Author: LaScalaLuke <lascala.luke@gmail.com>
    Date:   Sun Oct 30 14:32:44 2016 -0400

        Edited files

    commit 6e824db5ef3b0fa2eb2350f63a9f0fdd9cc7b0bf
    Author: LaScalaLuke <lascala.luke@gmail.com>
    Date:   Sun Oct 30 14:32:11 2016 -0400

    edited files
    """
    When I reset one commit
    """
    $ git reset @~1
    """
    Then I force the file to this commit
    """
    $ git reset --hard
    """
    When I try to get the text in the file
    Then I see the flag "flag{<FLAG>}"
    When I put the flag in the flag format
    """
    CTFlearn{<FLAG>}
    """
    Then I solve the challenge
