#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (game (:conc-name dr-))
  initial-moves
  final-results
)

(defun delimiterp (c)
  (char= c #\Space)
)

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end
  )
)

(defun load-game(games i)
  (setf (aref games i)
    (make-game
      :initial-moves (map 'array #'parse-integer (split (read-line)))
      :final-results (make-array 9)
    )
  )
)

(defun load-input-data (size-input games)
  (loop for i from 0 to (- size-input 1)
    do(load-game games i)
  )
)

(defun copy-array(board)
  (setq copy (make-array '(3 3) :initial-element "-"))
  (loop for i from 0 to 8
    do(setf (aref copy (floor i 3) (mod i 3))
      (aref board (floor i 3) (mod i 3)))
  )
  (return-from copy-array copy)
)

(defun get-current-player(current-turn)
  (if (= (mod current-turn 2) 0)
    (return-from get-current-player "x")
    (return-from get-current-player "o")
  )
)

(defun set-board (initial-moves)
  (setq board (make-array '(3 3) :initial-element "-"))
  (loop for i from 0 to (- (length initial-moves) 1)
    do(setf (aref board (floor (aref initial-moves i) 3)
      (mod (aref initial-moves i) 3))
      (get-current-player i))
  )
)

(defun winner(board)
  (if (or (and (string= (aref board 0 0) "x")
    (string= (aref board 1 1) "x") (string= (aref board 2 2) "x"))
    (and (string= (aref board 2 0) "x") (string= (aref board 1 1) "x")
    (string= (aref board 0 2) "x")))
    (return-from winner "x")
  )
  (if (or (and (string= (aref board 0 0) "o")
    (string= (aref board 1 1) "o") (string= (aref board 2 2) "o"))
    (and (string= (aref board 2 0) "o") (string= (aref board 1 1) "o")
    (string= (aref board 0 2) "o")))
    (return-from winner "o")
  )
  (setq cont 0)
  (loop
    (if (or (and (string= (aref board cont 0) "x")
      (string= (aref board cont 1) "x")(string= (aref board cont 2) "x"))
      (and (string= (aref board 0 cont) "x") (string= (aref board 1 cont) "x")
      (string= (aref board 2 cont) "x")))
      (return-from winner "x")
    )
    (if (or (and (string= (aref board cont 0) "o")
      (string= (aref board cont 1) "o") (string= (aref board cont 2) "o"))
      (and (string= (aref board 0 cont) "o") (string= (aref board 1 cont) "o")
      (string= (aref board 2 cont) "o")))
      (return-from winner "o")
    )
    (setq cont (+ cont 1))
    (when (> cont 2)(return NIL))
  )
)

(defun board-is-full(board)
  (setq i 0)
  (loop
    (if (string= (aref board (floor i 3)(mod i 3)) "-")
      (return-from board-is-full 0)
    )
    (setq i (+ i 1))
    (when (> i 8)(return 1))
  )
)

(defun utility(pos-winner current-player)
  (if pos-winner
    (progn
      (if (string= pos-winner current-player)
        (return-from utility 2)
        (return-from utility 0)
      )
    )
    (return-from utility 1)
  )
)

(defun max-value-action(copy current-player)
  (let ((max-utility (- 0 100))(i-utility 0)(ite 0)(pos-winner (winner copy)))
    (if (or pos-winner (= (board-is-full copy) 1))
      (return-from max-value-action (utility pos-winner current-player))
    )
    (loop
      (if (string= (aref copy (floor ite 3)(mod ite 3)) "-")
        (progn
          (setq board-copy (copy-array copy))
          (setf (aref board-copy (floor ite 3) (mod ite 3)) current-player)
          (setq i-utility (min-value-action board-copy current-player))
          (if (< max-utility i-utility)
            (setq max-utility i-utility)
          )
        )
      )
      (setq ite (+ ite 1))
      (when (> ite 8)(return-from max-value-action max-utility))
    )
  )
)

(defun other-player (current-player)
  (if (string= current-player "x")
    (return-from other-player "o")
    (return-from other-player "x")
  )
)

(defun min-value-action(copy current-player)
  (let ((min-utility 100)(i-utility 0)(ite 0)(pos-winner (winner copy)))
    (if (or pos-winner (= (board-is-full copy) 1))
      (return-from min-value-action (utility pos-winner current-player))
    )
    (loop
      (if (string= (aref copy (floor ite 3)(mod ite 3)) "-")
        (progn
          (setq board-copy (copy-array copy))
          (setf (aref board-copy (floor ite 3) (mod ite 3))
            (other-player current-player))
          (setq i-utility (max-value-action board-copy current-player))
          (if (> min-utility i-utility)
            (setq min-utility i-utility)
          )
        )
      )
      (setq ite (+ ite 1))
      (when (> ite 8)(return-from min-value-action min-utility))
    )
  )
)

(defun minimax (games)
  (setq cont-games 0)
  (loop
    (setq current-player (get-current-player
      (length (dr-initial-moves (aref games cont-games)))))
    (set-board (dr-initial-moves (aref games cont-games)))
    (setq conti 0)
    (loop
      (if (string= (aref board (floor conti 3) (mod conti 3)) "-")
        (progn
          (setq board-copy (copy-array board))
          (setf (aref board-copy (floor conti 3) (mod conti 3))
            current-player)
          (format t "~a" (min-value-action board-copy current-player))
        )
        (format t "0")
      )
      (setq conti (+ conti 1))
      (when (> conti 8)(return NIL))
    )
    (format t " ")
    (setq cont-games (+ cont-games 1))
    (when (> cont-games (- (length games) 1))(return NIL))
  )
)

(defvar size-input (read))
(defparameter games (make-array size-input))
(defparameter board (make-array '(3 3) :initial-element "-"))
(load-input-data size-input games)
(minimax games)

#|
  cat DATA.lst | clisp bridamo98.lsp
  000100000 000000200 000010000 000000000 000110100 000020000 000000210
  000110010 000000200 000000002 000000000 000000002 022000022 002000000
|#
