;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn shift-index
  [x]
  (if (> x 25)
    (- x 26)
    x))

(defn cesar-shift
  [counter index f-vec o-vec]
  (if (> counter 25)
    f-vec
    (let [a (inc counter)
          b (inc index)
          c (conj f-vec (o-vec (shift-index index)))
          d o-vec]
      (cesar-shift a b c d))))

(defn mapping
  [cesar abc counter eval dicc]
  (if (= counter eval)
    (assoc dicc "." ".")
    (let [a cesar
          b abc
          c counter
          d (inc eval)
          e (assoc dicc (cesar eval) (abc eval))]
     (mapping a b c d e))))

(defn result
  [body cesar-map]
  (doseq [x body]
    (doseq [y (str/split x #"")]
      (print (get cesar-map y " ")))
    (print " ")))

(defn solution [head body]
  (let [k (read-string (head 1))
        abc ["A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N"
             "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"]
        cesar (cesar-shift 0 k [] abc)
        cesar-length (- (alength (to-array-2d cesar)) 1)
        cesar-map (mapping cesar abc cesar-length 0 {})]
    (result body cesar-map)))

(defn main []
  (let [[head body] (get-data "DATA.lst")]
    (solution head body)))

(main)

;; CALLED IT THE RISING SUN. THE ONCE AND FUTURE KING. A DAY AT THE RACES.
;; LET HIM THROW THE FIRST STONE. AS EASY AS LYING.
;; THAT ALL MEN ARE CREATED EQUAL.
;; FOUR SCORE AND SEVEN YEARS AGO AND SO YOU TOO BRUTUS.
