#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

Compiling file /Users/Frank/Desktop/frank/054/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/054/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-index()
  (let ((dat))
    (setq dat(read))
  )
)

(defun pythagorean-triples()
  (let ((sn 0) (sn2 0) (m 1) (n 0) (sa 0) (sb 0) (sc 0) (sum 0) (index 0))
    (setq index(get-index))
    (loop for a from 0 to (- index 1)
      do (setq sn(read))
      do (setq sn2(* sn sn))
      (loop for i from m to sn2
        do (loop for j from (+(+ n m) 1) to sn2
          do(setq sa(- (* j j) (* i i)))
          do(setq sb(* j i 2))
          do(setq sc(+ (* j j) (* i i)))
          do(setq sum(+ sa sb sc))
          do(if (= sum sn)
            (print (* sc sc)))
          do(if (= sum sn)
            (setq i sn2))
          do(if (= sum sn)
            (return))
          do(if (> sum sn)
            (return))
        )
      )
    )
  )
)

(pythagorean-triples)

#|
$ cat DATA.lst| clisp frank1030.lsp
74769519481969
86897912829025
84579014989225
60020114980225
56010510456289
43231953160201
76400623155025
37827580070569
|#
