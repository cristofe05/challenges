;; $ clj-kondo --lint villjorge.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns villjorge-144
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (core/slurp core/*in*))
        size (str/split (data 0) #" ")
        values (subvec data 1 (alength (to-array-2d data)))]
    [size
     values]))

(defn egcd [a b]
  (if (= a 0)
    [b 0 1]
    (let [[g x y] (egcd (mod b a) a)]
      [g (- y (* (core/quot b a) x)) x])))

(defn calc_x [M A B]
  (let [[valid a _] (egcd A M)]
    (if (= valid 1)
      (if (< a 0)
        (print (mod (* (- 0 B) (+ a M)) M) "")
        (print (mod (* (- 0 B) a) M) ""))
    (print -1 ""))))

(defn mod_inv [size data]
  (let [M_vec (vec (map (fn [[valx _ _]] (core/read-string valx)) data))
        A_vec (vec (map (fn [[_ valy _]] (core/read-string valy)) data))
        B_vec (vec (map (fn [[_ _ valz]] (core/read-string valz)) data))
        size (vec (map (fn [[val]] (core/read-string val)) size))]
    (loop [x 0]
      (when (< x (core/nth size 0))
         (calc_x (core/nth M_vec x) (core/nth A_vec x) (core/nth B_vec x))
         (recur (+ x 1))))))

(defn main []
  (let [[size values] (get-data)]
    (mod_inv (map #(str/split % #" ") size) (map #(str/split % #" ") values))
    (println)))

(main)

;; $ cat DATA.lst | clj villjorge.cljs
;; 1658128 39031599 18275 -1 33179 30326514 -1 -1 536575 24744627 -1 -1 -1 -1
;; -1 2109734 758150978 16436 -1 14366837 38742 336430 315862220 -1 6237210
;; 214655567 -1 -1
