#!/bin/env bash

# $ shellcheck dfuribez.sh
# No issues detected!

fib () {
    echo "(l($1)/l(10) + 0.34948) / 0.20898" | bc -l  \
    | awk '{print int($1+0.5)}'
}

i=0
while read -r line
do
    if [ $i -eq 1 ] ; then
        fib "$line"
    else
        i=1
    fi
done < "${1:-/dev/stdin}"

# cat DATA.txt | ./dfuribez.sh
# 496
# 440
# 209
# 334
# 450
# 230
# 822
# 939
# 986
# 819
# 618
# 224
# 242
# 19
