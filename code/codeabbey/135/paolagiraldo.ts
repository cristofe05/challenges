/* $ npx eslint paolagiraldo.ts
$ tsc paolagiraldo.ts
*/

const code: { [key: string]: string } = {
  ' ': '11',
  'e': '101',
  't': '1001',
  'o': '10001',
  'n': '10000',
  'a': '011',
  's': '0101',
  'i': '01001',
  'r': '01000',
  'h': '0011',
  'd': '00101',
  'l': '001001',
  '!': '001000',
  'u': '00011',
  'c': '000101',
  'f': '000100',
  'm': '000011',
  'p': '0000101',
  'g': '0000100',
  'w': '0000011',
  'b': '0000010',
  'y': '0000001',
  'v': '00000001',
  'j': '000000001',
  'k': '0000000001',
  'x': '00000000001',
  'q': '000000000001',
  'z': '000000000000',
};

function encodeString(inputData: string): number {
  const char = inputData.split('');
  const encoded = new Map();

  char.forEach((letter: string, index) => {
    encoded.set(index, code[letter]);
    return 'Binary String';
  });

  const encodedSplited =
    Array.from(encoded.values()) || [].join('').match(/.{1,8}/g);
  const final = new Map();
  const hexChar = new Map();
  encodedSplited.forEach((hexLetter: string, index: number): number => {
    const byte = 8;
    const bin = 2;
    const hex = 16;
    if (hexLetter.length < byte) {
      hexChar.set(index, hexLetter.concat('0'.repeat(byte - hexLetter.length)));
    } else {
      hexChar.set(index, hexLetter);
    }

    const hexStr = parseInt(hexChar.get(index), bin).toString(hex);
    if (hexStr.length < 2) {
      const hexStrNew = '0'.concat(hexStr);
      final.set(index, hexStrNew);
    } else {
      final.set(index, hexStr);
    }

    return 0;
  });

  process.stdout.write(`${Array.from(final.values()).join(' ')}`);
  process.stdout.write('\n');
  return 0;
}

function main(): number {
  process.stdin.setEncoding('utf8');
  process.stdin.on('readable', () => {
    const inputData = process.stdin.read();
    if (inputData !== null) {
      encodeString(inputData);
    }
    return 'Data Loaded';
  });
  return 0;
}

main();

/*
$ cat DATA.lst | node paolagiraldo.js
20 28 c1 12 91 72 11 45 8a 0a b8 91 47 27 75 89 37 0a 34 05 8a de 22 62 39 48
41 a6 01 39 3a 1e 98 4c 71 62 18 52 52 e0 2d c1 9a 45 31 63 05 94 34 c1 79 3a
1f 31 d9 33 51 c9 d4 a3 12 28 0e a3 21 40 ac d0 d7 88 99 01 22 03 51 00 ec 27
22 4e e6 95 98 a0 0c 9d 0d 69 20 35 74 c0 06 ac d2 e0 d3 27 85 88 3a 8e 63 93
42 25 49 73 71 22 8c 6b a6 1b 24 9c 56 b5 70 66 e5 63 40 6a 00
 */
