#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun get-character (index letters)
  (setq cont 0)
  (setq cont-index 0)
  (loop
    (if (not (string= (aref letters cont) "-"))
      (progn
        (if (= cont-index index)
          (progn
            (format t "~a" (aref letters cont))
            (setf (aref letters cont) "-")
          )
        )
        (setq cont-index (+ cont-index 1))
      )
    )
    (setq cont (+ cont 1))
    (when (> cont-index index)(return NIL))
  )
)

(defun build-permutation(fact-number cont letters)
  (setq digit 0)
  (loop for i from 0 to (- 11 cont)
    do(get-character 0 letters)
  )
  (loop
    (setq digit (floor fact-number (expt 10 (- cont 1))))
    (get-character digit letters)
    (setq fact-number (- fact-number (* digit (expt 10 (- cont 1)))))
    (setq cont (- cont 1))
    (when (< cont 1)(return cont))
  )
)

(defun calc-nth-permutation (n letters)
  (setq cont 1)
  (setq fact-number 0)
  (setq permutation 0)
  (setq letters (make-array '(12) :initial-contents
    '("A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L")))
  (loop
    (setq remin (floor n cont))
    (setq modu (mod n cont))
    (setq fact-number (+ fact-number (* modu (expt 10 (- cont 1)))))
    (if (= remin 0)
      (build-permutation fact-number cont letters)
    )
    (setq n remin)
    (setq cont (+ cont 1))
    (when (= remin 0)(return NIL))
  )
  (format t " ")
)

(defun solve-all(size-input array-input letters)
  (loop for i from 0 to (- size-input 1)
    do(calc-nth-permutation (aref array-input i) letters)
  )
)

(defun get-input-line(x number array-input)
  (setq number (read))
  (setf (aref array-input x) number)
)

(defvar letters (make-array '(12) :initial-contents
  '("A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L")))

(defvar size-input (read))
(defparameter array-input (make-array size-input))
(defvar number 0)

(loop for x from 0 to (- size-input 1)
  do(get-input-line x number array-input)
)

(solve-all size-input array-input letters)

#|
  cat DATA.lst | clisp bridamo98.lsp
  LAGDFICJKHBE LGDIFKEBJCHA JIFDHLCAKBGE DFABGHLJKIEC AFDBGJELKHCI
  EHAKFBDCIJGL FALIEJDBCGHK KCEGJLHDAIBF FKIDCLGJAHEB EKBCLIJAHDFG
  JGKHILCDABEF ICKJGBHDEALF GJCKABEHDFIL JAEKBDHICFGL FJCLGKIAEBDH
  GFBDCAKLEIHJ GAECHDKBFLIJ CAHIKBJDGELF AEFIDJKBGCHL CDHEBLIKAJFG
  LDJEKHAFGBIC DFJLEHICKBAG AJIFDHCBGELK
|#
