;; clj-kondo --lint ludsrill.cljs
;; linting took 71ms, errors: 0, warnings: 0

(ns ludsrill.076
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn organize-data [data]
  (when (seq data)
      (cons (map (fn [x] (str/split x #"")) (first data))
       (organize-data (rest data)))))

(defn find-path [data movement]
  (when (seq data)
    (let [next (list (nth movement 2) (nth movement 3))
          next-movement (first data)]
      (if (and (= (first next-movement) (first next))
               (= (second next-movement) (second next)))
          (cons next-movement (find-path (rest data) next-movement))
          (find-path (rest data) movement)))))

(defn path-pawn [data]
  (let [movement (first data)]
    (when (seq data)
        (if (or (= (Integer. (second movement)) 2)
            (= (Integer. (second movement)) 7))
            (cons (cons movement (find-path (rest data) movement))
              (path-pawn (rest data)))
            (path-pawn (rest data))))))

(defn simplifing-path [data]
  (when (seq data)
    (let [aux (first data)]
      (concat (list (list (nth aux 0) (nth aux 1))
        (list (nth aux 2) (nth aux 3))) (simplifing-path (rest data))))))

(defn white-pawn [item]
  (when (seq item)
  (let [org-movement (first item)
        movement (simplifing-path (list (first item)))
        initial (first movement)
        final (second movement)
        pos-initial (Integer. (second initial))
        pos-final (Integer. (second final))]

    (if (= (first initial) (first final))
        (if (or (= (- pos-final pos-initial) 1)
                (= (- pos-final pos-initial) 2))
            (if (and (= (- pos-final pos-initial) 2) (= pos-initial 2))
                (white-pawn (rest item))
                (if (and (= (- pos-final pos-initial) 1))
                    (white-pawn (rest item))
                    [org-movement]))
            [org-movement])

        (if (or (= (+ (int (.charAt (first initial) 0)) 1)
                  (int (.charAt (first final) 0)))
                (= (- (int (.charAt (first initial) 0)) 1)
                  (int (.charAt (first final) 0))))

            (if (= (- pos-final pos-initial) 1)
                (white-pawn (rest item))
                [org-movement])
            [org-movement])))))

(defn black-pawn [item]
  (when (seq item)
  (let [org-movement (first item)
        movement (simplifing-path (list (first item)))
        initial (first movement)
        final (second movement)
        pos-initial (Integer. (second initial))
        pos-final (Integer. (second final))]

    (if (= (first initial) (first final))
        (if (or (= (- pos-initial pos-final) 1)
                (= (- pos-initial pos-final) 2))
            (if (and (= (- pos-initial pos-final) 2) (= pos-initial 7))
                (black-pawn (rest item))
                (if (and (= (- pos-initial pos-final) 1))
                    (black-pawn (rest item))
                    [org-movement]))
            [org-movement])

        (if (or (= (+ (int (.charAt (first initial) 0)) 1)
                  (int (.charAt (first final) 0)))
                (= (- (int (.charAt (first initial) 0)) 1)
                  (int (.charAt (first final) 0))))

            (if (= (- pos-initial pos-final) 1)
                (black-pawn (rest item))
                [org-movement])

            [org-movement])))))

(defn only-forward [path item]
  (when (seq path)
    (if (= (second (first item)) "2")
        ((fn [x] (if (nil? x) (only-forward (rest path) (first (rest path)))
         (first x))) (white-pawn item))
        ((fn [x] (if (nil? x) (only-forward (rest path) (first (rest path)))
         (first x))) (black-pawn item)))))

(defn aux-flat [item]
  (when (seq item)
    (cons (first item) (aux-flat (rest item)))))

(defn flat [path]
  (when (seq path)
    (concat (aux-flat (first path)) (flat (rest path)))))

(defn check-capture [item all-movements]
  (let [all-movements-simplified (simplifing-path all-movements)
        captured (list (nth item 2) (nth item 3))
        past (+ (.indexOf all-movements-simplified captured) 1)]
    (when (< (/ past 2) (+ (.indexOf all-movements item) 1))
        (if (< (Integer. (second item)) 5)
            (if (> (Integer. (second (nth all-movements (- (/ past 2) 1)))) 4)
                nil
                (first [item]))
            (if (< (Integer. (second (nth all-movements (- (/ past 2) 1)))) 5)
                nil
                (first [item]))))))

(defn capture [all-movements path]
  (when (seq path)
    (let [item (first path)]
        (if (or (= (+ (int (.charAt (first item) 0)) 1)
                  (int (.charAt (nth item 2) 0)))
                (= (- (int (.charAt (first item) 0)) 1)
                  (int (.charAt (nth item 2) 0))))
            (if (= (Math/abs (- (Integer. (second item))
                    (Integer. (nth item 3)))) 1)
                ((fn [x] (if (nil? x) (capture all-movements (rest path))
                 (first [x]))) (check-capture item all-movements))
                (capture all-movements (rest path)))
            (capture all-movements (rest path))))))

(defn check-colission [all-movements fla-path]
  (when (seq fla-path)
  (let [first-value (first fla-path)
        pos-first-value (.indexOf all-movements first-value)
        check-final (.indexOf (simplifing-path all-movements)
                      (list (nth first-value 2) (nth first-value 3)))
        white-pos (.indexOf (simplifing-path all-movements)
                    (list (nth first-value 2) "3"))
        black-pos (.indexOf (simplifing-path all-movements)
                    (list (nth first-value 2) "6"))]

    (if (and (= (first first-value) (nth first-value 2))
             (= (Math/abs (- (Integer. (second first-value))
                (Integer. (nth first-value 3)))) 2))

        (if (= (second first-value) 7)
            (if (and (odd? black-pos) (> (* pos-first-value 2) black-pos))
              (first [first-value])
              (check-colission all-movements (rest fla-path)))

            (if (and (odd? white-pos) (not= white-pos (- 1))
                     (> (* pos-first-value 2) white-pos))
              (first [first-value])
              (check-colission all-movements (rest fla-path))))

      (if (and (> (* pos-first-value 2) check-final) (odd? check-final)
               (= (first first-value) (nth first-value 2)))
          (first [first-value])
          (check-colission all-movements (rest fla-path)))))))

(defn pawn-move-validator [item]
  (let [forward (only-forward (path-pawn item) (first (path-pawn item)))
        capt (capture item (flat (path-pawn item)))
        colission (check-colission item (flat (path-pawn item)))
        final (distinct (remove nil? (list forward capt colission)))]

      (if (nil? final)
          (print "0 ")
          (print (core/format "%s " (+ (.indexOf item (first final)) 1))))))

(defn -main []
  (let [data (rest (get-data))
        organized-data (organize-data data)]
    (doseq [item organized-data]
      (pawn-move-validator item))))

(-main)

;; cat DATA | clj ludsrill.cljs
;; 3 3 0 3 4 6 5 4 0 0 6 2 5
