--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local case
local d = -1
local result = {}
for line in data do
    if d == -1 then
        case = line
    else
        local vowel = 0
        for i in line:gmatch"." do
            if (i=="a" or i=="e" or i=="i" or i=="o" or i=="u" or i=="y") then
                vowel = vowel +1
            end
        end
        result[d] = vowel
    end
    d = d + 1
end
for c = 0, case-1 do
    print (result[c])
end

--[[
$ lua mrsossa.lua
10
11
15
14
17
11
10
12
6
6
5
8
11
10
16
9
]]
