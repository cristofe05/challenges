/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn calc(n1: u32, n2: u32) {
  let d1 = n1 % 6;
  let d2 = n2 % 6;
  print!("{} ", d1 + d2 + 2u32);
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index > 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      let n1: u32 = params[0].parse().unwrap_or(0);
      let n2: u32 = params[1].parse().unwrap_or(0);
      calc(n1, n2);
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
8 9 8 7 5 9 3 4 6 8 3 3 7 10 3 10 9 8 8 2 9 8 6 6 10 7 10 9
*/
