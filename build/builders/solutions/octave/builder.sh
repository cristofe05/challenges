source "${srcGeneric}"

function compile {
  local solution_path="${1}"
  local HOME='.'

      pushd "${solution_path}" || return 1 \
  &&  octave \
        --eval \
        "${solutionFileName%.*}" \
  &&  popd || return 1
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  compile 'root/src'
}

build || exit 1
echo > "${out}"
