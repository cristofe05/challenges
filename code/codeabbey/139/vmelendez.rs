/*
  $ rustmft vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn balance(mass: i64) -> i64 {
  let mut init = 1;
  while init < mass {
    init *= 3;
  }

  if init == mass {
    return 1;
  }

  let check_half = init / 2;
  if check_half > mass {
    return 1 + balance(mass - init / 3);
  }

  if check_half < mass {
    return 1 + balance(init - mass);
  }

  return 1;
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  std::io::stdin().read_line(&mut n).unwrap();

  let input: Vec<i64> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i64>().unwrap())
      .collect()
  };

  for i in 0..input.len() {
    print!("{} ", balance(input[i]));
  }

  Ok(())
}

/*
  $ cat DATA.lst | .\vmelendez.exe
  8 11 13 7 5 8 10 11 14 13 8 2 5 10 10 12 13 14 14 5 13
*/
