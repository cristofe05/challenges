;; $ clj-kondo --lint bridamo98.cljs
;; linting took 25ms, errors: 0, warnings: 0

(ns bridamo98-020
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn is-vowel [lett]
  (if (or (= (compare lett "a") 0)
  (= (compare lett "e") 0)(= (compare lett "i") 0)
  (= (compare lett "o") 0)(= (compare lett "u") 0)
  (= (compare lett "y") 0)) 1 0))

(defn count-vowels[sentence]
  (loop [i 0 res 0]
    (if (< i (count sentence))
    (recur (+ i 1) (+ res (is-vowel (subs sentence i (+ i 1))))) res)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
      (str result (count-vowels (core/read-line)) " ")) result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 11 14 13 19 5 14 15 9 8 8 6 8 8 7 9
