#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/134/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/134/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun get-position()
  (let ((w 0) (h 0) (l 0) (r1 0) (r2 0) (x 1) (y 1))
    (setq w(get-dat))
    (setq h(get-dat))
    (setq l(get-dat))
    (loop for i from 0 to 100
      do (if (or (> (+ r1 l) w) (< r1 0))
        (progn
          (setq x(* -1 x))
          (setq r1(+ r1 (+ x x)))
        )
      )
      do (if (or (>= r2 h) (< r2 0))
        (progn
          (setq y(* -1 y))
          (setq r2(+ r2 (+ y y)))
        )
      )
      do (format t "~a ~a " r1 r2)
      do (setq r1(+ r1 x))
      do (setq r2(+ r2 y))
    )
  )
)

(get-position)

#|
$ cat DATA.lst | clisp frank1030.lsp

0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9
10 10 11 11 12 12 13 13 14 14 15 15 16 14
17 13 18 12 19 11 20 10 21 9 22 8 23 7 24
6 25 5 26 4 27 3 28 2 29 1 30 0 31 1 30 2
29 3 28 4 27 5 26 6 25 7 24 8 23 9 22 10
21 11 20 12 19 13 18 14 17 15 16 14 15 13
14 12 13 11 12 10 11 9 10 8 9 7 8 6 7 5 6
4 5 3 4 2 3 1 2 0 1 1 0 2 1 3 2 4 3 5 4 6
5 7 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13
15 14 14 15 13 16 12 17 11 18 10 19 9 20 8
21 7 22 6 23 5 24 4 25 3 26 2 27 1 28 0 29
1 30 2 31 3 30 4 29 5 28 6 27 7 26 8 25 9 24 10
|#
