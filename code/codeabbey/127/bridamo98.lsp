#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun order-array(array)
  (sort array #'string<)
)

(defun add-word(word words-db)
  (if (gethash word words-db)
    (setf (gethash word words-db) (+ (gethash word words-db) 1))
    (setf (gethash word words-db) 0)
  )
)

(defun load-db(words-db filename)
   (let ((in (open filename :if-does-not-exist nil)))
    (when in
      (loop for line = (read-line in nil)
        while line do (add-word (order-array line) words-db)
      )
      (close in)
    )
  )
)

(defun solve-problem (word words-db)
  (setq word (order-array word))
  (if (gethash word words-db)
    (format t "~a "(gethash word words-db))
    (format t "0 ")
  )
)

(defun solve-problem-words(words-db)
  (let ((size-input (read)))
    (loop for i from 0 to (- size-input 1)
      do(solve-problem (read-line) words-db)
    )
  )
)

(defun find-anagrams(words-db problem-words)
  (let ((c 0))
    (loop
      (if (gethash (aref words-db c) problem-words)
        (format t "entra~%")
      )
      (setq c (+ c 1))
      (when (> c (- (length words-db) 1)) (return NIL))
    )
  )
)

(defparameter words-db (make-hash-table :test 'equal))
(load-db words-db "words.lst")
(solve-problem-words  words-db)

#|
  cat DATA.lst | clisp bridamo98.lsp
  3 3 4 3 4 3 5
|#
