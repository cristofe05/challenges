## Version 2.0
## language: en

Feature: headache
  Site:
    Hackthebox
  Category:
    Reversing
  User:
    lkkpp
  Goal:
    Get the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04.4   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Windows         | 10        |
    | Python          | 2.7       |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see the following sentence
    """
    make the flag.
    """
    And it is possible to download the challenge

  Scenario: Fail: Analyze the binary statically
    Given a binary is provided
    Then I analyze it with IDA [evidences](ida.png)
    And I realize that IDA does not resolve the name of the functions
    When this indicates that it has been stripped
    And IDA finds it difficult to analyze it [evidences](main.png)

  Scenario: Fail: Analyze the binary dynamically (debugging)
    Given IDA does not correctly resolve the binary
    Then my idea is understand it by debugging
    And the first thing I do is run it with GDB
    """
    $ gdb headache
    GNU gdb (Ubuntu 8.1-0ubuntu3.2) 8.1.0.20180409-git
    Copyright (C) 2018 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
    and "show warranty" for details.
    This GDB was configured as "x86_64-linux-gnu".
    Type "show configuration" for configuration details.
    For bug reporting instructions, please see:
    <http://www.gnu.org/software/gdb/bugs/>.
    Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.
    For help, type "help".
    Type "apropos word" to search for commands related to "word"...
    pwndbg: loaded 192 commands. Type pwndbg [filter] for a list.
    pwndbg: created $rebase, $ida gdb functions (can be used with print/break)
    Reading symbols from headache...(no debugging symbols found)...done.
    pwndbg>
    """
    When tracing between instructions I hook a syscall (sys_ptrace)
    """
    0x555555555284    mov    rdi, 1
    0x55555555528b    mov    rdx, 0x64
    0x555555555292    xor    rax, rdx
    0x555555555295    xor    rdi, rdi
    0x555555555298    xor    rdx, rdx
    0x55555555529b    syscall  <SYS_ptrace>
    """ [evidences](ptrace.png)
    And this syscall is used in anti debug techniques
    When it determines the flow of program execution [evidences](check.png)
    And sys_ptrace returns -1 (0xFFFFFFFF)
    Then the program generates a flag and compares it with the entered value
    And the way it generates the flag is XORing two arrays:
    """
    xor_values = [0x2a, 0x31, 0x31, 0xf, 0x1c, 0x55, 0xe,
                  0x3a, 0x02, 0x0d, 0x46, 0x12, 0x1c, 0x2d, 0x11, 0x55,
                  0x51, 0x5c, 0x14, 0x19]
    key_values = "bestkeyeverforrealxd"
    """ [evidences](keyinmemory.png)(xor.png)(xorarray.png)
    Then I made a script to resolve the flag with Python
    """
    flag = ''
    for i in range(20):
        flag += chr(xor_values[i] ^ ord(key_values[i]))

    print flag
    """
    And I got a fake flag: "HTB{w0*_th**_c***l}"

  Scenario: Fail: Finding the correct flag
    Given the generation of the flag is given by the ptrace return
    When my idea is to bypass the antidebug
    And I did that initially by modifying the return
    """
    pwndbg> set *((int *) return_of_ptrace) = 0
    """
    Then I see a routine that generates a flag
    And using the value 0x61 in an XOR operation with another array
    """
    xor_values = [0x29, 0x35, 0x23, 0x1a, 0x15, 0x09,
                  0x55, 0x15, 0x3e, 0x16, 0x55, 0x12, 0x3e, 0x09,
                  0x55, 0x13, 0x05, 0x1c, 0x00, 0xff]
    """ [evidences](xorvalues2.png)
    Then I made a python script to solve the routine:
    """
    xor_values = [0x29, 0x35, 0x23, 0x1a, 0x15, 0x09,
                  0x55, 0x15, 0x3e, 0x16, 0x55, 0x12, 0x3e, 0x09,
                  0x55, 0x13, 0x05, 0x1c, 0x00, 0xff]

    flag = ''
    for i in range(20):
        flag += chr(xor_values[i] ^ 0x61)

    print flag
    """
    And again I found a fake flag: [evidences](fakeflag2.png)

  Scenario: Success: Resolving flag in memory
    Given the routines that generate the flags are incorrect
    When my idea is to go deeper
    And analyze each internal function as ptrace, putchar, etc
    Then analyzing a couple of functions I found:
    """
    mprotect
    """ [evidences](mprotect.png)(infomprotect.png)
    And this syscall is in charge of manipulating the permissions of the memory
    Then It is then used to make the data section executable
    And from there generate the flag [evidences](values.png)
    Then generation of the flag is given by a custom cryptographic algorithm
    """
    [evidences](custom.png)
    """
    And since it is compared with the entered values
    Then I can get the values once they have been decrypted
    And get the correct flag byte by byte [evidences](onebyteflag.png)
    Then it was just to put them all together
    And get the correct flag [evidences](flagbytes.png)(flag.png)
