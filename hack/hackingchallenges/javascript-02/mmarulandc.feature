## Version 2.0
## language: en

Feature: javascript-2 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    mmarulandc
  Goal:
    Enter the correct password

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Windows OS      | 10           |
    | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    And the site has one password field

  Scenario: Success:Finding values in javascript
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
      """
      function pwd() {
        passwort=unescape("%52%69%63%68%74%69%67");
        if (passwort== document.hackit.eingabe.value) {
          alert ("Super das Passwort ist richtig!");
        }
        else {
          alert("Das Passwort ist Falsch!");
        }
      }
      """
    And I find out that there is a conditional blocking the access
    And I need to find the right value for the variable
    When I ran the following line of code
      """
      console.log(unescape("%52%69%63%68%74%69%67"));
      """
    Then I find the value for the password
    When I enter the password
    And the form is submitted
    Then a success message is displayed
    And I solve the challenge
