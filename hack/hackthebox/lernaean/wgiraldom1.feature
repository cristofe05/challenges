## Version 2.0
## language: en

Feature: Lernaean - HackTheBox
  Site:
    www.hackthebox.eu
  User:
    williamgiraldo
  Goal:
    Find the password from an inexpert user

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | PHP               |    7.1.1   |
  Machine Information:
    Given this is a web-based challenge,
    When I get to the challenge page
    Then I will inspect its source and resources

  Scenario: Fail:load-challenge
    Given I start the challenge
    And it prompts
    """
    Your target is not very good with computers.
    Try and guess their password to see if they may be hiding anything!
    """
    And a small web page loads, prompting me not to guess its password.

  Scenario: Fail: inspect-source-code
    Given I get to the web page
    When I see its souce
    Then I can not find anything useful.

  Scenario: Fail: come-on-he-is-a-novice
    When I remember the user is not very skilled with computers.
    Then I think about a brute force attack.
    When I try some passwords and they all fail,
    And I got no more options,
    Then I think I must resort to a bigger password DB.

  Scenario: Success: big-database
    Given I develop a small script to bruteforce the web page
    And the script fetches passwords from a big DB.
    When the script has completed less than 1/4 of the file,
    Then it comes up with the password "leonardo".
    And it yields the flag [evidence](img1.png)
