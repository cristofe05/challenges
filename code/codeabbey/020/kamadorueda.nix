/*
 *  training $ /nix/store/*nix-linter-0.2.0.0/bin/nix-linter \
 *      -W AlphabeticalArgs \
 *      -W BetaReduction  \
 *      -W DIYInherit \
 *      -W EmptyInherit \
 *      -W EmptyLet \
 *      -W EmptyVariadicParamSet \
 *      -W EtaReduce \
 *      -W FreeLetInFunc \
 *      -W LetInInheritRecset \
 *      -W ListLiteralConcat \
 *      -W NegateAtom \
 *      -W SequentialLet \
 *      -W SetLiteralUpdate \
 *      -W UnfortunateArgName \
 *      -W UnneededAntiquote \
 *      -W UnneededRec \
 *      -W UnusedArg \
 *      -W UnusedLetBind \
 *      -W UpdateEmptySet \
 *    --recursive ./code/codeabbey/020
 *
 */


with builtins;

let
  pkgs = import <nixpkgs> { };
  inputData = readFile ./DATA.lst;
in

with pkgs.lib.strings;
with pkgs.lib.lists;

let
  # Transform the input into a list of strings
  inputLines = splitString "\n" inputData;

  # Discard the first line (it's the number of inputs)
  _problemLines = tail inputLines;

  # Discard the last line (it's empty)
  problemLines = reverseList (tail (reverseList _problemLines));

  # Function to solve a single problem
  solve = string:
    let
      # Turn a string into a list of chars
      chars = stringToCharacters string;

      # True if char is vowel
      isVowel = c:
           (c == "a")
        || (c == "e")
        || (c == "i")
        || (c == "o")
        || (c == "u")
        || (c == "y");
    in
      # Count the length of a string consisting in only vowels
      length (filter isVowel chars);

  # Solve every problem in the problem lists
  solutions = map solve problemLines;
  solutionsAsString = map toString solutions;

  # Separate solutions with an space
  solution = concatStringsSep " " solutionsAsString;
in
  pkgs.stdenv.mkDerivation rec {
    name = "vowel-count";
    buildInputs = [ ];
    builder = toFile "builder.sh" ''
      echo ${solution} > $out
    '';
  }


/*
 * $ cat $(nix-build ./code/codeabbey/020/kamadorueda.nix)
 *
 *   10 11 15 14 17 11 10 12 6 6 5 8 11 10 16 9
 *
 */
