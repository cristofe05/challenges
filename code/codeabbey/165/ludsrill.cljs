;; clj-kondo --lint ludsrill.cljs
;; linting took 71ms, errors: 0, warnings: 0

(ns ludsrill.165
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn perform-simulation-step [rate dt mass mass-fuel altitute speed]
  (let [exhaust-speed 2800
        moon-gravity 1.622
        moon-radius 1737100
        altitute (- altitute (* speed dt))
        total-mass (+ mass mass-fuel)
        dm ((fn [x] (if (> x 0) (* dt rate) 0)) mass-fuel)
        dv (* exhaust-speed (/ dm total-mass))
        mass-fuel ((fn [mass-fuel dm]
                    (if (>= (- mass-fuel dm) 0) (- mass-fuel dm) 0))
                     mass-fuel dm)
        gravity-acceleration (/ (* moon-gravity (* moon-radius moon-radius))
                              (* (+ moon-radius altitute)
                                 (+ moon-radius altitute)))
        speed (- (+ speed (* gravity-acceleration dt)) dv)]

    (list mass mass-fuel altitute speed)))

(defn CPU-on-board [item rate]

  (let [mass (edn/read-string (str (first item)))
        mass-fuel (edn/read-string (str (second item)))
        altitute (edn/read-string (str (nth item 2)))
        speed (edn/read-string (str (nth item 3)))]

    (loop [x 0 rate rate dt 0.1 mass mass mass-fuel mass-fuel altitute altitute
          speed speed memory 0]

      (let [group (perform-simulation-step (edn/read-string (str (first rate)))
                   dt mass mass-fuel altitute speed)
            memory ((fn [x] (if (> x 0) (nth group 3) memory)) (nth group 2))
            mass (first group)
            mass-fuel (second group)
            altitute (nth group 2)
            speed (nth group 3)]

        (if (< x 99)
          (if (> altitute 0)
            (recur (inc x) rate dt mass mass-fuel altitute speed memory)
            (println memory))
          (if (> altitute 0)
            (if (> (count rate) 1)
              (CPU-on-board (list mass mass-fuel altitute speed) (rest rate))
              (CPU-on-board (list mass mass-fuel altitute speed) (list '0)))
            (println memory)))))))

(defn -main []
(let [data (get-data)]
  (CPU-on-board (first data) (second data))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 606.2253762340662
