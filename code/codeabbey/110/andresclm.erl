% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  {N,_} = string:to_integer(hd(Data)),
  {B,_} = string:to_integer(tl(Data)),
  io:format("~B\n",[count_luck(N,B)]).

count_luck(N, B)->
  Nhalf = N div 2,
  lists:foldl(fun(S, Accumulator) ->
    Result = calculate_an(S,Nhalf,B),
    Accumulator + Result*Result
    end,
    2,
    lists:seq(1,Nhalf*(B-1)-1)
  ).

calculate_an(S,N,B) ->
  Kmax = S div B,
  lists:foldl(fun(K, Accumulator) ->
    C1 = erlang:round(math:pow(-1, K)),
    C2 = fac(S-K*B+N-1),
    C3 = fac(S-K*B),
    C4 = fac(N-K)*fac(K),
    C = (C1*C2*N) div (C3*C4),
    Accumulator + C
    end,
    0,
    lists:seq(0,Kmax)
  ).

fac(N) -> fac(N,1).
fac(N,Acc) when N =<0 -> Acc;
fac(N,Acc) when N > 0 -> fac(N-1,N*Acc).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [_|Data] = string:lexemes(erlang:binary_to_list(Binary), "\n"),
  string:lexemes(Data," ").

% $ erl -noshell -s andresclm -s init stop
% 9858827562998228784995004925
