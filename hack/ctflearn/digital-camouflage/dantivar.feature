## Version 2.0
## language: en

Feature: Digital Camouflage - Forensics - ctflearn
  Site:
    https://ctflearn.com/challenge/237
  User:
    dantivar
  Goal:
    Get flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          | 80.0.3987.122|
    | Wireshark       |    2.6.7     |
    | base64          |    8.26      |
  Machine information:
    Given A file "data.pcap"

  Scenario: Success: Wireshark Forensics
    Given The file I opened it with Wireshark
    Then I look for a "POST" request
    When I expand the information about this request I find a user
    And a password ending in "=="
    Then I suspect it is encoded
    And I try to decode it using "base64"
    And That is the flag
