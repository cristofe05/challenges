;$ clj-kondo --lint frank1030.cljs
;linting took 211ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn get-position [a b c d e f g i]
  (let [w a h b l c r1 d r2 e x f y g]
    (if (>= @i 0)
      (do
        (swap! i dec)
        (if (or (> (+ r1 l) w) (< r1 0))
          (print (+ r1 (+ (* -1 x) (* -1 x))) "")
          (print r1 ""))
        (if (or (>= r2 h) (< r2 0))
          (print (+ r2 (+ (* -1 y) (* -1 y))) "")
          (print r2 ""))
        (if (and (or (> (+ r1 l) w) (< r1 0)) (or (>= r2 h) (< r2 0)))
          (get-position w h l (+ r1 (+ (* -1 x) (* -1 x)) (* -1 x))
            (+ r2 (+ (* -1 y) (* -1 y)) (* -1 y)) (* -1 x) (* -1 y) i)
          (print ""))
        (if (or (> (+ r1 l) w) (< r1 0))
          (get-position w h l (+ r1 (+ (* -1 x) (* -1 x)) (* -1 x))
            (+ r2 y) (* -1 x) y i)
          (print ""))
        (if (or (>= r2 h) (< r2 0))
          (get-position w h l (+ r1 x) (+ r2 (+ (* -1 y) (* -1 y))
            (* -1 y)) x (* -1 y) i)
          (print ""))
        (print "")
        (get-position w h l (+ r1 x) (+ r2 y) x y i))
      (print ""))))

(defn main []
  (let [w (core/read) h (core/read) l (core/read) i (atom 100)]
    (get-position w h l 0 0 1 1 i)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9
;10 10 11 11 12 12 13 13 14 14 15 15 16 14
;17 13 18 12 19 11 20 10 21 9 22 8 23 7 24
;6 25 5 26 4 27 3 28 2 29 1 30 0 31 1 30 2
;29 3 28 4 27 5 26 6 25 7 24 8 23 9 22 10
;21 11 20 12 19 13 18 14 17 15 16 14 15 13
;14 12 13 11 12 10 11 9 10 8 9 7 8 6 7 5 6
;4 5 3 4 2 3 1 2 0 1 1 0 2 1 3 2 4 3 5 4 6
;5 7 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13
;15 14 14 15 13 16 12 17 11 18 10 19 9 20 8
;21 7 22 6 23 5 24 4 25 3 26 2 27 1 28 0 29
;1 30 2 31 3 30 4 29 5 28 6 27 7 26 8 25 9 24 10
