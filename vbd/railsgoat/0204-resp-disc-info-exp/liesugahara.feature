## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    Broken Authentication and Session Management
  Location:
    http://localhost:3000/login - Email
  CWE:
    CWE-204: Response Discrepancy Information Exposure
  Rule:
    REQ.063: https://fluidattacks.com/web/rules/063/
    REQ.142: https://fluidattacks.com/web/rules/142/
  Goal:
    Get administrator email.
  Recommendation:
    Restrict access by validating current user's ID.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 60.7.2      |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I fill out the form
    Then I can log in.

  Scenario: Static detection
    When I look at the user model in "app/models/"
    Then I can see different alerts for wrong password and wrong email
    And no there is no attepts limit
    And it can be found in in method self.authenticate from line 48 to 58.
    """
    48 def self.authenticate(email, password)
    49   auth = nil
    50   user = find_by_email(email)
    51   raise "#{email} doesn't exist!" if !(user)
    52   if user.password == Digest::MD5.hexdigest(password)
    53     auth = user
    54   else
    55     raise "Incorrect Password!"
    56   end
    57   return auth
    58 end
    """
    When I look at the sessions controller in "app/controllers/"
    Then I see that the create method rescues the exception raised previously
    And renders it as a flash alert
    And it can be found from line 10 to 28.
    """
    10  def create
    11    path = params[:url].present?
          ? params[:url] : home_dashboard_index_path
    12    begin
    13      user = User.authenticate(params[:email].to_s.downcase,
            params[:password])
    14      rescue Exception => e
    15    end
    16
    17    if user
    18      if params[:remember_me]
    19        cookies.permanent[:auth_token] = user.auth_token
    if User.where(:user_id => user.user_id).exists?
    20      else
    21        session[:user_id] = user.user_id
              if User.where(:user_id => user.user_id).exists?
    22      end
    23      redirect_to path
    24    else
    25      flash[:error] = "Your email or password is incorrect"
    26      render "new"
    27    end
    28  end
    """
    Then I can conclude that I can know if an email exists in the DB.

  Scenario: Dynamic detection
    Given I access http://localhost:3000
    When I try to log in with a random email
    Then I see that the email doesn't exist [evidence](error-msg.png)
    And I can conclude that I can know if an email exists in the DB.

  Scenario: Exploitation
    Given I can can see the company name is MetaCorp or GoatGroup
    And from the detection I know I can get the admin email through brute force
    When I start testing for valid emails
    Then I see that when I type "admin@metacorp.com" seems to be a valid email
    And now I get the "Incorrect Password" alert [evidence](passwd-error.png)
    And I can conclude that I can get an admin email through brute force.

  Scenario: Remediation
    Given I have patched the code by removing the error message displayed
    And added the following to the sessions_controller.rb in line 26
    """
    26 flash[:error] = "Your email or password is incorrect"
    """
    When I type in a random email and password
    Then I can't know which one is incorrect [evidence](new-msg.png)
    And I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - CR:H/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-17
