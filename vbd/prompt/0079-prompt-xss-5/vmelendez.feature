## Version 1.4.1
## language: en

Feature:
  TOE:
    Prompt
  Category:
    Cross-Site Scripting
  Location:
    http://prompt.ml/5
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see a sandbox environment to interact with the site
    And two sections to see the site's responses [evidences](site.png)

  Scenario: Normal use case
    Given I access the site
    Then I can enter something in the input box
    And it will be reflected in the sandbox web viewer [evidences](normal.png)

  Scenario: Static detection
    Given the site provides an area called "Text Viewer"
    And which is basically the source code of the relevant function
    Then I could analyze this function
    """
    function escape(input) {
        // apply strict filter rules of level 0
        // filter ">" and event handlers
        input = input.replace(/>|on.+?=|focus/gi, '_');

        return '<input value="' + input + '" type="text">';
    }
    """
    And this function takes a argument
    Then this argument is verified with a regular expression
    And if the pattern matches, it is replaced by "_"
    Then returns an input tag with the value attribute equal to the argument
    """
    return '<input value="' + input + '" type="text">';
    """

  Scenario: Dynamic detection
    Given the site operates within a sandbox
    And I can see the behavior of the site
    Then I can test payload to determine bugs on the site
    When I enter the following script in it [evidences](dpayload.png)
    """
    <script>alert(1)</script>
    """
    Then I can see that the site can interprets it as Javascript code but
    And this JS code it is inside a "value=" attribute
    And the character ">" is replaced with "_"
    Then I should find a way to get this script out of there
    And bypass the regex filter

  Scenario: Exploitation
    Given the idea is to execute Javascript code
    Then I should find a way to escape of the value attribute
    And this is possible by closing the tags [evidences](closetag.png)
    """
    <input value="" escaped" type="text">
    """
    And once I escaped from the value attribute
    Then I decided to understand how the regular expression works
    And for that there is a great page: "https://regex101.com/"
    """
    >          : matches the character ">" literally (case insensitive)
    |on.+?=|   : "on" matches the characters on literally (case insensitive)
               : ".+?" matches any character (except for line terminators)
               : "=" matches the character = literally (case insensitive)
    focus      : matches the characters "focus" literally (case insensitive)
    """
    Then this regex matches strings as: [evidences](regex.png)
    """
    onfocus=
    >
    focus
    onerror=
    """
    And it has a bug because it allows strings like
    """
    onerror
    onsize
    onload
    onresize
    """
    Then I was curious because it prioritizes the event handler "onfocus="
    And I found that this event handler is used to do XSS in input tags
    """
    https://security.stackexchange.com/a/97555
    """
    Then knowing that I can control the attributes of the input tag
    And onerror event handler is allowed
    Then I take advantage of the multiline functionality of HTML tags
    Then I configure the tag to interpret it as an image
    And thus be able to execute Javascript code through onerror
    Then the complete payload would look like this:
    """
    " type="image" src="1337" onerror
    ="alert(1)
    """ [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - /AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.8/10 (Low) - E:P/RL:U/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      3.2/10 (Low) - IR:L/MAV:A/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-07
