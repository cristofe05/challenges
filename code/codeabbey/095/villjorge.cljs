;; $ clj-kondo --lint villjorge.cljs
;; linting took 25ms, errors: 0, warnings: 0

(ns villjorge-095
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (core/slurp core/*in*))
        timelapse (str/split (data 0) #" ")
        points (subvec data 1 (alength (to-array-2d data)))]
    [timelapse
     points]))

(defn linreg [data]
  (let [rdays (vec (map (fn [[_ valx _]] (core/read-string valx)) data))
        pwine (vec (map (fn [[_ _ valy]] (core/read-string valy)) data))
        sum_rdays (double (reduce + rdays))
        sum_pwine (double (reduce + pwine))
        sum_insrdays (double (reduce + (map * rdays rdays)))
        sum_prod (double (reduce + (map * rdays pwine)))
        numer (- sum_prod (/ (* sum_rdays sum_pwine) (count rdays)))
        denom (- sum_insrdays (/ (* sum_rdays sum_rdays) (count rdays)))
        K (/ numer denom)
        B (- (/ sum_pwine (count pwine)) (* K (/ sum_rdays (count rdays))))]
    (print (str K " " B " "))))

(defn main []
  (let [[_ points] (get-data)]
    (linreg (map #(str/split % #" ") points))
    (println)))

(main)

;; $ cat DATA.lst | clj villjorge.cljs
;; 1.8247561947594788 112.46417621017048
