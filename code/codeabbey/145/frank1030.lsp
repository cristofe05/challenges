#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/145/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/145/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun solution()
  (let ((index 0) (a 0) (b 0) (m 0) (r 0))
    (setq index(get-dat))
    (loop for i from 0 to (1- index)
      do (setq a(get-dat))
      do (setq b(get-dat))
      do (setq m(get-dat))
      do (setq r(mod (expt a b) m))
      do (print r)
    )
  )
)

(solution)

#|
$ cat DATA.lst | clisp frank1030.lsp

10594023 211939547 55545729 2832713
56271383 178407083 58669467 22271111
82003049 47973142 109455420 230060344
163697559 200585365 160550707 293986818
159584647 202895490 215033073 154766909
26071892 135793813 66955167
|#
