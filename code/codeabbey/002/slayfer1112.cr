#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.92 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst").split
  data
end

def sum_in_loop(array)
  sums = 0
  array[1..].each do |x|
    x = x.is_a?(String) ? x.try &.to_i : x
    sums += x
  end
  sums
end

data = data_entry()
total = sum_in_loop(data)
puts total

# $ ./slayfer1112
# 27098
