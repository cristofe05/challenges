## Version 2.0
## language: en

Feature: Owned

  Site:
    https://www.rankk.org/
  User:
    Sierris (wechall)
  Goal:
    Find the username and password to log in

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3     |
    | Google Chrome   | 77.0.3865.90|
  Machine information:
    Given a "Youtube" video

  Scenario: Success:Watch the video information
    Given a "Youtube" video
    When I watch the full video I can see is a network capture
    Then I watch it again
    And wait for him to open the TCP stream
    When I pause the video at second 9
    Then  I can see the full request
    And see the user and password used [evidence](img1.png)
    When I see the password I notice a weird character
    Then I decode the string using URL decoder
    """
    https://www.urldecoder.org/
    """
    And I get the password decoded
    When I use the username and password
    Then I get the flag
