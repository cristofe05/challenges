source "${srcGeneric}"

function lint {
  local solution="${1}"
  local version='0.9.0'
  local HOME='.'

      dub fetch dscanner --version="${version}" \
  &&  dub run dscanner \
        -- \
        --syntaxCheck \
        "${solution}" \
  &&  dub run dscanner \
        -- \
        --styleCheck \
        "${solution}"
}

function compile {
  local solution="${1}"

  dmd \
    -w \
    -c \
    -de \
    -o- \
    -lowmem \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
