#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/071/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/071/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
  (setq dat(read))
  )
)

(defun dfa (n)
  (let ((a 0) (b 0) (sequence-div 0) (c 1))
    (setq a (mod 0 n))
    (setq b (mod 1 n))
    (setq sequence-div (mod (+ a b) n))
    (loop while (/= sequence-div 0)
      do (setq sequence-div (mod (+ (mod a n) (mod b n)) n))
      do (setq a b)
      do (setq b sequence-div)
      do (setq c (+ c 1))
    )
    (return-from dfa c)
  )
)

(defun main()
  (let ((index 0) (n 0) (c 0))
    (setq index(get-dat))
    (loop for i from 1 to index
      do (setq n (get-dat))
      do (setq c (dfa n))
      do (print c)
    )
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp
231228
104740
132282
112410
57057
22704
1400
158244
132804
23807
3480
208212
222060
421540
47592
112470
47608
29400
|#
