{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.pythonPackage = import ../../../builders/python-package pkgs;
  inputs = [
    pkgs.go
    pkgs.golint
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;

            pyPkgLizard = builders.pythonPackage "lizard==1.17.3";
          })
    )
