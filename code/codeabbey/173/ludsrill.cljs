;; clj-kondo --lint ludsrill.cljs
;; linting took 50ms, errors: 0, warnings: 0

(ns ludsrill.173
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn get-mod [data]
  (lazy-seq
    (if (seq data)
      (cons (map (fn [x] (rem (Integer. x) 12)) (first data))
        (get-mod (rest data)))
       nil)))

(defn chords-of-music [octave module item]
  (if (< (count module) 1)
    (print "other ")
    (if (or (= (some #{(+ (first module) 4)} item) (+ (first module) 4))
            (= (some #{(rem (+ (first module) 4) 12)} item)
              (rem (+ (first module) 4) 12)))
      (if (or (= (some #{(+ (first module) 7)} item) (+ (first module) 7))
              (= (some #{(rem (+ (first module) 7) 12)} item)
                (rem (+ (first module) 7) 12)))
        (print (core/format "%s-major " (nth octave (first module))))
        (if (or (= (some #{(+ (first module) 3)} item) (+ (first module) 3))
                (= (some #{(rem (+ (first module) 3) 12)} item)
                  (rem (+ (first module) 3) 12)))
          (if (or (= (some #{(+ (first module) 7)} item) (+ (first module) 7))
                  (= (some #{(rem (+ (first module) 7) 12)} item)
                    (rem (+ (first module) 7) 12)))
            (print (core/format "%s-minor " (nth octave (first module))))
            (chords-of-music octave (rest module) item))
          (chords-of-music octave (rest module) item)))

      (if (or (= (some #{(+ (first module) 3)} item) (+ (first module) 3))
              (= (some #{(rem (+ (first module) 3) 12)} item)
                (rem (+ (first module) 3) 12)))
        (if (or (= (some #{(+ (first module) 7)} item) (+ (first module) 7))
                (= (some #{(rem (+ (first module) 7) 12)} item)
                  (rem (+ (first module) 7) 12)))
          (print (core/format "%s-minor " (nth octave (first module))))
          (chords-of-music octave (rest module) item))
        (chords-of-music octave (rest module) item)))))

(defn -main []
  (let [data (rest (get-data))
        module (get-mod data)
        octave (list 'C 'C# 'D 'D# 'E 'F 'F# 'G 'G# 'A 'A# 'B)]

    (doseq [item module]
      (chords-of-music octave item item))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; F#-major D-minor C-minor D#-minor F-major G-minor G-minor A#-minor C-minor
;; other C-major A#-minor other other E-major G#-minor G#-minor other E-minor
;; G-minor other C-minor other other F#-major G#-major A-minor A-major F-major
;; A-minor other other D#-major A-minor C#-major
