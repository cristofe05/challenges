-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

main = do
  input <- getLine
  nums <- getLine
  let prim = primesToGT 2750131
  let result = search prim (checkString nums) []
  print (unwords (map show (reverse result)))

search :: [Int] -> [Int] -> [Int] -> [Int]
search primes [] result = result
search primes index result =
  search primes (tail index) (primes !! (head index-1):result)

checkString :: String -> [Int]
checkString str = map read $ words str :: [Int]

primesToGT :: Int -> [Int]
primesToGT m = sieve [2..m]
    where
    sieve (p:xs)
        | p*p > m   = p : xs
        | otherwise = p : sieve [x | x <- xs, rem x p > 0]

-- $ ./smendoz3
--   2200867 1739641 2212009 1823051 2020181 1788011 1318259 1342667
--   2544229 2615903
