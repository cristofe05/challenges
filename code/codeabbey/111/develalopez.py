# $ python3 -m mypy --strict develalopez.py
# $ Success: no issues found in 1 source file


def pow_finder(sect: int, sub: int) -> int:
    '''
    Returns a prime number of possible equal subsections of the necklace in
    order to apply Fermat's Little Theorem and return whole numbers.

    If the previous iteration involved a divisor of the beads quantity,
    it will be returned here.
    '''
    if sub == 0:
        return sect
    return pow_finder(sub, sect % sub)


def necklaces(colors: int, beads: int) -> int:
    '''
    Function that gets the number of different necklaces possible for a pair
    of data.
    '''
    ans_num = 0
    ans_den = beads
    for i in range(1, beads + 1):
        ans_num += colors ** (pow_finder(i, beads))
    return int(ans_num / ans_den)


def main() -> None:
    '''
    Program that returns the number of necklaces that can be made of a given
    number of beads of different colors in a number of test cases, applying
    Fermat's Little Theorem.
    '''
    file = open("DATA.lst", "r")
    cases = int(file.readline())
    answers = []
    for _ in range(cases):
        colors, beads = map(int, [int(x) for x in file.readline().split()])
        answers.append(necklaces(colors, beads))
    print(" ".join(str(x) for x in answers))


main()

# $ python3 develalopez.py
# $ 834 956635 8230 45 2195 1119796 11
