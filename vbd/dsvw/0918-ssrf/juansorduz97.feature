## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    Server-Side Request Forgery (SSRF)
  Location:
    http://127.0.0.1:65412/ - path (field)
  CWE:
    CWE-918: Server-Side Request Forgery (SSRF)
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get sensitive data by SSRF
  Recommendation:
    Validate user input before accepting a path

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 72.0.1          |
  TOE information:
    Given I am using the github repository "https://github.com/stamparm/DSVW"
    And I start a server in "localhost:65412"
    When I examine the application technologies
    Then I see that language is "Python 3.6.9"
    And the server is deployed with the Python 3 library "http.server"
    And the database is "SQLite3"

  Scenario: Normal use case
    Given I get a page that shows me a "path" argument
    When I introduce any URL as "https://www.youtube.com/"
    Then the page shows me the main page source code of "youtube.com"

  Scenario: Static detection
    Given I can access to the project source code
    When I review the source code
    Then I find a SSRF vulnerability in the code of "path" parameter:
    """
    Source: ~/DSVW/dsvw.py
    36. elif "path" in params:
    37.   content = (open(os.path.abspath(params["path"]), "rb")
          if not "://" in params["path"]
          else urllib.request.urlopen(params["path"])).read().decode()
    """
    When an attacker sends an URL or file request in "path"
    Then the site open the request without validation

  Scenario: Dynamic detection
  Detecting SSRF
    Given I want to determine if I can access to internal files in the server
    And I have a "dvwa" container running on port 80 for testing
    When I check if port 80 is open using loop back interface:
    """
    | <path>             | <response>              |
    | http://localhost   | [dvwa login page code]  |
    """
    Then I access to the port 80 code
    And I find a SSRF vulnerability

  Scenario: Exploitation
    Given I can read files inside the machine where "dsvw" is running
    When I use "file" URL schema instead of "http" to access internal files:
    """
    | <path>                    | <response>     |
    | file:///etc/debian_version| buster/sid     |
    """
    Then I obtain server sensitive information
    Given It is possible to read information in sensitive paths as "/etc"
    When an attacker tries to access to sensitive files as "/etc/passwd"
    Then an attacker retrieves user information
    And the attacker can use these information to perform other attacks

  Scenario: Remediation
    Given the site is opening an URL sent in "path" parameter
    When an attacker request information through loop back or "file" URL schema
    Then the site reveals internal information
    When the site receives a request through "path" parameter
    Then the site must identify if is a loop back request or "file" URL schema
    When I look the site source code
    Then I modify the code to identify loop back requests or "file" URL schema
    And I neutralizes these inputs
    """
    Source: ~/DSVW/dsvw.py
    37. parampath=params["path"]
    38. if 'localhost' in parampath:
    39.     content = "Requests to loop back is not allowed"
    40. elif '127.0.0.1' in parampath:
    41.     content = "Requests to loop back is not allowed"
    42. elif 'file:/' in parampath:
    43.     content = "Access to internal files is not allowed"
    44. else:
    45.     content = (open(os.path.abspath(parampath), "rb")
            if not "://" in params["path"]
            else urllib.request.urlopen(params["path"])).read().decode()
    """
    When I enter different inputs trying to get sensitive information
    Then the internal files are protected
    And the vulnerability is patched:
    """
    | <path>                  | <response>                              |
    | https://www.youtube.com/| [youtube main page code]                |
    | file:///etc/passwd      | Access to internal files is not allowed |
    | http://localhost:80     | Requests to loop back is not allowed    |
    | http://127.0.0.1:80     | Requests to loop back is not allowed    |
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.7/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2020-01-30
