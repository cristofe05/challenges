## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
    Session Fixation
  Location:
    http://localhost/btslab/login.php session (cookie)
  CWE:
    CWE-384: Session Fixation
  Rule:
    REQ.023 Close inactive users sessions
    REQ.030 Avoid object reutilization
  Goal:
    Inject cookie
  Recommendation:
    Use regenerate cookie session

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromer        | 76.0.3809.132   |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost/btslab/
    And the server is running MariaDB version 10.1.36
    And PHP version 5.6.38
    And Apache version 2.4.34
    And is running on Windows 10 with XAMPP 5.6

  Scenario: Normal use case
    When I am have access to http://localhost/btslab/
    Then I get a new session cookie

  Scenario: Static detection
    Given ToE source code
    When I inspect 'login.php'
    Then I see the vulnerability is being caused by lines 13 to 17
    """
    13    session_start();
    14    $_SESSION['isLoggedIn']=1;
    15    $_SESSION['userid']=$data["ID"];
    16    $_SESSION['username']=$data["username"];
    17    $_SESSION['avatar']=$data['avatar'];
    """
    And I notice that web server use the same session cookie for everything

  Scenario: Dynamic detection
    Given I am have access to http://localhost/btslab/
    And I notice that I get a new cookie
    When I am have access to login page and I login
    Then notice that mi cookie don't change

  Scenario: Exploitation
    Given I can put a costum session cookie in the browser
    """
    document.cookie="PHPSESSID=customCookie"
    """
    Then I login in the web page
    When I use the same cookie in another browser
    Then I notice that I get the same session data
    Then I conclude that the site is vulnerability to session fixation

  Scenario: Remediation
    Given I have patched the session fixation vulnerability using php function
    """
    session_regenerate_id(true)
    """
    Then I add 14 line as seen:
    """
    13    session_start();
    14    session_regenerate_id(true)
    15    $_SESSION['isLoggedIn']=1;
    16    $_SESSION['userid']=$data["ID"];
    17    $_SESSION['username']=$data["username"];
    18    $_SESSION['avatar']=$data['avatar'];
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.8/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.8/10 (Low) - CR:L/IR:L/AR:M/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:N/

  Scenario: Correlations
    btslab/0080-xss-store-fake-login
    Given this site contains XSS vulnerability
    When used in conjunction with this vulnerability
    Then I can use it to get an set cookies whenever I want
