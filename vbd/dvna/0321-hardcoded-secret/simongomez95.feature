## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Hardcoded Sensitive Data
  Location:
    / - Code
  CWE:
    CWE-0321: Use of Hard-coded Cryptographic Key -base-
      https://cwe.mitre.org/data/definitions/321.html
    CWE-0922: Insecure Storage of Sensitive Information -class-
      https://cwe.mitre.org/data/definitions/922.html
    CWE-1013: Encrypt Data -category-
      https://cwe.mitre.org/data/definitions/1013.html
  CAPEC:
    CAPEC-037: Retrieve Embedded Sensitive Data -detailed-
      http://capec.mitre.org/data/definitions/37.html
    CAPEC-167: White Box Reverse Engineering -standard-
      http://capec.mitre.org/data/definitions/167.html
    CAPEC-188: Reverse Engineering -meta-
      http://capec.mitre.org/data/definitions/188.html
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/156/
  Goal:
    Get secret from source code
  Recommendation:
    Store secrets safely

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "/dvna/server.js"
    """
    23  app.use(session({
    24    secret: 'keyboard cat',
    25    resave: true,
    26    saveUninitialized: true,
    27    cookie: { secure: false }
    28  }))
    """
    Then I see it has the session-signing secret hardcoded

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  Using secrets found in code
    Given I have obtained a copy of the project code
    Then I find the secret burned in the code
    Then I can use this to sign my own session ids
    And get access I shouldn't have

  Scenario: Remediation
  Fixing secrets
    Given I remove the secrets from the code
    And change them
    And safely store them
    Then they aren't in the codebase anymore
    And can't be found by an evil actor

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.0/10 (High) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.4/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.4/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-29
