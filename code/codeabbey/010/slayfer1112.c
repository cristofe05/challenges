/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <stdio.h>
#define MAX_SIZE 1024

/**/
typedef struct Point {
  int x;
  int y;
} Point;

typedef struct Line {
  Point p1;
  Point p2;
  int a;
  int b;
} Line;

int main(void) {
  char temp[MAX_SIZE];
  int cases, i = 0, z = 0;
  Line line;
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  cases = atoi(temp);
  for (i = 0; i < cases; i++) {
    if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
    z = sscanf(temp, "%d %d %d %d", &line.p1.x, &line.p1.y,
      &line.p2.x, &line.p2.y);
    line.a = (line.p2.y - line.p1.y) / (line.p2.x - line.p1.x);
    line.b = ((line.p2.x * line.p1.y) - (line.p1.x * line.p2.y)) /
      (line.p2.x - line.p1.x);
    printf("(%d %d) ", line.a, line.b);
  }
  printf("\n");
  return z;
}

/*
$ cat DATA.lst | ./slayfer1112
10.58701590 6.40312424 5.83095189 4.71495167 4.43760157 11.66190379 0.89442719
6.32455532 1 14.83762691 4.12310563 10.64689794 7.69468660 0.97014250
3.36242210 8.77895573 4.47213595 12.55143265 9.83991671 5.83095189
*/
