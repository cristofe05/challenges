## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Injection Flaws
  Location:
    /#/track-result - orderId (Parameter)
  CWE:
    CWE-754: Improper Check for Unusual or Exceptional Conditions
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute arbitrary JS in a victim's browser
  Recommendation:
    Sanitize user inputs and escape outputs

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No data validation
    Given I see the code at "feedback.service.ts"
    """
    ...
    22  save (params) {
    23    return this.http.post(this.host + '/', params).pipe(map((response: any
    ) => response.data), catchError((err) => { throw err }))
    24  }
    ...
    """
    Then I see it saves the request values exactly as passed

  Scenario: Dynamic detection
  Modifying request values
    Given I intercept a feedback POST request with Burp
    """
    POST /api/Feedbacks/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzd
    WNjZXNzIiwiZGF0YSI6eyJpZCI6MTQsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwic
    GFzc3dvcmQiOiIzNzQwY2YzYjU0NWVmZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhb
    HNlLCJsYXN0TG9naW5JcCI6IjAuMC4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsI
    mNyZWF0ZWRBdCI6IjIwMTktMDEtMzAgMTI6NTg6NTguODMyICswMDowMCIsInVwZGF0ZWRBdCI6I
    jIwMTktMDEtMzAgMTI6NTg6NTguODMyICswMDowMCJ9LCJpYXQiOjE1NDg4NTMxNDYsImV4cCI6M
    TU0ODg3MTE0Nn0.bbKEkklqNjL-dH-RlqKEarDMNdZY4r9Hig18ZAJomwK1RRA5V7kNAnieIUBYL
    Q3lRUX3K2kx27Sa0c5FOzowPTTU1UxF3NAS0-jGlFy_ay5Vh4z7nVeXWYQfkbAyud5qUg6aB57ui
    dgQfNYG7_xENSuAh6Cxy26gnh0vHSaozxo
    Content-Type: application/json
    Content-Length: 70
    Connection: close
    Cookie: cookieconsent_status=dismiss; io=65qNxGheHNM5rmZBAAAf; token=eyJhbGc
    iOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MT
    QsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwicGFzc3dvcmQiOiIzNzQwY2YzYjU0NW
    VmZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhbHNlLCJsYXN0TG9naW5JcCI6IjAuMC
    4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsImNyZWF0ZWRBdCI6IjIwMTktMDEtMz
    AgMTI6NTg6NTguODMyICswMDowMCIsInVwZGF0ZWRBdCI6IjIwMTktMDEtMzAgMTI6NTg6NTguOD
    MyICswMDowMCJ9LCJpYXQiOjE1NDg4NTMxNDYsImV4cCI6MTU0ODg3MTE0Nn0.bbKEkklqNjL-dH
    -RlqKEarDMNdZY4r9Hig18ZAJomwK1RRA5V7kNAnieIUBYLQ3lRUX3K2kx27Sa0c5FOzowPTTU1U
    xF3NAS0-jGlFy_ay5Vh4z7nVeXWYQfkbAyud5qUg6aB57uidgQfNYG7_xENSuAh6Cxy26gnh0vHS
    aozxo; continueCode=ZYrPKlWVv4xmnygo3MBpQ0NVHehqiauMcx5TMk02b7qXRN95zL18aDk6
    wEJj

    {"UserId":14,"captchaId":6,"captcha":"14","comment":"pppp","rating":1}
    """
    Then I change the rating value to 0
    """
    POST /api/Feedbacks/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    ...
    {"UserId":14,"captchaId":6,"captcha":"14","comment":"pppp","rating":0}
    """
    Then I get a success response
    """
    Thank you for your feedback.
    """

  Scenario: Exploitation
  Tanking the store score
    Given I know I can send 0-star reviews
    Then I can send several of them
    And drop the store average score to really low lows

  Scenario: Remediation
  Access control
    Given I enable angular sanitizing
    """
    ...
    22  save (params) {
    23    if(params.rating > 0) {
    24      return this.http.post(this.host + '/', params).pipe(map((response: a
    ny) => response.data), catchError((err) => { throw err }))
    25    }
    26  }
    ...
    """
    Then the feedback is only saved if rating>0

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
