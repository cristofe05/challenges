;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn get-vals [line]
  (let [values (str/split line #" ")
        val1 (read-string (values 0))
        val2 (read-string (values 1))
        val3 (read-string (values 2))]
    [val1 val2 val3]))

(defn prints-per-seg [printer? seg]
  (if (= 0 (mod seg printer?))
    1
    0))

(defn printers [X Y N]
  (let [a (* (Math/ceil (* (/ X (+ X Y)) N)) Y)
        b (* (Math/ceil (* (/ Y (+ X Y)) N)) X)]
    (if (< a b)
      a
      b)))

(defn solution [_ body]
  (doseq [x body]
    (let [[X Y N] (get-vals x)
          time-to-print (int (printers X Y N))]
      (print (str time-to-print " ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 263758430 237548096 379877502 197944 211802745 193536936 349935036
;; 252645018 290500263 39628224 355061139 275607571 356903610 25905942
;; 248863520 173586819 140773700 335585812 236054268 377929433
