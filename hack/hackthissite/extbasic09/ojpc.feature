# language: en

Feature: Solve the challenge 9
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a script in Pearl with an error in record logs 
    And I have to detect where is the error and fix it

  Scenario: Successful Solution
    When I analyze the structure of the script
    Then I find the script have a correct logic sequence
    Then I discover that error must be a wrong use or syntaxis in  operators
    Then I search information in internet about Pearl's operators
    Then I find that for append a line in a text file, I have to use ">>"
    Then I look for this operator and just find the single ">" version
    Then I declare this must be the error and change it for ">>"
    And I solve the challenge

