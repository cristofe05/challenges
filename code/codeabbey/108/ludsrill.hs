-- hlint ludsrill.hs
-- No hints

main = do
  n <- readLn :: IO Int
  starMedals n

starMedals :: Int -> IO ()
starMedals 0 = return ()
starMedals n = do
  array <- getLine
  let xs = map read $ words array :: [Int]
  let a = head xs
  let b = xs !! 1
  putStr $ solution a b
  putStr " "
  starMedals (n-1)

solution :: Int -> Int -> String
solution a b = c
  where
    c = show ((b - 1) * a)

-- cat DATA.lst | runhaskell ludsrill.hs
-- 27 68 44 85 28 14 57
