#!/usr/bin/env python
'''
$ pylint bridamo98.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python bridamo98.py #compilation
'''

import socket
import time
import math

_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server = "irc.root-me.org"
channel = "#root-me_challenge"
nick = "bridamo98"
bot = "candy"

_socket.connect((server, 6667))
aux = "NICK " + nick + "\r\n"
_socket.send(bytes(aux,'utf-8'))
aux = "USER " + nick + " " + nick + " " + nick + " :Hello!\n"
_socket.send(bytes(aux,'utf-8'))
aux = "JOIN " + channel + "\n"
_socket.send(bytes(aux,'utf-8'))

while True:
    txt = ""
    time.sleep(0.2)
    try:
        txt = _socket.recv(7000).decode("UTF-8")
    except Exception:
        print(Exception)
    if txt.find("PING") != -1:
        aux = 'PONG ' + txt.split()[1] + '\r\n'
        _socket.send(bytes(aux,'utf-8'))
        break

aux = "PRIVMSG " + "candy" + " " + '!ep1' + "\n"
_socket.send(bytes(aux,'utf-8'))

while True:
    txt = ""
    time.sleep(0.2)
    try:
        txt = _socket.recv(7000).decode("UTF-8")
    except Exception:
        print(Exception)
    if txt.find("/") != -1:
        first_number = float((txt.split()[3])[1:])
        second_number = float(txt.split()[5])
        result = math.sqrt(first_number)*second_number
        rounded_result = round(result, 2)
        aux = "PRIVMSG "+"candy"+" "+"!ep1 -rep "+str(rounded_result) + "\n"
        _socket.send(bytes(aux,'utf-8'))
    if txt.find("password") != -1:
        print(txt)
        break
'''
$ python bridamo98.py
:Candy!Candy@root-me.org PRIVMSG bridamo98 :You dit it!
You can validate the challenge with the password jaimlefr0m4g
'''
