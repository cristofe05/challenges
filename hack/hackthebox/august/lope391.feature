## Version 2.0
## language: en

Feature: august-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Crypto
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given A text file with this ciphered text:
    """
    KDYGb3Y]IHO3-[jxbz4NbS:qIBY4
    """
    And A flag format of
    """
    HTB{$FLAG}
    """
    And This official clue:
    """
    August left Chris in America. Can you get the flag and reunite them?
    """
    And An extra clue from the forums:
    """
    August and Chris both worked in the same field
    """

  Scenario: Fail: Did not find the flag
    Given The two clues and the ciphered text
    When I search for August in the context of technology
    Then I find a possible subject of interest in August Dvorak
    And He created the Dvorak keyboard layout
    When I search for Chris in the context of keyboards
    Then I find another subject of interest in Christopher Latham
    And He created the QWERTY Keyboard layout
    When I read the official clue again "August left Chris in America"
    Then I conclude the cipher must be moving one key to the left
    And It goes from the Dvorak US layout to the QWERTY US layout
    When I apply my possible cipher solution
    Then I am left with a string of incoherent characters
    When I try to decode the new string using base64 and URL decode
    Then I get invalid characters in the result
    And I conclude I did not solve the challenge

  Scenario: Success: Found the flag
    Given The ciphered text
    And The knowledge gained on the subjects from the Fail Scenario
    When I keep reading about the Dvorak layout
    Then I find there is more than one Dvorak layout
    And One of the Dvorak layouts is specifically for left hand typing
    When I read the official clue again "August left Chris in America"
    Then I conclude the cipher must be a direct substitution
    And It goes from US Dvorak left hand layout to the US QWERTY layout
    When I apply my possible cipher solution
    Then I am left with a string of incoherent characters
    When I decode the new string using base64
    Then I find what seems to be the flag
    When I submit the found as the answer
    Then I solve the challenge
