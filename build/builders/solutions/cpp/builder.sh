source "${srcGeneric}"

function lint {
  local solution="${1}"
  local lizard_max_warns='0'
  local lizard_max_func_length='30'
  local lizard_max_ccn='10'

      cppcheck \
        --error-exitcode=1 \
        "${solution}" \
  &&  lizard \
        --ignore_warnings "${lizard_max_warns}" \
        --length "${lizard_max_func_length}" \
        --CCN "${lizard_max_ccn}" \
        "${solution}"
}

function compile {
  local solution="${1}"

  g++ \
    -Wall \
    -Wextra \
    -Winit-self \
    -Wold-style-cast \
    -Woverloaded-virtual \
    -Wuninitialized \
    -Wmissing-declarations \
    -Winit-self \
    -ansi \
    -pedantic \
    -Werror \
    "${solution}" \
    -o "${solution%.*}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_python_packages \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
