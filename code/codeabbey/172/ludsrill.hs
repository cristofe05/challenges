-- hlint ludsrill.hs
-- No hints

main = do
  n <- readLn :: IO Int
  cloudAltitudeMeasurement n

cloudAltitudeMeasurement :: Int -> IO ()
cloudAltitudeMeasurement 0 = return ()
cloudAltitudeMeasurement n = do
  array <- getLine
  let xs = map read $ words array :: [Float]
  let d = head xs
  let a = (xs !! 1) * pi / 180
  let b = (xs !! 2) * pi / 180
  putStr $ solution d a b
  putStr " "
  cloudAltitudeMeasurement (n-1)

solution :: Float -> Float -> Float -> String
solution d a b = h
  where
    h = show (round (tan a * d / (1 - (tan a / tan b))))

-- cat DATA.lst | runhaskell ludsrill.hs
-- 1544 1515 1766 1981 1370 527 1995 1915 610 1835 1499 1628 592 1445 1523 838
-- 1804 603
