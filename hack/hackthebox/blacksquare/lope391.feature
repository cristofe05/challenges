## Version 2.0
## language: en

Feature: blackSquare-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Stego
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
    | ImageMagick     | 6.9.7-4     |
    | Python          | 3.6.9       |
    | pip             | 9.0.1       |
    | Pillow          | 7.0.0       |
  Machine information:
    Given An image file of Malevich's Black Square
    And A flag format of
    """
    HTB{$FLAG}
    """
    And An image analysis software in ImageMagick
    And An image transformation library in Pillow for python3
    And Its running on Mint 19.3 with  kernel 18.04.3
    And pillow is installed in the local python enviroment
    """
    pip install pillow
    """
    And The histogram of colors present in the image generated with ImageMagick
    """
    identify -verbose ~/Documents/tmp/blackSquare.png
    """

  Scenario: Fail: Did not find the flag
    Given The color histogram of the image
    When I read through the color frequencies
    Then I see the colors in the image are shades of black
    And The shades of black vary from (0,0,0) to (5,5,5) in RGB notation
    When I analyze the histogram further
    Then I realize most of the pixels in the image have a blue component of 0
    And I think the flag is in the pixels with a blue component different to 0
    When I use the python script "lope391.py"
    And Modify the original image using pillow
    Then I replace all the pixels with a blue component of 0 to white
    And I am left with an image that resembles Morse code [evidence](wrong.png)
    When I try to translate the Morse code in the image
    Then I find it is not a valid Morse code
    And There is no message or flag in it
    And I have not solved the challenge

  Scenario: Success: Found the flag
    Given The color histogram of the image
    And The previous analysis of frequencies from the Fail Scenario
    When I look through the frequencies again
    Then I realize there are only a small number of all black pixels (0,0,0)RGB
    When I use the python script "lope391.py" using pillow
    Then I replace all the true black pixels in the image to white
    When I look at the resulting image
    Then I find it also resembles Morse code [evidence](right.png)
    And It is a valid Morse code
    When I translate this Morse code
    Then I get a long paragraph talking about Morse code
    And The flag is written in the paragraph
    When I format the answer using the flag format
    Then I enter the flag in the website
    And I solve the challenge
