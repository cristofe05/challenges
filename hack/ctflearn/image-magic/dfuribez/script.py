from PIL import Image
import numpy as nd
from matplotlib import pyplot as plt

im = Image.open("img.jpg")
im = nd.asarray(im)

print(im.shape)
im = im.reshape(304, 92, 3)
plt.imsave("flag.png", im)
