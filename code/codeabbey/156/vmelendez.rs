/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/
use std::io::BufRead;

fn swap(card: &str) -> String {
  for i in 0..card.len() - 1 {
    let mut rcard = card.chars().collect::<Vec<_>>();
    rcard.swap(i, i + 1);
    let test_card = rcard.iter().collect::<String>();
    if check(&test_card) {
      return test_card;
    }
  }
  return card.to_string();
}

fn check(card: &str) -> bool {
  let mut sum = 0;
  for (i, c) in card.chars().rev().enumerate() {
    if i % 2 != 0 {
      let mut s = &c.to_string().parse::<i32>().unwrap() * 2;
      if s >= 10 {
        s -= 9;
      }
      sum += s;
    } else {
      sum += &c.to_string().parse::<i32>().unwrap();
    }
  }

  if sum % 10 == 0 {
    return true;
  } else {
    return false;
  }
}

fn luhn_algorithm(card: &str, res: &mut Vec<String>) {
  if card.contains("?") {
    for i in 0..10 {
      if check(&card.replace("?", &i.to_string())) {
        res.push(card.replace("?", &i.to_string()));
      }
    }
  } else {
    res.push(swap(card));
  }
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  let stdin = std::io::stdin();
  stdin.read_line(&mut n).unwrap();

  let int_n = n.trim().parse().expect("[-] a num!");
  let mut res = Vec::new();
  for line in stdin.lock().lines().take(int_n) {
    let card = line.unwrap();
    let card = card.trim();
    luhn_algorithm(card, &mut res);
  }
  let mut out_string = String::new();
  let vec_iter = res.iter();

  for i in vec_iter {
    out_string += &i.to_string();
    out_string += &" ".to_owned();
  }

  println!("{}", out_string);
  Ok(())
}

/*
  $ cat .\DATA.lst | .\vmelendez.exe
  8969144023813056 6772582689506541 7309966460741023 9229374427085795
  5862253652508145 6586746067527942 1389132636151086 2191241905189242
  1289408938530560 2475140437917301 8892104896844432 9128524409398929
  7187850672618434 1426534455193807 9624027214738056 3880033780673125
  6700641624739729 7389167579118475 7672346679297464 8066614071400172
  6293016468378673 1743029704118800 1577980079745281 2514664152995344
  4144857862362177 4652586940711774 2631089891485589 8667657206620332
  7728334990533522 7869415011979783 6757997146628465 4688100251546262
  2561453738371184 9664895346103501 6373502359946721 6296569219278276
  7675149968273973 6678613087169148 5193376452276017 5233442682055050
  1214581809153041 9871360278267344
*/
