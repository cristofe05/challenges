## Version 1.4.1
## language: en

Feature:
  TOE:
    Bodgeit
  Category:
    SecureFlag
  Location:
    bodgeit/
  CWE:
    CWE-1004: Sensitive Cookie Without 'HttpOnly' Flag -variant-
      https://cwe.mitre.org/data/definitions/1004.html
    CWE-0732: Incorrect Permission Assignment for Critical Resource -class-
      https://cwe.mitre.org/data/definitions/732.html
    CWE-1011: Authorize Actors -category-
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-031: Accessing/Intercepting/Modifying HTTP Cookies -detailed-
      https://capec.mitre.org/data/definitions/31.html
    CAPEC-157: Sniffing Attacks -standard-
      https://capec.mitre.org/data/definitions/157.html
    CAPEC-117: Interception -meta-
      https://capec.mitre.org/data/definitions/117.html
  Rule:
    REQ.029: https://fluidattacks.com/web/es/rules/029/
  Goal:
    Listening Cookie whiout Secure set
  Recommendation:
    Add lines of code to the web.xml file in dir of your tomcat conf

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Parrot Security OS  |  4.2.2x64   |
    | Firefox             |   52.9.0    |
    | Wireshark           |    2.4.2    |

  TOE information:
    Given I enter the site bodgeit/
    And I can see items for the name, type and price
    And the server is runing HSQLDB
    And tomcat version 8.5.37
    And is running on Parrot OS 4.2.2x64

  Scenario: Normal use case
  See items in the menu of bodgeit
    Given I access the site bodgeit/
    And I see the products of your type and price
    Then I can click on a product to add it to my basket
    And I see the list of the items related to my search

  Scenario: Static detection
  The code web.xml in the conf folder of the tomcat no is set
    When I open the file web.xml
    Then I see the resource content the error in the lines 596-597.
    """
    595 <session-config>
    596     <session-timeout>30</session-timeout>
    597 </session-config>
    """

  Scenario: Dynamic detection
  Seeing if the site has the vulnerability Cookie whiout Secure set
    Given I access the site bodgeit/
    Then I open the vega subgraph
    And vega show that the server have vulnerability
    #Session Cookie Without Secure Flag (view imagen.png)
    Then the vulnerability Session Cookie Without Secure Flag exist in the
    #server
    And I conclude that the server is vulnerable why the secure flag does
    #not set, but this vulnerability exist on the server, no in the bodgeit page

  Scenario: Exploitation
  gettin cookie of user in http://localhost:8080/bodgeit/
    Given I need listen the local site of bodgeit
    Then I open the wireshark
    And I slect my local listening card
    Then I update the page bodgeit
    And I see on the wireshark the dir bodgeit/. filtering (http)
    Then I see the coockie.

  Scenario: Remediation
  The server bodgeit has a vulnerability in the direction
    #http://localhost:8080/bodgeit/
    Given this I advise to repair the vulnerability adding the lines between
    #lines 596 and 597:
    """
    595 <session-config>
    596     <session-timeout>30</session-timeout>
    597          <cookie-config>
    598          <http-only>true</http-only>
    599          <secure>true</secure>
    600          </cookie-config>
    601 </session-config>
    """
    Then I can confirm that the vulnerability can be successfully patched.
    #The complety method for verify that this vulneabilty is patched is through
    #Of the HTTPS chanels or insecure chanels.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.3/10 (Low) - AV:L/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.1/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    2.4/10 (Low) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-23
