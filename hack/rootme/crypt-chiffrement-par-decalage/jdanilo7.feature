## Version 2.0
## language: en

Feature:  Web Client - Root Me
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    jdanilo7
  Goal:
    Decrypt a shift cipher encrypted message

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |    60.2.0esr    |
  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/fr/Challenges/Cryptanalyse/Chiffrement-par-decalage
    """
    And The challenge statement
    """
    Retrouvez le mot de passe / Recover the password
    """
    And the challenge ch7.bin file
    Then I am asked to decrypt the message.

  Scenario: Success: Shift using all ASCII characters
    Given I open the ch7.bin file using the atom editor
    """
    L|k�y+*^*zo�*�kvsno|*k�om*vo*zk}}*cyvksr
    """
    And this ASCII Caesar cipher
    """
    https://sltls.org/shift
    """
    Then I left-shift the message ten times and I get this string
    """
    Bra�o! T pe� �alider a�ec le pass Yolaih
    """
    And I conclude the message is
    """
    Bravo! Tu peut valider avec le pass Yolaihu
    """
    And I validate the pass "Yolaihu" on the challenge website
    And it is accepted
