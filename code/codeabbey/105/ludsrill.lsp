#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun euclidian-distance (item)
  (let ((x1 (first item))
        (y1 (second item))
        (x2 (third item))
        (y2 (fourth item))
        (distance))
    (setq distance (sqrt (coerce (+ (expt (- x2 x1) 2)
      (expt (- y2 y1) 2)) 'double-float)))))

(defun calculate-triangle-area (item)
  (let ((first-side)
        (second-side)
        (third-side)
        (s) (area))

    (setq first-side (euclidian-distance (subseq item 0 4)))
    (setq second-side (euclidian-distance (subseq item 2)))
    (setq third-side (euclidian-distance (append (subseq item 4)
                      (subseq item 0 2))))
    (setq s (/ (+ first-side second-side third-side) 2))
    (setq area (sqrt (coerce (* s (- s first-side) (- s second-side)
                (- s third-side)) 'double-float)))
    (return-from calculate-triangle-area area)))

(defun convex-polygon-area ()
  (let ((data)
        (total-area)
        (main-vertex)
        (area-one-triangle)
        (cluster-points))
    (setq data (cdr (read-data)))
    (setq total-area 0)
    (loop for i from 1 to (- (length data) 2)
      do(setq main-vertex (first data))
      (setq cluster-points (append main-vertex (elt data i)
                            (elt data (+ i 1))))
      (setq area-one-triangle (calculate-triangle-area cluster-points))
      (setq total-area (+ total-area area-one-triangle)))
    (format t "~,2f " total-area)))

(convex-polygon-area)

#|
cat DATA.lst | clisp ludsrill.lsp
64513293.00
|#
