;; $ clj-kondo --lint nickkar.cljs
;; linting took 68ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as string]))

(defn reverse-string [s]
  (string/reverse s))

(defn main []
  (let [word (core/read-line)]
    (println (reverse-string word))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; nrut evitagorretni tes boj erehw elpmis llit dne
