/*
$ cargo clippy
Checking code_101 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.27s
$ cargo build
Compiling code_101 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.25s
$ rustc sullenestd1.rs
*/

use std::io::{self, BufRead};

fn function(x0: f64, y0: f64, a: f64, b: f64, c: f64) -> f64 {
  let aux_e = -(x0 + a).powf(2.0) - (y0 + b).powf(2.0);
  let e = aux_e.exp();
  (x0 - a).powf(2.0) + (y0 - b).powf(2.0) + c * e
}

fn gradient(x0: f64, y0: f64, a: f64, b: f64, c: f64) -> Vec<f64> {
  let dt = 0.000000001;
  let gx = (function(x0 + dt, y0, a, b, c) - function(x0, y0, a, b, c)) / dt;
  let gy = (function(x0, y0 + dt, a, b, c) - function(x0, y0, a, b, c)) / dt;
  let vec: Vec<f64> = [gx, gy].to_vec();
  vec
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 1;
  let mut a: f64 = 0.0;
  let mut b: f64 = 0.0;
  let mut c: f64 = 0.0;
  for w in lines {
    let line = w.unwrap();
    let num: Vec<f64> = line
      .trim()
      .split(' ')
      .map(|w| w.parse().expect("Not a float!"))
      .collect();
    if n == 1 {
      a = num[1];
      b = num[2];
      c = num[3];
      n = 2;
    } else {
      let x0 = num[0];
      let y0 = num[1];
      let vector = gradient(x0, y0, a, b, c);
      let aux_degrees: f64 = vector[1];
      let degrees = aux_degrees.atan2(vector[0]).to_degrees().round() as i64;
      println!("{}", degrees + 180);
    }
  }
}

/*
$ cat DATA.lst | ./sullenestd1
222
246
211
212
234
254
19
214
172
234
244
254
175
213
*/
