## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Insufficient Logging & Monitoring
  Location:
    / - Insecure Functions
  CWE:
    CWE-0778: Insufficient Logging -base-
      https://cwe.mitre.org/data/definitions/778.html
    CWE-0693: Protection Mechanism Failure -class-
      https://cwe.mitre.org/data/definitions/693.html
    CWE-0254: 7PK - Security Features
      https://cwe.mitre.org/data/definitions/254.html
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.075: https://fluidattacks.com/web/es/rules/075/
  Goal:
    Execute malicious actions undetected
  Recommendation:
    Implement proper logging

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/core/appHandler.js"
    """
    09  module.exports.userSearch = function (req, res) {
    10    var query = "SELECT name,id FROM Users WHERE login='" + req.body.login
     + "'";
    11    db.sequelize.query(query, {
    12      model: db.User
    13    }).then(user => {
    14      if (user.length) {
    15        var output = {
    16          user: {
    17            name: user[0].name,
    18            id: user[0].id
    19          }
    20        }
    21        res.render('app/usersearch', {
    22          output: output
    23        })
    24      } else {
    25        req.flash('warning', 'User not found')
    26        res.render('app/usersearch', {
    27          output: null
    28        })
    29      }
    30    }).catch(err => {
    31      req.flash('danger', 'Internal Error')
    32      res.render('app/usersearch', {
    33        output: null
    34      })
    35    })
    36  }
    """
    Then I see it doesn't log errors

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  Sneaky SQLi
    Given there is no logging on SQL errors
    Then I can continually test SQLi attacks
    And not raise any suspicions that could make an admin block my attempts

  Scenario: Remediation
  Proper logging
    Given I install a logging library
    And implement logging in all potentially insecure functions
    Then I can monitor for suspicious usage and prevent/block attacks

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
