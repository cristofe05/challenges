{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  msg <- getLine
  let divMsg = words msg
  let convDivMsg = map read divMsg :: [Int]
  let binValues = map dec2bin convDivMsg
  let fillBytes = map fillAndReverse binValues
  let filteredMsg = filter (\n -> sum n `mod` 2 == 0) fillBytes
  let convertedMsg = map convertMsg filteredMsg
  print convertedMsg

convertMsg :: [Int] -> Char
convertMsg dat = res
  where
    decDat = bin2dec 0 (reverse dat)
    res = toEnum decDat

bin2dec :: Int -> [Int] -> Int
bin2dec 7 dat = 0
bin2dec n dat = (dat !! n) * (2 ^ n) + bin2dec (n + 1) dat

dec2bin :: Int -> [Int]
dec2bin 0 = [0]
dec2bin n = if mod n 2 == 0
              then 0:dec2bin (div n 2)
              else 1:dec2bin (div n 2)

fillAndReverse :: [Int] -> [Int]
fillAndReverse dat = res
  where
    filled = dat ++ replicate (8 - length dat) 0
    res =  reverse filled

{-
$ cat DATA.lst | ./bridamo98
  QBt3u W5qa7I YPpKigaj Y Gr YmQ TmKRNjQSA8  PO sWkcI CXId G Yg3Ppn .
-}
