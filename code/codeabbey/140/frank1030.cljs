;$ clj-kondo --lint frank1030.cljs
;linting took 750ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn factorial [n]
  (reduce core/*' (range 1 (inc n))))

(defn bracket-sequences [n]
  (let [num (/ (factorial (* 2 n)) (* (factorial (+ n 1)) (factorial n)))
    res (str/split (str num) #"N")]
    (print (nth res 0))))

(defn main []
  (let [n (core/read)]
    (bracket-sequences n)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;1788944184090754677284847690823682198584587592261422137764435440543925438
;8305652765368694579438993856237800706568207512361086308799009494135074153
;61722800967825848288856452920
