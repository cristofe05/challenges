"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting

"""
from __future__ import print_function


def decrypt_middle_9_challenge(data):
    """ Function to decrypt """

    size = int(data())
    for _ in range(size):

        encrypted_string = input().split()
        dictonary = {"a": "483", "b": "6B3", "c": "8E3", "d": "A14",
                     "e": "C44", "f": "E74", "g": "0B4", "h": "2E4",
                     "i": "415", "j": "645", "k": "875", "l": "AA5",
                     "m": "CD5", "n": "E06", "o": "046", "p": "276",
                     "q": "4A6", "r": "6D6", "s": "807", "t": "A37",
                     "u": "C67", "v": "E97", "w": "0D7", "x": "208",
                     "y": "438", "z": "668", "A": "898", "B": "AC8",
                     "C": "CF8", "D": "E29", "E": "069", "F": "299",
                     "G": "4C9", "H": "6F9", "I": "82A", "J": "A5A",
                     "K": "C8A", "L": "EBA", "M": "0FA", "N": "22B",
                     "O": "45B", "P": "68B", "Q": "8BB", "R": "AEB",
                     "S": "C1C", "T": "E4C", "U": "08C", "V": "2BC",
                     "W": "4EC", "X": "61D", "Y": "84D", "Z": "A7D",
                     "_": "EDD", ".": "01E", ",": "24E", ";": "47E",
                     ":": "6AE", "?": "8DE", "!": "A0F", " ": "C3F",
                     "0": "091", "1": "2C1", "2": "4F1", "3": "622",
                     "4": "852", "5": "A82", "6": "CB2", "7": "EE2",
                     "8": "023", "9": "253"}

        inv_map = {v: k for k, v in dictonary.items()}
        result = []
        iterations = len(encrypted_string[0])
        answer = []
        for pairs_str in range(iterations):
            result.append(encrypted_string[0][pairs_str:pairs_str+3])

        for str_to_decrypt in result[0::3]:
            answer.append(inv_map[str_to_decrypt])

        print("".join(answer[::-1]))


decrypt_middle_9_challenge(input)

# $python dferrans.py < DATA.lst
# fireball 123
