# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use List::Util qw( min max reduce );
use List::MoreUtils qw(first_index);
use Readonly;
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data[ 0 .. $#data ];
}

sub solution {
  my @data = @_;
  my $vals = $data[0];
  Readonly my @dices => ( 1.5, 2.5, 3.5, 4.5, 5.5, 6.5 );
  Readonly my $limit => 99;
  my @dat     = ( split $SPACE, $vals )[ 0 .. $limit ];
  my $minimum = min @dat;
  my $maximum = max @dat;
  my $pro     = ( reduce { $a + $b } @dat ) / ( $limit + 1 );
  my @prob    = map { sprintf '%.0f', ( abs $_ * $minimum - $pro ) } @dices;
  my $faces   = ( first_index { $_ eq ( min @prob ) } @prob ) * 2 + 2;

  if ( $minimum * $faces <= $maximum ) {
    exit 1 if !print "$minimum" . 'd' . "$faces ";
  }
  else {
    $faces = $faces - 2;
    exit 1 if !print "$minimum" . 'd' . "$faces ";
  }
  return 'This is time of probability!';
}

sub main {
  my @vals = data_in();
  for my $test (@vals) {
    solution($test);
  }
  exit 1 if !print "\n";
  return 'You make it in the first attemp?';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 6d2 4d2 2d10
