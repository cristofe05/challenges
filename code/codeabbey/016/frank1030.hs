{-
$ ghc -o  frank1030 frank1030.hs
[1 of 1] Compiling Main             ( frank1030.hs, frank1030.o )
Linking frank1030 ...

$ hlint frank1030.hs
No hints
-}

wordsWhen     :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
  "" -> []
  s' -> w : wordsWhen p s''
    where (w, s'') = break p s'

toint :: [String] -> [Int]
toint = map read

average :: Int -> IO ()
average 0 = return ()
average index = do
  n <- getLine
  let numbers = toint (wordsWhen (== ' ') n)
  let a = sum numbers
  let b = length numbers
  let avg = fromIntegral a / fromIntegral (b - 1)
  print (round avg)
  average (index - 1)

main = do
  x <- getLine
  let index = read x :: Int
  average index

{-
$ cat DATA.lst | ./frank1030
277
611
10977
517
1098
5051
135
138
947
4499
7933
4607
471
-}
