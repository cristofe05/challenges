## Version 2.0
## language: en

Feature: Web server sql truncation
  Site:
    www.root-me.org
  Category:
    Web server
  User:
    neocbz
  Goal:
    Retrieve an access to the administration's zone

 Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Mozilla Firefox | 66.0.3      |
  Machine information:
    Given I am accessing the following link
    """
    https://www.root-me.org/en/Challenges/Web-Server/SQL-Truncation
    """
    And I see the problem statement
    """
    Retrieve an access to the administration's zone
    """
    Then I selected the challenge button to start
    And The page redirects me to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch36
    """

 Scenario: Success: adding user admin with username of thirty characters
    Given I registered a new user with username admin
    And I got a message that there was already a user with that username
    Then I concluded that the database had default parameters
    Then I registered an admin user with thirty characters length username
    """
    admin              neosecurity
    """
    And I registered a password
    And I was registered succefully
    Then I accessed the administration menu and entered the new password
    And I got a message that indicated the validation flag
    Then I solve the challenge
