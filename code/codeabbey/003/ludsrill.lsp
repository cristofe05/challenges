#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun sums-in-loop ()
  (let ((data))
    (setq data (cdr (read-data)))
    (dolist (item data)
      (format t "~s " (+ (first item) (second item))))))

(sums-in-loop)

#|
cat DATA.lst | clisp ludsrill.lsp
77386 440366 1038645 785728 809727 792139 1001087 708771 676606 1286978 1423888
|#
