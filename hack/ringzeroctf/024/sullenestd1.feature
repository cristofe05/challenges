## Version 2.0
## language: en

Feature: 24-forensics-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/24
  Category:
    Forensics
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> |  <Version>    |
    | Arch linux      | 5.7.6-arch1-1 |
    | Chrome          | 85.0.4181.8   |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/24
    """
    When I open the URL with Google Chrome
    Then I see the challenge statement
    """
    Can you find the matrix password again?
    """
    And I download a file
    """
    2b4d08e1a1eac8a8c9034036d420bd88.zip
    """
    Given the file
    When I extract the file contents
    Then I found a file without extension
    """
    BK
    """

  Scenario: Fail: Check Hex from file
    Given BK file
    When I try to check the file type with
    """
    exiftool
    """
    Then found not useful information about the file
    And I procede to look the interesting strings in file with
    """
    Strings
    """
    Given the output of that command
    When I check it, it looks like strings from files in a linux file system
    Then just to check if the file was not hiding something, I check it with
    """
    https://hexed.it/
    """
    And found nothing interesting or useful

  Scenario: Success: Check partition
    Given The file BK suspected as a linux partition
    When I check it with
    """
    sleuth-kit fsstab
    """
    Then found that indeed is an ext3 file system
    And that root directory is in node 2
    Given that information, I check that file system with
    """
    ext3grep
    """
    When I check the files there, most of them are not interesting
    Then I found some deleted files with suspicious names
    """
    secret.sve and secret.odg
    """
    And I recover those files with
    """
    ext3grep
    """
    Given those files
    When I check the odg file, it displays an image that has no relevant info
    Then I check
    """
    secret.sve
    """
    And found that it is a password protected file
    Given that password protected file
    When I try the most obvious password 12345, it works
    Then I found
    """
    secret.txt
    """
    Given that file
    When I open it
    Then I found the flag
