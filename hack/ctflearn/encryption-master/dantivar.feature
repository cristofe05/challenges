## Version 2.0
## language: en

Feature: Encryption Master - Cryptography - ctflearn
  Site:
    https://ctflearn.com/challenge/243
  User:
    dantivar
  Goal:
    Get flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          |      84      |
    | base64          |    8.26      |
  Machine information:
    Given A file "Here ya go!.txt" that contains a hint
    And some encoded text and the description of the challenge:
    """
    Alright. Serious talk. You need to work pretty hard for this one
    (unless you are an encryption god.) Well, good luck.
    https://mega.nz/#!iPgzXIiD!Pkza_S8YUxIXrZ7gdwMcIoufMzi_FjSio3Vx9GuL0ok
    """

  Scenario: Success: base64 decode
    Given the file
    Then I print it
    And as I see a lot of random text I try to decode it using "base64"
    Then I see a new message that seems like hex code
    Then I try to go from that hex code to ASCII
    And I find a new message which contains binary code
    Then I go from that binary code to ASCII
    And I have the flag
