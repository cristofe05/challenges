# $ lintr::lint('dantivar.r')

sort_stars <- function(stars)  {
    sorted <- FALSE
    while (!sorted) {
        i <- 2
        sorted <- TRUE
        while (i <= length(stars[, 1])) {
            if (as.integer(stars[i, 3]) == as.integer(stars[i - 1, 3])) {
                if (as.integer(stars[i, 2]) < as.integer(stars[i - 1, 2])) {
                    temp <- stars[i, ]
                    stars[i, ] <- stars[i - 1, ]
                    stars[i - 1, ] <- temp
                    sorted <- FALSE
                }
            } else if (as.integer(stars[i, 3]) < as.integer(stars[i - 1, 3])) {
                    temp <- stars[i, ]
                    stars[i, ] <- stars[i - 1, ]
                    stars[i - 1, ] <- temp
                    sorted <- FALSE
            }
            i <- i + 1
        }
    }
    return(stars[, 1])
}

star_position <- function(star) {
    x <- as.integer(star[2])
    y <- as.integer(star[3])

    new_x <- x * cos(rotation) - y * sin(rotation)
    new_y <- y * cos(rotation) + x * sin(rotation)

    value <- c(star[1], new_x, new_y)

    return(value)
}
input <- readLines("stdin")
i <- 1
result <- c()

objects <- as.integer(unlist(strsplit(input[1], " ")))

rotation <- objects[2] * (pi / 180)
objects <- objects[1]


while (i <= objects) {
    star <- unlist(strsplit(input[i + 1], " "))

    result <- rbind(result, star_position(star))
    i <- i + 1
}

result <- sort_stars(result)
cat(paste(result, sep = " "))
# $ cat DATA.lst | Rscript dantivar.r
# Mizar Nembus Media Diadem Kastra Alcyone Altair Vega Procyon Deneb Kochab
# Gemma Algol Capella Zosma Rigel Aldebaran Sirius Thabit Jabbah Bellatrix
# Yildun Unuk Electra Alcor Betelgeuse Pherkad
