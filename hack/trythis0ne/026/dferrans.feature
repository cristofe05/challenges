## Version 2.0
## language: en

Feature: 026-decrypt-trythis0ne.com
  Code:
    026
  Site:
    trythis0ne.com
  Category:
    decrypt
  User:
    dferrans
  Goal:
    decrypt secret string

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
  Machine information:
    Given the challenge URL
    """
    http://www.trythis0ne.com/out.php?level=26
    """
    And the code to analyze how the login works:
    """
    http://www.trythis0ne.com/levels/web-challanges/OSLogin/index.txt
    """

  Scenario: Success:add-form-field-to-login-as-admin
    When I access the challenge using firefox
    And I start to analyze the source code in the site
    And I see the parameters required to login
    And I add add an additional field in the form called admin
    """"
    <input type="password" name="admin" value="1" />
    """
    Then I am able to login as admin to get the secret word
    Then I solve the challenge.
