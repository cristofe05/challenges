#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 57.36 milliseconds
# $ crystal build slayfer1112.cr

def data_entry
  cases = gets
  if cases
    cases = cases.is_a?(String) ? cases.try &.to_i : cases
    data = [] of Array(Float64)
    0.step(to: cases, by: 1) do |_|
      temp = [] of Float64
      args = gets
      if args
        dat = args.split
        val1 = dat[0].to_f
        val2 = dat[1].to_f
        temp << val1
        temp << val2
        data << temp
      end
    end
  end
  data
end

def det(x, y)
  n = x.size - 2
  d = 0
  u = 0
  0.step(to: n, by: 1) do |i|
    d += x[i] * y[i + 1]
    u += y[i] * x[i + 1]
  end
  result = (d - u).abs
  result
end

def solution(array)
  x = [] of Float64
  y = [] of Float64
  array.each do |i|
    x << i[0]
    y << i[1]
  end
  x << x[0]
  y << y[0]

  det = det(x, y)

  area = 0.5 * det

  print "#{area} "
end

data = data_entry()
data ? solution(data) : print "invalid data"
puts

# $ cat DATA.lst | crystal slayfer1112.cr
# 61742862.0
