# frozen_string_literal: true

# rubocop cleancamera.rb
# 1 file inspected, no offenses detected

data = ''
keyboard = ''
driver = ''
money = ''

File.open('DATA.lst', 'r') do |lines|
  line = lines.gets
  data = line
end

data_spl = data.split(' ')

keyboard = data_spl[0] if data_spl[0].eql? data_spl[0]
driver = data_spl[1] if data_spl[1].eql? data_spl[1]
money = data_spl[2] if data_spl[2].eql? data_spl[2]

def get_money_spent(keyboards, drivers, money)
  num_keyboad = keyboards[-1].to_i
  num_driver = drivers[-1].to_i
  money_user = money.to_i
  shop_cart = num_keyboad + num_driver
  if shop_cart <= money_user
    puts(shop_cart)
  elsif shop_cart > money_user
    puts('-1')
  end
end

get_money_spent(keyboard, driver, money)

# ruby cleancamera.rb
# OutPut:
# 9
