/*
# Lint
$ ktlint --experimental --verbose --disabled_rules=experimental:indent,indent \
> miyyer1946.ky
# Compile
$ kotlinc -Werror miyyer1946.kt -include-runtime -d miyyer1946.jar
*/

fun main(args: Array<String>) {
  var input = readLine()!!.split(" ")
  var datos: Long
  var result: String = ""
  for (i in 1..input.size - 1) {
  var conversion = ((input[i].toDouble() - 32) * 5 / 9)
  datos = Math.round(conversion)
  result += datos.toString() + " "
  }
  println(result)
}

/*
$ cat DATA.lst | java -jar miyyer1946.jar
32 566 537 301 52 561 438 278 154 548 427 591 568 376 203 272 165 327 512
347 209 490 367 186 482 475 247 254 512 516 244 72 481
*/
