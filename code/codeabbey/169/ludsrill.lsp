#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(setq *read-default-float-format* 'double-float)

(defun perform-simulation-step (rate dt mass mass-fuel altitute speed)
  (let ((exhaust-speed 2800)
        (moon-gravity 1.622)
        (moon-radius 1737100)
        (total-mass)
        (dm) (dv)
        (gravity-acceleration))

    (setq altitute (- altitute (* speed dt)))
    (setq total-mass (+ mass mass-fuel))
    (if (> mass-fuel 0)
      (setq dm (* dt rate))
      (setq dm 0))

    (setq dv (* exhaust-speed (/ dm total-mass)))

    (if (>= (- mass-fuel dm) 0)
      (setq mass-fuel (- mass-fuel dm))
      (setq mass-fuel 0))

    (setq gravity-acceleration (/ (* moon-gravity (expt moon-radius 2))
      (expt (+ moon-radius altitute) 2)))
    (setq speed (- (+ speed (* gravity-acceleration dt)) dv))
    (return-from perform-simulation-step
      (list mass mass-fuel altitute speed))))

(defun cpu-on-board (item free-time rate-initial rate-change)
  (let ((mass)
        (mass-fuel)
        (altitute)
        (speed)
        (steps)
        (dt)
        (timer)
        (rate)
        (memory)
        (group))

    (setq mass (first item))
    (setq mass-fuel (second item))
    (setq altitute (third item))
    (setq speed (fourth item))
    (setq steps 100)
    (setq dt  (/ 1 steps))
    (setq timer  0)
    (loop while (> altitute 0) do
      (if (>= timer free-time)
        (progn
          (setq rate (+ rate-initial (* rate-change (- timer free-time))))
          (setq rate (min (max rate 0) 100)))
        (setq rate 0))

      (loop for i from 0 to 99 do
        (setq group
          (perform-simulation-step rate dt mass mass-fuel altitute speed))
        (setq mass (first group))
        (setq mass-fuel (second group))
        (setq altitute (third group))
        (setq speed (fourth group))
        (if (< altitute 0)
          (return))
        (setq memory group))
      (setq timer (+ timer 1)))
    (return-from cpu-on-board memory)))

(defun random-search ()
  (let ((data)
        (x)
        (new-speed)
        (y-speed)
        (x-new)
        (value)
        (flag))

    (setf *random-state* (make-random-state t))
    (setq data (cdr (read-data)))
    (dolist (item data)
      (setq x '(0 0 0))
      (setq new-speed 6)
      (loop while (> new-speed 5) do
        (setq y-speed (fourth
          (cpu-on-board item (first x) (second x) (third x))))
        (setq value (random 3))
        (setq x-new (copy-seq x))
        (setq flag 'AGAIN)

        (loop while (equal flag 'AGAIN) do
          (when (= value 0)
            (setf (elt x-new 0) (+ (first x-new) (- (random (1+ (* 2 2))) 2)))
            (if (< (elt x-new 0) 0)
              (progn
                (setf (elt x-new 0) (elt x 0))
                (setq flag 'AGAIN))
              (setq flag 'OK)))

          (when (= value 1)
            (setf (elt x-new 1)
              (+ (second x-new) (- (random (1+ (* 2 1.0))) 1.0)))

            (if (or (< (elt x-new 1) 0) (> (elt x-new 1) 100))
              (progn
                (setf (elt x-new 1) (elt x 1))
                (setq flag 'AGAIN))
              (setq flag 'OK)))

          (when (= value 2)
            (setf (elt x-new 2) (+ (third x-new) (random 0.00001)))
            (if (or (< (elt x-new 2) (- 1)) (> (elt x-new 2) 1))
              (progn
                (setf (elt x-new 2) (elt x 2))
                (setq flag 'AGAIN))
              (setq flag 'OK))))

        (setq new-speed (fourth (cpu-on-board item
          (first x-new) (second x-new) (third x-new))))

        (if (< new-speed y-speed)
          (setq x x-new)))

      (dolist (item X)
        (format t "~F " item)))))

(random-search)

#|
cat DATA.lst | clisp ludsrill.lsp
0 29.525013207263456 0.00025343357625984205 0 14.57566564480731
0.0003410139985314346 1 21.847354370213566 0.00038520035200873936
0 33.16200986286244 0.0005870949710302461 0 28.655690160278926
0.0009269265331514173 3 16.786249587860773 0.0001521559075847806
|#
