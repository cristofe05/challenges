pkgs:

path:
  pkgs.stdenv.mkDerivation rec {
    name = "cargo-libs";
    inherit path;

    srcIncludeGenericShellOptions = ../../include/generic/shell-options.sh;
    srcIncludeGenericDirStructure = ../../include/generic/dir-structure.sh;

    builder = ./builder.sh;
    buildInputs = [
      pkgs.git
      pkgs.cargo
    ];
  }
