;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.033
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn solution [indexed-data data]
  (when (seq data)
    (doseq [item indexed-data]
      (when (= (Integer. (second item)) (first data))
        (print (core/format "%s " (+ (first item) 1)))))
    (solution indexed-data (rest data))))

(defn -main []
  (let [data (map (fn [x] (Integer. x)) (first (rest (get-data))))
        data-sort (sort < data)
        indexed-data (map-indexed vector data)]
    (solution indexed-data data-sort)))

(-main)

;; cat DATA.lst | ludsrill.cljs
;; 14 5 18 20 9 15 19 16 8 2 3 12 10 11 21 17 6 1 13 4 7
